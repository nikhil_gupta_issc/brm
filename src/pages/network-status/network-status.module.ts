import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NetworkStatusPage } from './network-status';

@NgModule({
  declarations: [],
  imports: [
    IonicPageModule.forChild(NetworkStatusPage),
  ],
})
export class NetworkStatusPageModule {}
