import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController  } from 'ionic-angular';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { WalletService } from '../../providers/wallet.service';
import { AppConstants } from '../../domain/app-constants';
import { LogService } from '../../providers/log.service';
import { LokiAddress } from '../../domain/lokijs';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { CSCURI } from '../../domain/csc-types';
import { CSCUtil } from '../../domain/csc-util';
import { ContactPopoverComponent } from '../../components/contact-popover/contact-popover';
import { AlertController } from 'ionic-angular';
import { BRMService } from '../../providers/brm.service';

@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {

  connectedText: string = "Disconnected";
  connectedColor: string = "brand";
  addresses: Array<LokiAddress> = [];
  noContactShow: boolean = true;
  inputShow: boolean = false;
  addButtonShow: boolean = true;
  contactListShow: boolean = false;
  contactName: string = "";
  accountID: string = "";
  errorMessage: string = "";

  constructor( public navCtrl: NavController, 
               public navParams: NavParams,
               private casinocoinService: CasinocoinService,
               private walletService: WalletService,
               private barcodeScanner: BarcodeScanner,
               private logger: LogService,
               public popoverCtrl: PopoverController,
               private alertCtrl: AlertController,
               public brmService: BRMService) {
    // subscribe to CasinoCoin connections
    this.casinocoinService.casinocoinConnectedSubject.subscribe( connected => {
      if(connected){
        // set connected header
        this.connectedText = "Connected";
        this.connectedColor = "connected";
      } else {
        // set connected header
        this.connectedText = "Disconnected";
        this.connectedColor = "disconnected";
      }
    });
    //get all contact addresses
    this.walletService.openWalletSubject.subscribe( result => {
      if(result == AppConstants.KEY_LOADED){
        this.logger.debug("### Contacts Open ###");
        this.addresses = this.walletService.getAllAddresses();
      }
    });
    if(this.addresses.length > 0){
      this.noContactShow = false;
      this.contactListShow = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactsPage');
  }

  presentPopover(myEvent,accountID) {
    this.walletService.contactAccountID = accountID;
    let popover = this.popoverCtrl.create(ContactPopoverComponent);
    popover.present({
      ev: myEvent
    });    
    popover.onDidDismiss(()=>{
      // refresh addresses
      this.addresses = this.walletService.getAllAddresses();
      if(this.addresses.length <= 0){
        this.noContactShow = true;
        this.contactListShow = false;
      }
    });
  }

  showForm() {    
    this.inputShow = true;
    this.addButtonShow = false;
    this.noContactShow = false;
    this.contactListShow = false;
  }

  scanQRCode(){
    // Scan CasinoCoin QRCode
    this.barcodeScanner.scan().then((barcodeData) => {
      this.logger.debug('### SEND - QR Scanned: ' + JSON.stringify(barcodeData));
      let cscUri: CSCURI = CSCUtil.decodeCSCQRCodeURI(barcodeData.text);
      try {        
        this.accountID = cscUri.address;   
      } catch (error) {
        this.accountID = "";
        let alert = this.alertCtrl.create({
          title: 'Not a BRM Account',
          subTitle: 'You can only Add BRM Account in Contacts.',
          buttons: ['Dismiss']
        });
        alert.present();
      }   
    }, (err) => {
      // An error occurred
      this.logger.error('### SEND QR Error: ' + JSON.stringify(err));
    });
  }

  addContact() {
    // create addressbook entry
    let newAddress:LokiAddress = {
      accountID: this.accountID,
      label: this.contactName,
      owner: false
    }
    try {
      //add address in lokijs
      this.walletService.addAddress(newAddress);           
    } catch (error) {
      this.errorMessage = "Account ID Already added in Contacts.";
      setTimeout(() => {
        this.errorMessage = "";
      }, 3000);
      return;
    } 
    this.accountID = "";
    this.contactName = "";
    // hide inputs
    this.inputShow = false;
    this.addButtonShow = true;
    this.contactListShow = true;
    // refresh addresses
    this.addresses = this.walletService.getAllAddresses();
  }

  cancel() {
    this.inputShow = false;
    this.addButtonShow = true;
    this.contactName = "";
    this.accountID = "";
    if(this.addresses.length > 0){
      this.noContactShow = false;
      this.contactListShow = true;
    } else {
      this.noContactShow = true;
      this.contactListShow = false;
    }
  }

}
