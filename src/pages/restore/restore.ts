import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { CSCCrypto, CSCKeyPair } from '../../domain/csc-crypto';
import { AppConstants } from '../../domain/app-constants';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { LocalStorageService } from 'ngx-store';
import * as LokiTypes from '../../domain/lokijs';
import { BRMService } from '../../providers/brm.service';
import { LogService } from '../../providers/log.service';
import { WalletService } from '../../providers/wallet.service';
import { LoginPage } from '../login/login';
import { Observable, Subject } from 'rxjs';
import { MainPage } from '../main/main';
import { CustomModalComponent } from '../../components/custom-modal/custom-modal';
import { UUID } from 'angular2-uuid';
import { User, Registration } from '../../domain/brm-api';
// import { CasinocoinKeypairs } from 'casinocoin-libjs';
// import { CloudSettings } from '@ionic-native/cloud-settings';

@IonicPage()
@Component({
  selector: 'page-restore',
  templateUrl: 'restore.html',
})
export class RestorePage {

  // backupCode: string = "";
  // parsedCode: any;
  // disableButtons: boolean = false;
  defaultKeyPair: LokiTypes.LokiKey;

  // final status
  accountCreated: boolean = false;
  accountCreatedFailed: boolean = false;
  cscConnected: boolean = false;
  cscConnectedFailed: boolean = false;
  currentUser: User;
  recoveryMnemonicWordsArray: Array<string>;
  salt: string;
  pinCode: string;
  setPinDone: boolean = false;
  showPinDialog: boolean = false;
  showInputs: boolean = true;
  enteredPinCode: string = "";
  loader: any;
  message: string = "";
  recoveryEmail: string = "";
  recoveryWords = {
    word1: "",
    word2: "",
    word3: "",
    word4: "",
    word5: "",
    word6: "",
    word7: "",
    word8: "",
    word9: "",
    word10: ""
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private casinocoinService: CasinocoinService,
              private localStorageService: LocalStorageService,
              private brmService: BRMService,
              public logger: LogService,
              private walletService: WalletService,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              public modalCtrl: ModalController) {
  }

  // submitRestoreFromCloud() {
  //   this.cloudSettings.exists()
  //   .then((exists: boolean) => {
  //     this.logger.debug("### RestorePage Cloud Setting - Data Exists in cloud: " + exists);
  //     if(exists) {
  //       this.cloudSettings.load()
  //       .then((savedSettings: any) => {
  //         this.logger.debug("### RestorePage Cloud Loaded settings: " + JSON.stringify(savedSettings));
  //         this.backupCode = savedSettings.User;
  //         this.submitRestore();
  //       })
  //       .catch((error: any) => {
  //         this.logger.error("### RestorePage Backup Error Loaded in cloud: " + error);
  //         this.alertCloudIssue();
  //       });
  //     } else {
  //       this.alertCloudIssue();
  //     }
  //   })
  //   .catch(err => {
  //     this.logger.error("### RestorePage Backup Error Connecting in cloud: " + err);
  //     this.alertCloudIssue();
  //   });
  // }

  // alertCloudIssue(){
  //   let alert = this.alertCtrl.create({
  //     title: 'Error',
  //     subTitle: 'There was an error fetching the data from cloud. Please enter backup code.',
  //     buttons: ['Dismiss']
  //   });
  //   alert.present();    
  // }

  recoverWallet() {
    this.message = "";
    if (this.recoveryWords.word1.length == 0 ||
        this.recoveryWords.word2.length == 0 ||
        this.recoveryWords.word3.length == 0 ||
        this.recoveryWords.word4.length == 0 ||
        this.recoveryWords.word5.length == 0 ||
        this.recoveryWords.word6.length == 0 ||
        this.recoveryWords.word7.length == 0 ||
        this.recoveryWords.word8.length == 0 ||
        this.recoveryWords.word9.length == 0 ||
        this.recoveryWords.word10.length == 0 ||
        this.recoveryEmail.length == 0) {
      this.message = "Please enter all 10 words and your email address!";
    } else {
      this.loader = this.loadingCtrl.create({ spinner: 'crescent', content: 'Generating Account ID with entered words and your Email address', duration: 60000 });
      this.loader.present().then(() => {
        this.salt = this.recoveryEmail.toLowerCase().trim();
        this.recoveryMnemonicWordsArray = [];
        this.recoveryMnemonicWordsArray.push(
          this.recoveryWords.word1.toLowerCase().trim(),
          this.recoveryWords.word2.toLowerCase().trim(),
          this.recoveryWords.word3.toLowerCase().trim(),
          this.recoveryWords.word4.toLowerCase().trim(),
          this.recoveryWords.word5.toLowerCase().trim(),
          this.recoveryWords.word6.toLowerCase().trim(),
          this.recoveryWords.word7.toLowerCase().trim(),
          this.recoveryWords.word8.toLowerCase().trim(),
          this.recoveryWords.word9.toLowerCase().trim(),
          this.recoveryWords.word10.toLowerCase().trim()
        );
        this.logger.debug("### Recover with words: " + JSON.stringify(this.recoveryMnemonicWordsArray) + " and email: " + this.salt);
        try {
          let cscCrypto = new CSCCrypto(this.recoveryMnemonicWordsArray, this.salt);
          let keyPair: CSCKeyPair = cscCrypto.generateKeyPair(0);
          this.logger.debug("### KeyPair[0]: " + JSON.stringify(keyPair));

          this.brmService.getAccountBalance(keyPair.accountID).subscribe(info => {
            this.logger.debug("### Get Account Balance - AccountID: " + keyPair.accountID + " Info: " + JSON.stringify(info));
            if (parseFloat(info.cscBalance) > 0) {
              this.loader.setContent("Fetching data from the server");
              this.defaultKeyPair = {
                privateKey: keyPair.privateKey,
                publicKey: keyPair.publicKey,
                secret: keyPair.seed,
                accountID: keyPair.accountID,
                encrypted: false
              };

              this.brmService.loginUser(this.defaultKeyPair).subscribe(loginResult => {
                this.logger.debug("### LoginResult: " + loginResult);
                if (loginResult == AppConstants.KEY_FINISHED) {
                  this.brmService.getUserByAccountID(this.defaultKeyPair.accountID).subscribe(result => {
                    this.loader.dismiss();
                    this.showInputs = false;
                    this.showPinDialog = true;
                    this.currentUser = result;
                    this.currentUser.UserGamingLimits = result.UserGamingLimits[0];
                    this.currentUser.LinkedAccount = result.LinkedAccount[0];
                    this.currentUser.KYCStatus = result.KYCStatus[0];
                    this.brmService.currentUser = this.currentUser;
                    this.localStorageService.set(AppConstants.KEY_BRM_USER, this.currentUser);
                    this.localStorageService.set(AppConstants.KEY_WALLET_MNEMONIC_HASH, cscCrypto.getPasswordKey());
                    this.brmService.updateCurrentOperators();
                  }, error => {
                    this.loader.dismiss();
                    this.showAlert();
                    this.logger.debug("### Error - Account ID doesn't exists in Backend: " + JSON.stringify(error));
                  });
                }
              });
            } else {
              this.loader.dismiss();
              this.showAlert();
              this.logger.debug("### Error - Account ID doesn't have balance. ");              
            }
          }, error => {
            this.loader.dismiss();
            if (this.casinocoinService.networkConnected) {
              this.showAlert();
            } else {
              let alert = this.alertCtrl.create({
                title: 'Network Error',
                subTitle: 'Please check your Internet connection.',
                buttons: ['Dismiss']
              });
              alert.present();              
            }
            this.logger.debug("### Error - Account ID Not Found or Invalid: " + JSON.stringify(error));
          });
        } catch (error) {
          this.logger.error("### Decrypt Error: " + error);
          this.message = "Can not restore wallet with entered words and email.";
        }
      });
    }
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Oops!',
      subTitle: 'Can not restore wallet with entered words and email.',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  handlePinInput(pin: string) {
    this.enteredPinCode += pin;
    if (this.enteredPinCode.length === 6) {
      this.validatePincode();
    }
  }

  backspacePin() {
    this.enteredPinCode = this.enteredPinCode.substring(0, this.enteredPinCode.length - 1);
  }

  clearPin() {
    this.enteredPinCode = "";
  }

  validatePincode() {
    if (this.setPinDone) {
      this.showPinDialog = false;
      if (this.pinCode === this.enteredPinCode) {
        this.walletService.walletPIN = this.pinCode;
        let pinConfirmationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'PinConformation', Success: true });
        pinConfirmationModal.present();
        pinConfirmationModal.onDidDismiss(() => {
          this.loader = this.loadingCtrl.create({ spinner: 'crescent', content: 'Encrypting PIN Code', duration: 90000 });
          this.loader.present().then(() => {
            let cscCrypto = new CSCCrypto(this.defaultKeyPair.accountID, this.salt);
            // encrypt and store pin
            let encryptedPIN = cscCrypto.encrypt(this.pinCode);
            this.localStorageService.set(AppConstants.KEY_BRM_PIN, encryptedPIN);
            // encrypt and store mnemonic words
            let encryptedMnemonicWords = cscCrypto.encrypt(JSON.stringify(this.recoveryMnemonicWordsArray));
            this.localStorageService.set(AppConstants.KEY_WALLET_MNEMONIC_WORDS, encryptedMnemonicWords);
            // create wallet
            this.createWallet();
          });
        });
      } else {
        // pin doesn't match
        let pinConfirmationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'PinConformation', Success: false });
        pinConfirmationModal.present();
        pinConfirmationModal.onDidDismiss(() => {
          setTimeout(() => {
            this.showPinDialog = true;
          }, 100);
        });
        this.enteredPinCode = "";
        this.setPinDone = false;
      }
    } else {
      this.pinCode = this.enteredPinCode;
      this.enteredPinCode = "";
      this.setPinDone = true;
      this.showPinDialog = false;
      setTimeout(() => {
        this.showPinDialog = true;
      }, 100);
    }
  }

  insertAccount(keypair: LokiTypes.LokiKey) {
    this.logger.debug("### Insert Key in Loki DB: " + JSON.stringify(keypair));
    this.walletService.addKey(keypair);
    let accountSequence = 0;
    let reg: Registration = this.currentUser.Registrations.find(x => x.UserAccountIDForOperator === keypair.accountID);
    if (reg) {
      this.logger.debug("### Registration: " + JSON.stringify(reg));
      accountSequence = reg.AccountSequence;
    }
    this.logger.debug("### Insert Account in Loki DB - AccountSequence: " + accountSequence + ", AccountID: " + keypair.accountID);
    let walletAccount: LokiTypes.LokiAccount = {
      accountID: keypair.accountID,
      accountSequence: accountSequence,
      balance: "0",
      lastSequence: 0,
      label: "Default Account",
      activated: false,
      ownerCount: 0,
      lastTxID: "",
      lastTxLedger: 0
    };
    this.walletService.addAccount(walletAccount);
  }

  getUserOperatorAccounts(): Observable<string> {
    let loopAccountsSubject = new Subject<any>();
    this.logger.debug("### Looking for User Operator Accounts ###");
    let loopAccounts = Observable.timer(0, 1000)
      .subscribe((index) => {
        let cscCrypto = new CSCCrypto(this.recoveryMnemonicWordsArray, this.salt);
        let keyPair: CSCKeyPair = cscCrypto.generateKeyPair(index + 1);
        this.logger.debug("### KeyPair[" + (index + 1) + "]: " + JSON.stringify(keyPair));
        this.brmService.getAccountBalance(keyPair.accountID).subscribe(info => {
          this.logger.debug("### Get Account Balance - AccountID: " + keyPair.accountID + " Info: " + JSON.stringify(info));
          let accountKeypair: LokiTypes.LokiKey = {
            privateKey: keyPair.privateKey,
            publicKey: keyPair.publicKey,
            secret: keyPair.seed,
            accountID: keyPair.accountID,
            encrypted: false
          };
          this.insertAccount(accountKeypair);
        }, error => {
          this.logger.debug("### User Operator Accounts Loop Complete");
          loopAccounts.unsubscribe();
          loopAccountsSubject.next(AppConstants.KEY_FINISHED);
        });
      });
    return loopAccountsSubject.asObservable();
  }

  createWallet() {
    this.logger.debug("### Restore - Creating Wallet - KeyPair: " + JSON.stringify(this.defaultKeyPair));

    let uuid = UUID.UUID();
    let recoveryHash = new CSCCrypto(this.recoveryMnemonicWordsArray, this.salt).encrypt(this.pinCode);
    this.loader.setContent("Restoring Wallet. Please wait...");
    // create Wallet
    this.walletService.createWallet(
      uuid,
      this.pinCode,
      LokiTypes.LokiDBEnvironment.test, // CHANGE THIS WHEN GOING TO PRODUCTION !!!!
      recoveryHash).subscribe(createResult => {
        if (createResult == AppConstants.KEY_FINISHED) {
          this.logger.debug("### RESTORE - Create Backup Account " + createResult);
          if (this.defaultKeyPair.accountID.length > 0) {
            this.accountCreated = true;
            this.insertAccount(this.defaultKeyPair);
            this.loader.setContent("Generating Operator Accounts");

            this.getUserOperatorAccounts().subscribe(result => {
              this.logger.debug("### GetUserOperatorAccounts Status: " + JSON.stringify(result));
              if (result == AppConstants.KEY_FINISHED) {
                this.logger.debug("### RESTORE - Default AccountID: " + this.defaultKeyPair.accountID);
                this.logger.debug("### RESTORE - Accounts: " + JSON.stringify(this.walletService.getAllAccounts()));
                this.loader.setContent("Encrypting Wallet");

                this.localStorageService.set(AppConstants.KEY_DEFAULT_ACCOUNT_ID, this.defaultKeyPair.accountID);
                this.localStorageService.set(AppConstants.KEY_CURRENT_WALLET, uuid);
                this.localStorageService.set(AppConstants.KEY_SETUP_COMPLETED, true);
                this.localStorageService.set(AppConstants.TOUCH_ID_ENABLE, false);

                this.logger.debug("### RESTORE - Encrypt Wallet Keys");
                this.walletService.encryptAllKeys(this.pinCode).subscribe(result => {
                  if (result == AppConstants.KEY_FINISHED) {
                    this.logger.debug("### RESTORE - Keys Encryption Finished");
                  }
                });

                // create casinocoin connection
                this.casinocoinService.connect().subscribe(connected => {
                  if (connected == AppConstants.KEY_CONNECTED) {
                    this.logger.debug("### RESTORE - CasinoCoin Connected ###");
                    this.cscConnected = true;
                  } else if (connected == AppConstants.KEY_DISCONNECTED) {
                    this.cscConnectedFailed = true;
                  }
                });

                this.loader.dismiss();
                this.logger.debug("### RESTORE - Wallet Creation Completed.");
                this.walletService.saveWallet();
                let alert = this.alertCtrl.create({
                  title: 'Success',
                  subTitle: 'Wallet Restore Completed',
                  buttons: ['OK']
                });
                alert.present();
                alert.onDidDismiss(() => {
                  this.walletService.openWalletSubject.next(AppConstants.KEY_INIT);
                  this.brmService.loginSubject.next(AppConstants.KEY_INIT);
                  this.navCtrl.setRoot(LoginPage);
                });

              }
            });
          }
        }
      });
  }

  // submitRestore(){
  //   this.disableButtons = true;
  //   if(this.isValidBackup()){
  //     try {
  //       this.logger.debug("### Restore BRM "+this.backupCode);
  //       //set accountID, brmPin & touchID
  //       this.localStorageService.set(AppConstants.KEY_DEFAULT_ACCOUNT_ID, this.parsedCode.accountID);
  //       this.localStorageService.set(AppConstants.TOUCH_ID_ENABLE, this.parsedCode.touchId);
  //       this.localStorageService.set(AppConstants.KEY_BRM_PIN, this.parsedCode.brmPinData); 

  //       // decrypte Pin
  //       let cscCrypto = new CSCCrypto(this.parsedCode.accountID);
  //       let storedPIN = this.parsedCode.brmPinData;
  //       let decryptedPIN = cscCrypto.decrypt(storedPIN);
  //       this.logger.debug("### Restore Decrypted PIN "+decryptedPIN);      

  //       // decrypt secret key
  //       cscCrypto = new CSCCrypto(decryptedPIN);
  //       let secret = cscCrypto.decrypt(this.parsedCode.accountData);

  //       // create Keypair
  //       let deriveKeypair = CasinocoinKeypairs.deriveKeypair(secret);  
  //       let accountID = CasinocoinKeypairs.deriveAddress(deriveKeypair.publicKey);  
  //       this.defaultKeyPair = {
  //         privateKey: "",
  //         publicKey: "",
  //         secret: "",
  //         accountID: "",
  //         encrypted: false
  //       };  
  //       this.defaultKeyPair.secret = secret;
  //       this.defaultKeyPair.publicKey = deriveKeypair.publicKey;
  //       this.defaultKeyPair.privateKey = deriveKeypair.privateKey;
  //       this.defaultKeyPair.accountID = accountID;  

  //       this.logger.debug("### Restore Account: " + JSON.stringify(this.defaultKeyPair)); 
  //       this.brmService.loginSubject = new BehaviorSubject<boolean>(false);
  //       this.brmService.loginUser(this.defaultKeyPair).subscribe(loginResult => {
  //         this.logger.debug("### LoginResult: " + loginResult);
  //         if(loginResult){     
  //           // get the operators list
  //           // this.brmService.getAllOperators();
  //           this.brmService.updateCurrentOperators();            
  //           this.brmService.updateCurrentUser();

  //           // create Wallet
  //           this.walletService.createWallet(
  //           this.parsedCode.walletUUID, // wallet UUID
  //           decryptedPIN,
  //           LokiTypes.LokiDBEnvironment.test, // CHANGE THIS WHEN GOING TO PRODUCTION !!!!
  //           this.parsedCode.recoveryHash ).subscribe(createResult => {
  //             if(createResult == AppConstants.KEY_FINISHED){
  //               this.logger.debug("### RESTORE - Create Backup Account");
  //               if (this.defaultKeyPair.accountID.length > 0){
  //                 this.walletService.addKey(this.defaultKeyPair);
  //                 // create new account
  //                 let walletAccount: LokiTypes.LokiAccount = {
  //                   accountID: this.defaultKeyPair.accountID,
  //                   accountSequence: 0,
  //                   balance: "0", 
  //                   lastSequence: 0, 
  //                   label: "Default Account",
  //                   activated: false,
  //                   ownerCount: 0,
  //                   lastTxID: "",
  //                   lastTxLedger: 0
  //                 };
  //                 this.walletService.addAccount(walletAccount);
  //                 this.logger.debug("### RESTORE - Accounts: " + JSON.stringify(this.walletService.getAllAccounts()));
  //                 this.logger.debug("### RESTORE - Encrypt Wallet Keys");
  //                 this.walletService.encryptAllKeys(decryptedPIN).subscribe( result => {
  //                   if(result == AppConstants.KEY_FINISHED){
  //                     this.logger.debug("### RESTORE - Key Encryption Finished")
  //                   }
  //                 });
  //                 this.logger.debug("### RESTORE - Walet Creation Complete");
  //                 this.logger.debug("### RESTORE - Default AccountID: " + this.defaultKeyPair.accountID);
  //                 this.localStorageService.set(AppConstants.KEY_CURRENT_WALLET, this.parsedCode.walletUUID);
  //                 // set Setup complete
  //                 this.localStorageService.set(AppConstants.KEY_SETUP_COMPLETED, true);  

  //                 this.accountCreated = true;
  //                 // create casinocoin connection
  //                 this.casinocoinService.connect().subscribe(connected => {
  //                   if(connected == AppConstants.KEY_CONNECTED){
  //                     this.logger.debug("### RESTORE Connected ###");
  //                     this.cscConnected = true;
  //                     setTimeout(() => {
  //                       this.brmService.loginSubject = new BehaviorSubject<boolean>(false);
  //                       this.navCtrl.setRoot(LoginPage);
  //                     }, 1000);
  //                   } else if(connected == AppConstants.KEY_DISCONNECTED){
  //                     this.cscConnectedFailed = true;
  //                   }
  //                 });
  //               }
  //             }
  //           }); 
  //         }
  //       });     
  //     } catch(error) {
  //       this.logger.debug("#### Not a valid Backup Code "+ JSON.stringify(error));
  //       this.disableButtons = false;
  //       this.alertInvalidBackup();
  //     }
  //   } else {
  //     this.logger.debug("#### Not a valid Backup Code.");
  //     this.disableButtons = false;
  //     this.alertInvalidBackup();
  //   }
  // }

  // alertInvalidBackup(){
  //   let alert = this.alertCtrl.create({
  //     title: 'Invalid backup',
  //     subTitle: 'Check that you copied the whole text.',
  //     buttons: ['Dismiss']
  //   });
  //   alert.present();    
  // }

  // isValidBackup(): boolean{
  //   if(this.backupCode.length < 0){
  //     return false;
  //   }
  //   try{
  //     this.parsedCode = JSON.parse(this.backupCode);
  //   } catch(err) {
  //     return false;
  //   }
  //   if(this.parsedCode.accountID == undefined || 
  //     this.parsedCode.accountData == undefined || 
  //     this.parsedCode.brmPinData == undefined ||
  //     this.parsedCode.walletUUID == undefined ||
  //     this.parsedCode.recoveryHash == undefined ||
  //     this.parsedCode.touchId == undefined){
  //     return false;
  //   }
  //   return true;
  // }

  cancel() {
    this.navCtrl.setRoot(MainPage);
  }

}
