import { Component, ViewChild, HostListener, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Slides, Modal, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LogService } from '../../providers/log.service';
import { User, VerificationStatus } from '../../domain/brm-api';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { WalletService } from '../../providers/wallet.service';
import { ValidatorService } from '../../providers/validator.service';
import { LocalStorageService } from 'ngx-store';
import { UUID } from 'angular2-uuid';
import { CSCCrypto, CSCKeyPair } from '../../domain/csc-crypto';
import { AppConstants } from '../../domain/app-constants';
import * as LokiTypes from '../../domain/lokijs';
import { CountrySearchComponent } from '../../components/country-search/country-search';
import { PincodeController } from  'ionic2-pincode-input';
import { BRMService } from '../../providers/brm.service';
import { Keyboard } from '@ionic-native/keyboard';
import { Content } from 'ionic-angular/components/content/content';
import { HomePage } from '../home/home';
// import { LoginPage } from '../login/login';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { NotificationService, NotificationType, SeverityType } from '../../providers/notification.service';
// import { RestorePage } from '../restore/restore';
// import { Clipboard } from '@ionic-native/clipboard';
import { BASE_PATH } from '../../domain/brm-api/variables';
import { JumioService } from '../../providers/jumio.service';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import * as CountryStates from 'node-countries';
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { CustomModalComponent } from '../../components/custom-modal/custom-modal';
import { WebsocketService } from '../../providers/websocket.service';
import { MainPage } from '../main/main';
import { DatePicker } from '@ionic-native/date-picker';
// import { CloudSettings } from '@ionic-native/cloud-settings';
// import { CustomPinComponent } from '../../components/custom-pin/custom-pin';

declare var Jumio;

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  @ViewChild(Content) content: Content;
  @ViewChild('slider') slider: Slides;
  @ViewChild('firstName') firstName;
  @ViewChild('fullName') fullName;
  @ViewChild('pinCodeViewChild') pinCodeViewChild;
  @ViewChild('birthDate') birthDate;

  @HostListener('keydown', ['$event']) onInputChange(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      // e.preventDefault();
      // this.fgPersonalInfo.controls['test'].focusNext();
      this.logger.debug("### Close Keyboard ###");
      this.keyboard.hide();
    }
  }

  dir: string = 'ltr';
  previousSlideEnabled: boolean = false;
  nextSlideEnabled: boolean = false;
  isKeyboardHide: boolean = true;
  footerButtonText: string = "Next";

  userProfile: User;
  walletUUID: string;
  defaultKeyPair:LokiTypes.LokiKey;

  fgPersonalInfo: FormGroup;
  fgContactInfo: FormGroup;
  fgAddressInfo: FormGroup;
  
  lastValidSlideIndex: number = 0;
  progressBarPercent: string = "10%";

  pinSubmitted: boolean = false;
  pinVerified: boolean = false;

  dateOfBirthTouched: boolean = false;
  isDobInvalid: boolean = false;

  jumioResult: any;
  documentSubmitted: boolean = false;

  countryModal:Modal;
  modalTrigger:string;

  pincodeButtonLabel:string = "SET";
  pincodeButtonDisabled:boolean = false;
  private decryptedPIN: string;
  
  setupSuccessShow: boolean = false;
  setupErrorShow: boolean = false;
  recoveryMnemonicWords: Array<string>;
  backupData: any;
  kycVerificationDone: boolean = false;
  changesWithKyc: boolean = false;
  validDoc: boolean = true;
  setPinDone: boolean = false;
  verifyPinDone: boolean = false;
  pincodeVerified: boolean = false;

  // final status
  // accountCreated: boolean = false;
  // accountCreatedFailed:boolean = false;
  // cscConnected: boolean = false;
  // cscConnectedFailed: boolean = false;
  accountActivated: boolean = false;
  accountActivatedFailed: boolean = false;
  // kycVerified: boolean = false;
  // kycVerifiedFailed: boolean = false;

  kycAccount = "cwmm2RkhynaPHXWCD94xcxLSVn1pJrU9Fh";
  // jumio api token and secret (MOVE out of source code !!!)
  jumioToken: string = "654da134-d533-4d86-aabe-749568459529";
  jumioSecret: string = "OA9NKY8hW9R0HeX4yaZhrBSh3WINX5TY";
  jumioCallbackUrl = "";

  defaulLanguage: string = "en";
  defaultCurrency: string = "USD";

  currentSlide = 0;
  toggleContent = false;
  alert: any;
  accountCreationModal: any;
  selectedCountry: any;
  appTitle: string = "Create Account";
  countryStates: Array<string> = [];
  terms_of_service: string = "";
  privacy_policy: string = "";
  titleHide: boolean = true;
  statesAvailable: boolean = true;
  pinCode: string;
  enteredPinCode: string = "";
  pincodeEntered: boolean = false;
  showCustomPin: boolean = false;
  isCordova: boolean = false;
  loader: any;
  jumioDocUploaded = 0;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public platform: Platform,
              private statusBar: StatusBar,
              public keyboard: Keyboard,
              public modalCtrl: ModalController,
              private formBuilder: FormBuilder,
              public logger: LogService,
              private casinocoinService: CasinocoinService,
              private walletService: WalletService,
              private brmService: BRMService,
              private localStorageService: LocalStorageService,
              public pincodeCtrl: PincodeController,
              private toastCtrl: ToastController,
              // private clipboard: Clipboard,
              // private cloudSettings: CloudSettings,
              public notificationService: NotificationService,
              public jumioService: JumioService,
              private androidPermissions: AndroidPermissions,
              public datepipe: DatePipe,
              private alertCtrl: AlertController,
              private http: HttpClient,
              private webSocketService: WebsocketService,
              private datePicker: DatePicker,
              private loadingCtrl: LoadingController,
              @Inject(BASE_PATH) basePath: string) {
    // set light text on dark status bar
    this.statusBar.styleLightContent();
    // Init user profile object
    this.userProfile = this.initUserProfile();
    // create form groups
    this.fgPersonalInfo = this.formBuilder.group({
      // firstname: ['', [Validators.required, ValidatorService.nameValidator]],
      // middlename: ['', ValidatorService.nameValidator],
      // lastname: ['', [Validators.required, ValidatorService.nameValidator]],
      fullName: ['', [Validators.required, ValidatorService.nameValidator]],
      emailaddress:['', [Validators.required, ValidatorService.emailValidator]],
      telephone: ['', [Validators.required, Validators.minLength(7)]]
    });
    // check next slide enabled
    this.fgPersonalInfo.valueChanges.subscribe( value => {
      this.logger.debug("#### fgPersonalInfo Changed: " + JSON.stringify(value));
      this.nextSlideEnabled = this.fgPersonalInfo.valid;
      this.slider.lockSwipeToNext(this.fgPersonalInfo.invalid);
    });
    
    this.fgContactInfo = this.formBuilder.group({
      // telephone: ['', [Validators.required, Validators.minLength(7)]],
      gender: ['',  Validators.required],
      dateOfBirth: ['', [Validators.required, ValidatorService.ageValidator]],
      dobDisplay: [''],
      nationality: ['',  Validators.required],
      nationalityISOCode: ['', Validators.required],
      country: ['', Validators.required],
      countryISOCode: ['', Validators.required],
      countryCallingCode: ['']
      // emailaddress:['', [Validators.required,ValidatorService.emailValidator]],
      // nickname: ['', [Validators.required, Validators.minLength(3)]]
    });
    this.fgContactInfo.valueChanges.subscribe( value => {
      this.logger.debug("#### fgContactInfo Changed: " + JSON.stringify(value));
      this.nextSlideEnabled = this.fgContactInfo.valid;
      this.slider.lockSwipeToNext(this.fgContactInfo.invalid);
    });
    // check age valid
    this.fgContactInfo.controls['dateOfBirth'].valueChanges.subscribe(value => {
      this.logger.debug("### fgContactInfo dateOfBirth changed: " + JSON.stringify(value));
      this.logger.debug("### fgContactInfo.dateOfBirth.valid: " + this.fgContactInfo.controls['dateOfBirth'].valid);
      this.logger.debug("### fgContactInfo.dateOfBirth.touched: " + this.fgContactInfo.controls['dateOfBirth'].touched);
    });

    this.fgAddressInfo = this.formBuilder.group({
      street: ['', Validators.required],
      housenumber: [''],
      postalcode: ['', [Validators.required, ValidatorService.postalcodeValidator]],
      cca2Code: [''],
      city: ['', Validators.required],
      state: ['']
      // country: ['', Validators.required],
      // countryISOCode: ['', Validators.required],
      // countryCallingCode: ['']
    });
    this.fgAddressInfo.valueChanges.subscribe( value => {
      this.logger.debug("#### fgAddressInfo Changed: " + JSON.stringify(value));
      if (this.fgAddressInfo.value.street !== '' && this.fgAddressInfo.value.postalcode !== '' && this.fgAddressInfo.value.city !== '') {
        this.nextSlideEnabled = this.fgAddressInfo.valid;
        this.slider.lockSwipeToNext(this.fgAddressInfo.invalid);
      }
    });
    this.platform.ready().then(() => {
      this.logger.debug("### Signup INIT ###");
      // check platforms
      this.logger.debug("### Signup Platforms: " + this.platform.platforms());
      this.keyboard.onKeyboardShow().subscribe(() => {
        this.isKeyboardHide = false;
        this.logger.debug("Before scroll");
          //  this.content.scrollToBottom(10);
          //  this.keyboard.disableScroll(false);
           //console.log(this.content);
           /*console.log(this.content);
           console.log(this.content._scrollContent.nativeElement.style);
           console.log(this.content._scrollContent.nativeElement.style.cssText);*
           this.logger.debug("After scrole");*/
          //  this.content._scrollContent.nativeElement.style.cssText = "margin-top: -10px; margin-bottom: 64px; padding-bottom: 300px;";
     });
     
     this.keyboard.onKeyboardHide().subscribe(() => {
        this.isKeyboardHide = true;
        // this.content._scrollContent.nativeElement.style.cssText = "margin-top: 56px; margin-bottom: 64px; padding-bottom: 300px;";
           this.content.scrollToTop(10);
     });
      this.localStorageService.set(AppConstants.TOUCH_ID_ENABLE, false); 
      // connect to BRM websocket
      this.webSocketService.connectToBrmWebsocket();
    });
    // Country Modal
    this.countryModal = this.modalCtrl.create(CountrySearchComponent);
    this.countryModal.onDidDismiss(data => {
      this.logger.debug("### Selected Country: " + JSON.stringify(data));
      // for mobile number caller code & flag selection
      if(data != null){
        if(this.modalTrigger == 'nationality'){
          this.fgContactInfo.controls['nationality'].patchValue(data.name.common);
          this.fgContactInfo.controls['nationalityISOCode'].patchValue(data.cca3);
        } else if(this.modalTrigger == 'country'){
          this.fgContactInfo.controls['country'].patchValue(data.name.common);
          this.fgContactInfo.controls['countryISOCode'].patchValue(data.cca3);
          this.fgContactInfo.controls['countryCallingCode'].patchValue(data.callingCode);
          this.fgAddressInfo.controls['cca2Code'].patchValue(data.cca2);
          this.fgAddressInfo.controls['state'].patchValue('');
          if (this.fgAddressInfo.value.postalcode !== '') 
            this.fgAddressInfo.controls['postalcode'].patchValue('');
          this.selectedCountry = data;
          let countryDetails = CountryStates.getCountryByNameOrShortName(data.cca2);
          let countryProvinces = countryDetails.provinces;
          if(countryProvinces) {
            this.statesAvailable = true;
            this.logger.debug("### Country States Added " + JSON.stringify(countryDetails));          
            this.countryStates = [];
            countryProvinces.forEach(element => {
              this.countryStates.push(element.name);           
            });
          } else {
            this.statesAvailable = false;
          }
        }
      }
    });
    // add scroll to ionic slides with keyboard opens
    if(this.platform.is('cordova') && this.platform.is('android')) {
      // for Date of birth date picker
      this.isCordova = true;
      // for scroll
      let appEl = <HTMLElement>(document.getElementsByTagName('ION-APP')[0]),
      appElHeight = appEl.clientHeight;
      // this.keyboard.disableScroll(true);
      window.addEventListener('native.keyboardshow', (e) => {
        appEl.style.height = (appElHeight - (<any>e).keyboardHeight) + 'px';
      });
      window.addEventListener('native.keyboardhide', () => {
        appEl.style.height = '100%';
      });
    }
    let lastTimeBackPress=0;
    let timePeriodToExit = 2000;
    // register back button
    platform.registerBackButtonAction(() => {
      if ((new Date().getTime() - lastTimeBackPress) < timePeriodToExit) {
        this.platform.exitApp(); //Exit from app
      } else {
        const toast = this.toastCtrl.create({
          message: 'Press again to exit',
          duration: 3000,
          position: 'bottom'
        });           
        toast.present();
        lastTimeBackPress = new Date().getTime();
      }
    });
    this.recoveryMnemonicWords = CSCCrypto.getRandomMnemonic();
    this.jumioCallbackUrl = basePath + '/jumio/callback';
    // terms of service & privacy policy    
    this.http.get('assets/terms-of-services.html', {responseType: 'text'}).subscribe(data => {
      this.terms_of_service = data;
    });
    this.http.get('assets/privacy-policy.html', {responseType: 'text'}).subscribe(data => {
      this.privacy_policy = data;
    });
  }

  ionViewDidLoad() {
    this.logger.debug('### ionViewDidLoad SignupPage');
    // add platform to user
    if(this.platform.is('cordova')) {
      if(this.platform.is('android')) {
        this.userProfile.Platform = 'android';
      } else if(this.platform.is('ios')) {
        this.userProfile.Platform = 'ios';
      } else {
        this.userProfile.Platform = 'cordova';
      }
    } else {
      this.userProfile.Platform = 'mobileweb';
    }
    this.webSocketService.userUpdate.subscribe(user => {
      if(user){
        this.logger.debug("### SignupPage Updating userProfile ###");
        this.userProfile = user;
      }
    });
    setTimeout(() => {
      this.keyboard.show();
      // this.firstName.setFocus();
      this.fullName.setFocus();
    },150);
    this.slider.lockSwipeToNext(true);
  }

  initUserProfile(): User{
    return {
        AccountID: "",
        Active: true,
        Fullname: "",
        Firstname: "",
        Middlename: "",
        Lastname: "",
        DateOfBirth: "",
        Gender: "",
        Nationality: "",
        NationalityISOCode: "",
        Street: "",
        Housenumber: "",
        PostalCode: "",
        State: "",
        City: "",
        Country: "",
        CountryISOCode: "",
        Emailaddress: "",
        Telephonenumber: "",
        KYCStatus: {
          Status: VerificationStatus.NOTVERIFIED
        },
        Registrations: [],
        CreationDate: new Date(),
        Language: "en",
        Currency: "USD",
        LastUpdateDate: new Date()
    };
  }

  previousSlide(){
    if(this.slider.getActiveIndex() == 0) {
      this.navCtrl.setRoot(MainPage);
    } else if(this.slider.getActiveIndex() == 5) {
      // Set Pin Slide
      if(this.pinSubmitted || this.pinVerified) {
        this.setPinDone = false;
        this.pincodeVerified = false;
        this.pinSubmitted = false;
        this.pinVerified = false;
        this.enteredPinCode = "";
        this.showCustomPin = true;
        this.pincodeButtonLabel = "SET";
      } else {
        this.slider.slidePrev();        
      }
    } else {
      this.slider.slidePrev();
    }
  }

  // enterPIN(){
  //   console.log("I am here");
  //   var loginPage: LoginPage;
  //   loginPage.enterPIN();
  // }

  nextSlide(){
    //console.log(this.slider.getActiveIndex(), "Next Slide");
    // if(this.slider.getActiveIndex() == 9){
    //   let walletUUID = this.localStorageService.get(AppConstants.KEY_CURRENT_WALLET);  
    //   this.walletService.openWallet(walletUUID).subscribe(result => {
    //     if(result == AppConstants.KEY_LOADED){
    //       // this.webSocketService.connectToBrmWebsocket();
    //       this.navCtrl.setRoot(HomePage);
    //     }
    //   });
    // } else if(this.slider.getActiveIndex() == 8){
    //   // this.nextSlideEnabled = true;
    //   let checked = false;
    //   let alert = this.alertCtrl.create({
    //     title: 'IMPORTANT, PLEASE READ!',
    //     message: "You must save a backup of your wallet to proceed. If you lose your device, you can restore your wallet onto a new device using the backup. The CasinoCoin Foundation cannot assist with restoring lost wallets, you hold full responsibility. Click the box below to confirm you understand the importance of the wallet backup and will store it safely on a separate device or computer.",
    //     inputs: [{
    //         type: 'checkbox',
    //         label:'I have read and understood.',
    //         handler: (data) =>  { 
    //           if(data.checked) {
    //             checked = true;
    //           } else {
    //             checked = false;
    //           }
    //         }
    //       }],
    //     buttons: [
    //       // {
    //       //   text: 'Disagree',
    //       //   role: 'cancel',
    //       //   handler: () => {
    //       //     this.logger.debug('### SignUpPage Backup - Disagree clicked');
    //       //   }
    //       // },
    //       {
    //         text: 'Ok',
    //         handler: data => {
    //           if(checked){
    //             this.logger.debug('### SignUpPage Backup - Agree clicked');
    //             console.log("Copy Backup Code "+this.backupData);
    //             this.clipboard.copy(this.backupData);                
    //             this.cloudSettings.exists()
    //             .then((exists: boolean) => {
    //               this.logger.debug("### SignUpPage Cloud Setting - Data Exists in cloud: " + exists);
    //               let settings = { User: JSON.parse(this.backupData) };
    //               this.cloudSettings.save(settings)
    //               .then((savedSettings: any) => {
    //                 this.logger.debug("### SignUpPage Cloud Saved settings: " + JSON.stringify(savedSettings));
    //               })
    //               .catch((error: any) => {
    //                 this.logger.error("### SignUpPage Backup Error Storing in cloud: " + error);
    //               });
    //             })
    //             .catch(err => {
    //               this.logger.error("### SignUpPage Backup Error Connecting in cloud: " + err);
    //             });
    //             let toast = this.toastCtrl.create({
    //               // message: 'Copied, store it somewhere safely offline.',
    //               message: "Copied and Stored in Google Drive",
    //               duration: 5000,
    //               position: 'top'
    //             });         
    //             toast.present();
    //             this.slider.lockSwipes(false);
    //             this.slider.slideNext();
    //           } else {
    //             this.logger.debug('### SignUpPage Backup - Agree clicked without checkbox Checked');
    //           }
    //         }
    //       }]
    //   });
    //   alert.present();
    // } else 

    if (this.slider.getActiveIndex() == 7) {
      let walletUUID = this.localStorageService.get(AppConstants.KEY_CURRENT_WALLET);  
      this.walletService.openWallet(walletUUID).subscribe(result => {
        this.logger.debug('### SignUp Open Wallet ' + result);
        if(result == AppConstants.KEY_LOADED){
          // this.webSocketService.connectToBrmWebsocket();
          this.navCtrl.setRoot(HomePage);
        }
      });
    } else if(this.slider.getActiveIndex() == 6) {
      let checked = false;
      let alert = this.alertCtrl.create({
        title: 'IMPORTANT!',
        message: "Check the below box to confirm you've written down and safely stored your ten-word recovery phrase, and you understand that if you forget your PIN and lose your recovery phrase, you will lose access to your wallet and the CasinoCoin stored in it.",
        inputs: [{
          type: 'checkbox',
          label:'I have read and understood.',
          handler: (data) =>  { 
            if(data.checked) {
              checked = true;
            } else {
              checked = false;
            }
          }
        }],
        buttons: [
        //   {
        //   text: 'Disagree',
        //   role: 'cancel',
        //   handler: () => {
        //     this.logger.debug('### SignUpPage Recovery Passphrase 2 - Disagree clicked');
        //   }
        // },
        {
          text: 'Continue'
        }]
      });
      alert.present();
      alert.onDidDismiss(() => {
        if(checked){
          this.logger.debug('### SignUpPage Recovery Passphrase 2 - Agree clicked');              
          this.titleHide = false;
          this.slider.lockSwipeToNext(false);
          setTimeout(() => {
            this.slider.slideNext();
          }, 200);
          this.accountCreationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'AccountCreation', Success: true });
          this.accountCreationModal.present();
          this.accountCreationModal.onDidDismiss(data => {
            if (this.brmService.accountCreated && this.brmService.cscConnected && this.brmService.accountActivated && this.brmService.kycVerified) {
              this.nextSlide();
            }
          });
        } else {
          this.logger.debug('### SignUpPage Recovery Passphrase 2 - Agree clicked without checkbox Checked');
          let checkboxAlert = this.alertCtrl.create({
            title: 'Please check the checkbox to continue',
            buttons: [{
              text: 'Continue'
            }]
          });
          checkboxAlert.present();
        }
      });
    } else if (this.slider.getActiveIndex() == 3) {      
      let checked = false;
      let alert = this.alertCtrl.create({
        title: 'CasinoCoin Terms of Service and Privacy Policy',
        message: this.terms_of_service + "<br>" + this.privacy_policy,
        enableBackdropDismiss: false,
        inputs: [
          {
            type: 'checkbox',
            label:'I agree to the CasinoCoin Foundation Terms of Service and Privacy Policy',
            disabled: false,
            handler: (data) =>  { if (data.checked) { checked = true; } else { checked = false; } }
          }
        ],
        buttons: [
          {
            text: 'Continue'
          }
        ]
      });   
      alert.present();   
      alert.onDidDismiss(data => {
        if(checked){
          this.logger.debug('### SignUpPage Terms of Service - Continue clicked redirecting to SignUp Page');
          this.slider.lockSwipeToNext(false);
          this.slider.slideNext();
        } else {
          this.logger.debug('### SignUpPage Terms of Service - Continue clicked without checkbox Checked');
          let checkboxAlert = this.alertCtrl.create({
            title: 'Please accept the Terms and Conditions to continue.',
            buttons: [{
              text: 'Ok'
            }]
          });
          checkboxAlert.present();
        }
      });
    } else if(this.slider.getActiveIndex() == 1){
      if(this.changesWithKyc) {
        this.updateUserProfile();
        this.accountCreationModal.present();
        this.slider.lockSwipes(false);
        this.slider.slideTo(7);
        this.slider.lockSwipes(true);
      } else {
        this.slider.slideNext();
      }
    } else if(this.slider.getActiveIndex() == 0){
      if(this.changesWithKyc) {
        if(this.isDobInvalid){
          this.slider.lockSwipes(false);
          this.slider.slideNext();
        } else {
          this.updateUserProfile();
          this.accountCreationModal.present();
          this.slider.lockSwipes(false);
          this.slider.slideTo(7);
          this.slider.lockSwipes(true);
        }
      } else {
        this.slider.slideNext();
      }
    } else {
      this.slider.slideNext();
    }
  }

  editFields() {
    this.slider.slideTo(0);
  }

  // finishPersonalInfo(){
  //   if (this.fgPersonalInfo.dirty && this.fgPersonalInfo.valid) {
  //     this.logger.debug("### Personal Info Valid ###");
  //     this.userProfile.Firstname = this.fgPersonalInfo.get('firstname').value;
  //     this.userProfile.Lastname = this.fgPersonalInfo.get('lastname').value;
  //   } else {
  //     this.logger.debug("### Personal Info INVALID ###");
  //   }
  // }

  selectCountry(field) {
    this.logger.debug("### selectCountry: " + field);
    this.modalTrigger = field;
    if(this.modalTrigger == 'nationality'){
      this.fgContactInfo.controls['nationality'].markAsTouched();
    } else if(this.modalTrigger == 'country'){
      this.fgContactInfo.controls['country'].markAsTouched();
    }
    this.countryModal.present();
  }

  onStateCancel() {
    this.fgAddressInfo.controls['state'].patchValue('');
  }

  selectDateOfBirth(){
    this.logger.debug("### onClick dateOfBirth");
    this.fgContactInfo.controls['dateOfBirth'].markAsTouched();
    this.dateOfBirthTouched = true;
  }

  selectCordovaDateOfBirth() {    
    this.logger.debug("### onClick dateOfBirth");
    this.fgContactInfo.controls['dateOfBirth'].markAsTouched();
    let selectedDate = this.fgContactInfo.value.dateOfBirth != '' ? new Date(this.fgContactInfo.value.dateOfBirth) : new Date();
    this.datePicker.show({
      mode: 'date',
      date: selectedDate,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        this.logger.debug(" datePicker Got date: " + date);
        this.fgContactInfo.controls['dobDisplay'].patchValue(this.datepipe.transform(date, 'dd-MMM-yyyy'));
        let datePipe = this.datepipe.transform(date, 'yyyy-MM-dd');
        this.fgContactInfo.controls['dateOfBirth'].patchValue(datePipe);
        this.birthDate.setBlur();
      },
      err => {
        this.birthDate.setBlur();
        this.logger.debug("Error occurred while getting date: " + err)
      }
    );
  }

  displayAlert(message: string){
    // display alert message
    this.alert = this.alertCtrl.create({
      title: 'Invalid Credentials',
      message: message,
      buttons: ['Ok']
    });   
    this.alert.present();
  }

  validateUserDetails(scanReference: string): Observable<string>{
    // validate users first name, last name & dob with kyc document result
    this.logger.debug("### SIGNUP Validating User Details with Scan Reference: " + scanReference);
    let msg: NotificationType = {severity: SeverityType.info, title:'Loading', body:'Please wait...'};
    this.notificationService.addMessage(msg);
    let validateSubject = new Subject<any>();
    // jumio api calling for every 5 sec
    let callJumioApi = Observable.interval(5000)
      .subscribe((val) => {
        this.logger.debug("### SIGNUP - Calling Jumio API with interval of 5 secs : " + val);
        // if(val == 1){
        //   setTimeout(() => {
        //     this.logger.debug("### SIGNUP - No Response from Jumio after 10 secs. Continue Process.");
        //     callJumioApi.unsubscribe();
        //     let alert = this.alertCtrl.create({
        //       title: 'KYC Verification Pending',
        //       message: 'Your account is not verified yet, you would be unable to signup for any operator. Once the account is verified, you can signup for operators.',
        //       buttons: ['Ok']
        //     });   
        //     alert.present();
        //     alert.onDidDismiss(() => {
        //       validateSubject.next(AppConstants.KEY_FINISHED);
        //     });
        //   }, 2000);
        // }
        // this.jumioService.kycVerify(this.jumioToken, this.jumioSecret, scanReference).subscribe( jumioResult => {
        this.brmService.getScanResult(scanReference).subscribe(jumioResult => {
          this.logger.debug("### SIGNUP - JUMIO API result: "+JSON.stringify(jumioResult));
          // update status of user in localstorage
          this.userProfile.KYCStatus.ScanReference = jumioResult.scanReference;
          this.userProfile.KYCStatus.Status = jumioResult.document.status;
          this.localStorageService.set(AppConstants.KEY_BRM_USER, this.userProfile);
          if(jumioResult.document.status == 'APPROVED_VERIFIED'){
            // let firstName = jumioResult.document.firstName;
            // let lastName = jumioResult.document.lastName;
            let jumioFullName = jumioResult.document.firstName + " " + jumioResult.document.lastName;
            let dob = jumioResult.document.dob;
            this.isDobInvalid = false;
            let isNameInvalid = false;
            // let firstMiddleName = this.userProfile.Firstname.toLowerCase() + " " + this.userProfile.Middlename.toLowerCase();
            let message = "Your";
            let asPerDoc = "<p><u> As Per Documents </u></p>";
            callJumioApi.unsubscribe();
            // if(firstName.toLowerCase() === firstMiddleName.trim() && lastName.toLowerCase() === this.userProfile.Lastname.toLowerCase() && dob === this.userProfile.DateOfBirth){
            if(jumioFullName.toLowerCase().trim() === this.userProfile.Fullname.toLowerCase() && dob === this.userProfile.DateOfBirth){
              this.logger.debug("### SIGNUP - JUMIO API result is matching with User Entered Details: "+JSON.stringify(this.userProfile));              
              this.localStorageService.set(AppConstants.JUMIO_RESULT, jumioResult);
              this.changesWithKyc = false;
              validateSubject.next(AppConstants.KEY_FINISHED);
            } else {
              // if(firstName.toLowerCase() !== firstMiddleName.trim()){
              //   isNameInvalid = true;
              //   message = message + " First name";
              //   asPerDoc = asPerDoc + "<p>First Name: &nbsp; &nbsp; "+ firstName +"</p>";
              // } 
              // if(lastName.toLowerCase() !== this.userProfile.Lastname.toLowerCase()){
              //   isNameInvalid = true;
              //   message = message + " Last name";
              //   asPerDoc = asPerDoc + "<p>Last Name: &nbsp; &nbsp; "+ lastName +"</p>";
              // } 

              this.changesWithKyc = true;
              if(jumioFullName.toLowerCase().trim() !== this.userProfile.Fullname.toLowerCase()){
                this.logger.debug("### SIGNUP - Entered Name: " + this.userProfile.Fullname.toLowerCase() + ", Jumio Name: " + jumioFullName.toLowerCase().trim());
                isNameInvalid = true;
                message = message + " Full name";
                asPerDoc = asPerDoc + "<p>Full Name: &nbsp; &nbsp; "+ jumioFullName +"</p>";
              }
              if(dob !== this.userProfile.DateOfBirth){
                this.logger.debug("### SIGNUP - Entered DOB: " + this.userProfile.DateOfBirth + ", Jumio DOB: " + dob);
                this.isDobInvalid = true;
                message = message + " Date of Birth";
                dob = this.datepipe.transform(dob, 'dd-MMM-yyyy');
                asPerDoc = asPerDoc + "<p>Date of Birth: &nbsp; "+ dob +"</p>";
              }
              message = message + " doesn't match with the document provided.";
              if(asPerDoc.length > 32){
                message = message + asPerDoc;
              }
              this.displayAlert(message);
              this.alert.onDidDismiss(() => {
                this.accountCreationModal.dismiss();
                this.slider.lockSwipes(false);
                if(isNameInvalid){
                  this.slider.slideTo(0);
                } else if(this.isDobInvalid){
                  this.slider.slideTo(1);
                }
                this.slider.lockSwipes(true);
              });
            }
          } else {
            callJumioApi.unsubscribe();
            this.logger.debug("### SIGNUP - Document Uploaded is " + jumioResult.document.status);
            this.displayAlert("Your Document is " + jumioResult.document.status + ". Please upload a valid Document.");
            this.alert.onDidDismiss(() => {
              this.accountCreationModal.dismiss();
              this.slider.lockSwipes(false);
              this.previousSlideEnabled = false;
              this.nextSlideEnabled = false;
              this.kycVerificationDone = false;
              this.validDoc = false;
              this.slider.slideTo(4);
              this.slider.lockSwipes(true);
            });
            validateSubject.next(AppConstants.KEY_ERRORED);
          }
        }, error => {
          // 404 error - jumio api result not yet updated
        });
      });
    return validateSubject.asObservable();
  }

  runNetverify(){
    if (this.platform.is('cordova')) {
      if (this.platform.is('android')) {
        // android permissions
        this.androidPermissions.requestPermissions(
          [
            this.androidPermissions.PERMISSION.CAMERA,
            this.androidPermissions.PERMISSION.GET_ACCOUNTS, 
            this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, 
            this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE,
            this.androidPermissions.PERMISSION.INTERNET
          ]
        );
      }
    }
    this.startNetverify();
  }

  startNetverify() {
    // Netverify => requireVerification= true
		this.logger.debug("### Init Netverify ###");
		Jumio.initNetverify(this.jumioToken, this.jumioSecret, "EU", {
        requireVerification: true,
        requireFaceMatch: true,
				customerId: this.defaultKeyPair.accountID,
				preselectedCountry: this.fgContactInfo.value.countryISOCode,
				cameraPosition: "BACK",
        documentTypes: ["DRIVER_LICENSE", "PASSPORT", "IDENTITY_CARD"],
        callbackUrl: this.jumioCallbackUrl
		});
    this.logger.debug("### Start Netverify");
    // set submitted date
    this.userProfile.KYCStatus.ScanSubmittedDate = new Date();
		Jumio.startNetverify( (documentData, error) => {
      if(error){
        this.logger.debug("### Netverify Error: " + JSON.stringify(error));
        this.documentSubmitted = false;
      } else {
        this.logger.debug("### Netverify DocumentData: " + JSON.stringify(documentData));
        this.jumioResult = documentData;
        this.documentSubmitted = true;        
        // set scanReference
        this.userProfile.KYCStatus.ScanReference = this.jumioResult.scanReference;
        // save user to brm
        this.brmService.saveUser(this.userProfile).subscribe( result => {
          this.logger.debug("### Save User Result: " + JSON.stringify(result));
        });
        this.kycVerificationDone = true;
        this.jumioDocUploaded++;

        if(this.validDoc){
          this.nextSlideEnabled = true;
          this.slider.lockSwipeToNext(false);
          this.slider.slideNext();
        } else {
          this.slider.lockSwipes(false);
          this.slider.slideTo(7);
          this.slider.lockSwipes(true);          
        }


        /* Jumio.initDocumentVerification(this.jumioToken, this.jumioSecret, "EU", {
          type: "UB",
          customerId: this.defaultKeyPair.accountID,
          country: this.fgAddressInfo.value.countryISOCode,
          merchantScanReference: this.jumioResult.scanReference,
          cameraPosition: "BACK"
        });
        Jumio.startDocumentVerification((documentData, error) => {
          if(error){
            this.logger.debug("### DocumentVerification Error: " + JSON.stringify(error));
          } else {
            this.logger.debug("### DocumentVerification documentData: " + JSON.stringify(documentData));
            // valid doc check, works after jumio api is done
            if(this.validDoc){
              this.nextSlideEnabled = true;
              this.slider.lockSwipeToNext(false);
              this.slider.slideNext();
            } else {
              this.slider.lockSwipes(false);
              this.slider.slideTo(7);
              this.slider.lockSwipes(true);          
            }
          }
        }); */


        // call Jumio api with ScanReference
        // setTimeout(() => { 
        //   // call Jumio api with ScanReference
        //   this.brmService.kycVerify(this.jumioResult.scanReference, this.userProfile).subscribe(jumioData => {
        //     this.logger.debug("### Jumio Netverify: " + JSON.stringify(jumioData));
        //     if(jumioData.document.status !== null) {
        //       this.userProfile.KYCStatus.Status = jumioData.document.status;
        //       this.userProfile.KYCStatus.VerificationReceivedDate = new Date();
        //       this.localStorageService.set(AppConstants.JUMIO_RESULT, jumioData);
        //     }
        //     // save user to brm
        //     this.brmService.saveUser(this.userProfile).subscribe( result => {
        //       this.logger.debug("### Save User Result: " + JSON.stringify(result));
        //     });
        //   });
        // }, 91000);           
      }
		});
  }
  
  createWalletIfNotExists(): Observable<boolean>{
    this.logger.debug("### SIGNUP createWalletIfNotExists");
    let walletCreationCompleteSubject = new BehaviorSubject<boolean>(false);
    // get PIN to set as wallet password
    let cscCrypto = new CSCCrypto(this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID), this.userProfile.Emailaddress);
    this.decryptedPIN = cscCrypto.decrypt(this.localStorageService.get(AppConstants.KEY_BRM_PIN));
    this.walletUUID = UUID.UUID();
    this.logger.debug("### SIGNUP recovery mnemonic: " + this.recoveryMnemonicWords);
    let recoveryHash = new CSCCrypto(this.recoveryMnemonicWords, this.userProfile.Emailaddress).encrypt(this.decryptedPIN);
    this.logger.debug("### SIGNUP recovery hash: " + recoveryHash);
    this.walletService.createWallet( 
      this.walletUUID, // wallet UUID
      this.decryptedPIN,
      LokiTypes.LokiDBEnvironment.test, // CHANGE THIS WHEN GOING TO PRODUCTION !!!!
      recoveryHash ).subscribe(createResult => {
        if(createResult == AppConstants.KEY_FINISHED){
          this.logger.debug("### SIGNUP - Create new Account");
          if (this.defaultKeyPair.accountID.length > 0){
            this.walletService.addKey(this.defaultKeyPair);
            // create new account
            let walletAccount: LokiTypes.LokiAccount = {
              accountID: this.defaultKeyPair.accountID, 
              accountSequence: 0,
              balance: "0", 
              lastSequence: 0, 
              label: "Default Account",
              activated: false,
              ownerCount: 0,
              lastTxID: "",
              lastTxLedger: 0
            };
            this.walletService.addAccount(walletAccount);
            this.logger.debug("### SIGNUP - Accounts: " + JSON.stringify(this.walletService.getAllAccounts()));
            this.logger.debug("### SIGNUP - Encrypt Wallet Keys");
            this.walletService.encryptAllKeys(this.decryptedPIN).subscribe( result => {
              if(result == AppConstants.KEY_FINISHED){
                this.logger.debug("### SIGNUP - Key Encryption Finished")
              }
            });
            this.logger.debug("### SIGNUP - Walet Creation Complete");
            this.logger.debug("### SIGNUP - Default AccountID: " + this.userProfile.AccountID);
            this.localStorageService.set(AppConstants.KEY_CURRENT_WALLET, this.walletUUID);
            walletCreationCompleteSubject.next(true);
          }
        }
      });  
      return walletCreationCompleteSubject.asObservable();
  }

  updateUserProfile(){
    // this.userProfile.Firstname = this.fgPersonalInfo.value.firstname.trim();
    // this.userProfile.Middlename = this.fgPersonalInfo.value.middlename.trim();
    // this.userProfile.Lastname = this.fgPersonalInfo.value.lastname.trim();
    this.userProfile.Fullname = this.fgPersonalInfo.value.fullName.trim();
    this.userProfile.DateOfBirth = this.fgContactInfo.value.dateOfBirth;
    this.userProfile.Gender = this.fgContactInfo.value.gender;
    this.userProfile.NationalityISOCode = this.fgContactInfo.value.nationalityISOCode;
    this.userProfile.Nationality = this.fgContactInfo.value.nationality;
    this.userProfile.Street = this.fgAddressInfo.value.street;
    this.userProfile.Housenumber = this.fgAddressInfo.value.housenumber;
    this.userProfile.PostalCode = this.fgAddressInfo.value.postalcode.toUpperCase();
    this.userProfile.City = this.fgAddressInfo.value.city;
    this.userProfile.State = this.fgAddressInfo.value.state;
    this.userProfile.CountryISOCode = this.fgContactInfo.value.countryISOCode;
    this.userProfile.Country = this.fgContactInfo.value.country;
    this.userProfile.Emailaddress = this.fgPersonalInfo.value.emailaddress.toLowerCase().trim();
    this.userProfile.Telephonenumber = "+" + this.fgContactInfo.value.countryCallingCode + " " + this.fgPersonalInfo.value.telephone;
    this.userProfile.Language = this.defaulLanguage;
    this.userProfile.Currency = this.defaultCurrency;
    this.logger.debug("### User Profile: " + JSON.stringify(this.userProfile));
  }

  slideChanged() {
    this.logger.debug("### Current Slide: " + this.slider.getActiveIndex());
    if(this.slider.getActiveIndex() < 4) {
      this.updateUserProfile();
    }
    this.currentSlide = this.slider.getActiveIndex();
    if(this.slider.getActiveIndex() == 0){
      // Personal Info
      this.progressBarPercent = "10%";
      this.titleHide = true;
      this.appTitle = "Create Account";
      this.previousSlideEnabled = false;
      this.nextSlideEnabled = this.fgPersonalInfo.valid;
      if(this.changesWithKyc) {
        this.slider.lockSwipes(true);
      } else {
        // this.firstName.setFocus();
        this.fullName.setFocus();
        this.slider.lockSwipeToNext(this.fgPersonalInfo.invalid);
      }  
    } else if(this.slider.getActiveIndex() == 1){
      // Contact Info
      this.progressBarPercent = "20%";
      this.titleHide = true;
      this.previousSlideEnabled = true;
      this.nextSlideEnabled = this.fgContactInfo.valid;
      if(this.changesWithKyc) {
        this.slider.lockSwipes(true);
      } else {
        this.slider.lockSwipeToNext(this.fgContactInfo.invalid);
      }

      // let focus async first
      setTimeout(() => {
        if (!this.userProfile.AccountID) {
          // We finished page 1 and have the users email address
          // Generate and create the main users account using sequence number 0
          let cscCrypto = new CSCCrypto(this.recoveryMnemonicWords, this.userProfile.Emailaddress);
          // generate default keypair for sequence 0
          let cscKeyPair:CSCKeyPair = cscCrypto.generateKeyPair(0);
          this.defaultKeyPair = { 
            privateKey: cscKeyPair.privateKey, 
            publicKey: cscKeyPair.publicKey, 
            accountID: cscKeyPair.accountID, 
            secret: cscKeyPair.seed, 
            encrypted: false
          }
          this.logger.debug("### Signup Default Account: " + JSON.stringify(this.defaultKeyPair));
          this.brmService.loginUser(this.defaultKeyPair).subscribe(loginResult => {
            this.logger.debug("### LoginResult: " + loginResult);
            if(loginResult){
              // get the operators list
              this.brmService.getAllOperators()
            }
          });
          this.userProfile.AccountID = this.defaultKeyPair.accountID;
          this.localStorageService.set(AppConstants.KEY_DEFAULT_ACCOUNT_ID, this.defaultKeyPair.accountID);
          // store mnemonic_hash for later usage
          this.localStorageService.set(AppConstants.KEY_WALLET_MNEMONIC_HASH, cscCrypto.getPasswordKey());
        }        
      }, 100);
      
    } else if(this.slider.getActiveIndex() == 2){
      // Address Info
      this.progressBarPercent = "35%";
      this.titleHide = true;
      this.appTitle = "Create Account";
      this.previousSlideEnabled = true;
      this.nextSlideEnabled = this.fgAddressInfo.valid;
      this.slider.lockSwipeToNext(this.fgAddressInfo.invalid);
    } else if(this.slider.getActiveIndex() == 3){
      // KYC Info
      this.progressBarPercent = "50%";
      this.titleHide = true;
      this.appTitle = "Confirmation";
      this.nextSlideEnabled = true;
      this.slider.lockSwipeToNext(true);
    } else if(this.slider.getActiveIndex() == 4){
      // Upload Document
      this.progressBarPercent = "60%";
      this.titleHide = true;
      this.appTitle = "Confirmation";
      this.showCustomPin = false;
      // activate user account on blockchain
      if(!this.accountActivated) {
        this.brmService.activateAccount(this.userProfile).subscribe( result => {
          this.logger.debug("### Account Activation Result: " + JSON.stringify(result));
          if(result['status'] == "OK") {
            this.accountActivated = true;
            // save user to brm
            this.brmService.saveUser(this.userProfile).subscribe( result => {
              this.logger.debug("### Save User Result: " + JSON.stringify(result));
            });
          } else {
            this.accountActivatedFailed = true;
          }
        });
      }
      // enable swipe next in browser version
      if (this.platform.is('cordova')) {
        if(this.kycVerificationDone){
          this.nextSlideEnabled = true;
          this.slider.lockSwipeToNext(false);
        } else {
          this.nextSlideEnabled = false;
          this.slider.lockSwipeToNext(true);        
        }
      } else {
        this.nextSlideEnabled = true;
        this.slider.lockSwipeToNext(false);
      }
    } else if(this.slider.getActiveIndex() == 5){
      // Set PIN
      this.progressBarPercent = "75%";
      this.titleHide = true;
      this.appTitle = "Wallet PIN";
      if(!this.pinSubmitted && !this.pinVerified) {
        // pinpad controller
        // this.setWalletPIN();
        
        // input field
        // setTimeout(() => {
        //   this.pinCodeViewChild.setFocus();
        // }, 200);

        // custom modal pin
        // this.openCustomPin("Set PIN");

        // custom local pin
        this.showCustomPin = true;
      }
      this.nextSlideEnabled = (this.pinSubmitted && this.pinVerified);
      this.slider.lockSwipeToNext((!this.pinSubmitted || ! this.pinVerified));
    } else if(this.slider.getActiveIndex() == 6){
      // Recovery Passphrase Info
      this.progressBarPercent = "90%";
      this.titleHide = true;
      this.appTitle = "PIN Code Recovery Phrase";
      let checked = false;
      this.slider.lockSwipeToNext(true);
      let alert = this.alertCtrl.create({
        title: 'IMPORTANT!',
        message: "The following ten-word recovery phrase is the ONLY way you can reset your PIN code if you forget it and restore your wallet. The CasinoCoin Foundation cannot assist with this. Please check the below box to confirm you've read and understand the importance of saving the ten-word recovery phrase.",
        inputs: [{
            type: 'checkbox',
            label:'I have read and understood.',
            handler: (data) =>  { 
              if(data.checked) {
                checked = true;
              } else {
                checked = false;
              }
            }
          }],
        buttons: [
          // {
          //   text: 'Disagree',
          //   role: 'cancel',
          //   handler: () => {
          //     this.logger.debug('### SignUpPage Recovery Passphrase 1 - Disagree clicked');
          //   }
          // },
          {
            text: 'Continue'
          }]
      });
      alert.present();
      alert.onDidDismiss(() => {
        if(checked){
          this.logger.debug('### SignUpPage Recovery Passphrase 1 - Agree clicked');
          this.nextSlideEnabled = true;
        } else {
          this.logger.debug('### SignUpPage Recovery Passphrase 1 - Agree clicked without checkbox Checked');
          let checkboxAlert = this.alertCtrl.create({
            title: 'Please check the checkbox to continue',
            buttons: [{
              text: 'Continue'
            }]
          });
          checkboxAlert.present();          
          checkboxAlert.onDidDismiss(() => {
            this.slider.slideTo(5);
          });
        }
      });
    } else if(this.slider.getActiveIndex() == 7){
      // Finising Setup
      this.progressBarPercent = "100%";
      this.titleHide = false;
      this.appTitle = "Final Review";
      this.previousSlideEnabled = false;
      this.nextSlideEnabled = false;
      this.slider.lockSwipes(true);
      this.finalizeSetup();

    // } else if(this.slider.getActiveIndex() == 8){
    //   // Backup 
    //   this.progressBarPercent = "90%";
    //   this.titleHide = true;
    //   this.appTitle = "Account Backup";
    //   this.backupCodeGenerate();
    //   this.previousSlideEnabled = false;
    //   this.nextSlideEnabled = true;
    //   this.slider.lockSwipes(true);
    // } else if(this.slider.getActiveIndex() == 9){
    //   // Go To Account
    //   this.progressBarPercent = "100%";
    //   this.titleHide = false;
    //   this.appTitle = "Welcome to BRM";
    //   let accountBackupModal = this.modalCtrl.create(CustomModelComponent, { Type: 'AccountBackUp', Success: true });
    //   accountBackupModal.present();
    //   accountBackupModal.onDidDismiss(data => {
    //     this.nextSlide();
    //   });
    }
  }

  // openCustomPin(pageTitle) {
  //   let modal = this.modalCtrl.create(CustomPinComponent, { pageTitle: pageTitle });
  //   modal.present();
  //   modal.onDidDismiss(data => {
  //     if(data) {
  //       this.enteredPinCode = data;
  //       this.validatePincode();
  //     }
  //   });
  // }

  handlePinInput(pin: string) {
    this.enteredPinCode += pin;
    if (this.enteredPinCode.length === 6) {
      this.validatePincode();
    }
  }

  backspacePin() {
    this.enteredPinCode = this.enteredPinCode.substring(0, this.enteredPinCode.length - 1);
  }

  clearPin() {
    this.enteredPinCode = "";
  }

  validatePincode() {
    if(this.setPinDone){
      if(this.enteredPinCode.length === 6) {
        // this.pinCodeViewChild.setBlur();
        this.loader = this.loadingCtrl.create({spinner: 'crescent', cssClass: 'black-spinner', duration: 60000});
        this.loader.present().then(() => {
          // setTimeout(() => {
            if(this.pinCode === this.enteredPinCode) {
              // pin match
              this.showCustomPin = false;
              let pinConfirmationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'PinConformation', Success: true });
              pinConfirmationModal.present();
              this.pinVerified = true;
              this.pincodeVerified = true;
              this.walletService.walletPIN = this.pinCode;
              let cscCrypto = new CSCCrypto(this.defaultKeyPair.accountID, this.userProfile.Emailaddress);
              // encrypt and store pin
              let encryptedPIN = cscCrypto.encrypt(this.pinCode);
              this.localStorageService.set(AppConstants.KEY_BRM_PIN, encryptedPIN);
              // encrypt and store mnemonic words
              let encryptedMnemonicWords = cscCrypto.encrypt(JSON.stringify(this.recoveryMnemonicWords));
              this.localStorageService.set(AppConstants.KEY_WALLET_MNEMONIC_WORDS, encryptedMnemonicWords);
              pinConfirmationModal.onDidDismiss(data => {
                this.loader.dismiss();
                this.nextSlideEnabled = (this.pinSubmitted && this.pinVerified);
                this.slider.lockSwipeToNext((!this.pinSubmitted || ! this.pinVerified));
                this.nextSlide();
              });
            } else {
              // pin doesn't match
              this.loader.dismiss();
              this.showCustomPin = false;
              let pinConfirmationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'PinConformation', Success: false });
              pinConfirmationModal.present();
              pinConfirmationModal.onDidDismiss(() => {
                setTimeout(() => {
                  this.showCustomPin = true;
                }, 100);
                // this.openCustomPin("Set PIN");
                // setTimeout(() => {
                //   this.pinCodeViewChild.setFocus();
                // }, 200);
              });
              this.pinVerified = false;
              this.enteredPinCode = "";
              this.setPinDone = false;
            }
          // }, 200);
        });
      }
    } else {
      if(this.enteredPinCode.length === 6) {
        // this.pinCodeViewChild.setBlur();
        this.pinCode = this.enteredPinCode;
        this.enteredPinCode = "";
        this.setPinDone = true;
        this.pinSubmitted = true;
        this.showCustomPin = false;
        setTimeout(() => {
          this.showCustomPin = true;
        }, 100);
        // this.openCustomPin("Confirm PIN");
        // setTimeout(() => {
        //   this.pinCodeViewChild.setFocus();
        // }, 200);
      }
    }
  }

  // setWalletPIN(){
  //   this.pincodeButtonDisabled = true;
  //   this.pincodeButtonLabel = "Setting PIN ...";
  //   let pinCode =  this.pincodeCtrl.create({
  //     title:'BRM PIN Code',
  //     hideForgotPassword: true,
  //     enableBackdropDismiss: false,
  //     hideCancelButton: true
  //   });
  //   pinCode.present();
  //   pinCode.onDidDismiss( (code,status) => {
  //     if(status === 'done'){
  //       this.logger.debug("### PIN: " + JSON.stringify(code));
  //       let cscCrypto = new CSCCrypto(this.defaultKeyPair.accountID, this.userProfile.Emailaddress);
  //       let encryptedPIN = cscCrypto.encrypt(code);
  //       this.localStorageService.set(AppConstants.KEY_BRM_PIN, encryptedPIN);
  //       this.pinSubmitted = true;
  //       this.setPinDone = true;
  //       this.verifyPinDone = true;
  //       this.pincodeButtonLabel = "Confirm PIN";
  //       this.pincodeButtonDisabled = false;
  //       this.verifyWalletPIN();
  //     } else if (status === 'forgot'){
  //       this.logger.debug("### Forgot Password");
  //     } else {
  //       this.pincodeButtonDisabled = false;
  //       this.pincodeButtonLabel = "SET";        
  //     }
  //   });
  // }

  // verifyWalletPIN(){
  //   this.pincodeButtonDisabled = true;
  //   this.pincodeButtonLabel = "Confirm PIN ...";
  //   let pinCode =  this.pincodeCtrl.create({
  //     title:'Verify PIN',
  //     hideForgotPassword: true,
  //     enableBackdropDismiss: false,
  //     hideCancelButton: true
  //   });
  //   pinCode.present();
  //   pinCode.onDidDismiss( (code,status) => {
  //     if(status === 'done'){
  //       this.logger.debug("### Verify PIN: " + JSON.stringify(code));
  //       let cscCrypto = new CSCCrypto(this.defaultKeyPair.accountID, this.userProfile.Emailaddress);
  //       let decryptedPIN = cscCrypto.decrypt(this.localStorageService.get(AppConstants.KEY_BRM_PIN));
  //       this.logger.debug("### Entered PIN: " + code + " Stored PIN: " + decryptedPIN);
  //       if(code === decryptedPIN){
  //         this.pinVerified = true;
  //         this.pincodeButtonDisabled = false;
  //         this.pincodeButtonLabel = "Set PIN";
  //         this.walletService.walletPIN = code;
  //         //this.setPinDone = true;
  //         this.verifyPinDone = false;
  //         this.pincodeVerified = true;
  //         this.nextSlideEnabled = (this.pinSubmitted && this.pinVerified);
  //         this.slider.lockSwipeToNext((!this.pinSubmitted || ! this.pinVerified));
  //         let pinConfirmationModal = this.modalCtrl.create(CustomModelComponent, { Type: 'PinConformation', Success: true });
  //         pinConfirmationModal.present();
  //         pinConfirmationModal.onDidDismiss(data => {
  //           this.nextSlide();
  //         });
  //       } else {
  //         // let msg: NotificationType = {severity: SeverityType.error, title:'PIN Error', body:'Set and Verify PIN do not match.'};
  //         // this.notificationService.addMessage(msg);
  //         let pinConfirmationModal = this.modalCtrl.create(CustomModelComponent, { Type: 'PinConformation', Success: false });
  //         pinConfirmationModal.present();
	// 	      this.setWalletPIN();
  //         this.pinVerified = false;
  //         this.pincodeButtonDisabled = false;
  //         this.verifyPinDone = false;
  //         this.setPinDone = false;
  //         this.pincodeButtonLabel = "Set PIN";
  //         this.nextSlideEnabled = (this.pinSubmitted && this.pinVerified);
  //         this.slider.lockSwipeToNext((!this.pinSubmitted || ! this.pinVerified));
  //       }
  //     } else {
  //       this.pincodeButtonDisabled = false;
  //       this.pincodeButtonLabel = "SET";        
  //     }
  //   });
  // }

  finalizeSetup(){
    // create wallet
    try {
      // check users KYC Status
      // this.validateUserDetails(this.jumioResult.scanReference).subscribe( result => {
      //   this.logger.debug("### KYC Status: " + JSON.stringify(result));
      //   if(result == AppConstants.KEY_FINISHED){
          this.createWalletIfNotExists().subscribe( result => {
            this.logger.debug("### SIGNUP - Create Wallet If Not Exists: " + result);
            if(result){
              this.brmService.accountCreated = true;
              // create casinocoin connection
              this.casinocoinService.connect().subscribe(connected => {
                if(connected == AppConstants.KEY_CONNECTED){
                  this.logger.debug("### SIGNUP Connected ###");
                  this.brmService.cscConnected = true;
                  // activate user account on blockchain
                  // this.brmService.activateAccount(this.userProfile).subscribe( result => {
                  //   this.logger.debug("### Account Activation Result: " + JSON.stringify(result));
                  //   if(result['status'] == "OK"){
                      this.brmService.accountActivated = true;
                      this.brmService.kycVerified = true;
                      this.nextSlideEnabled = true;
                      this.slider.lockSwipeToNext(false);
                      this.setupSuccessShow = true;
                      // save created loki db before opening db. auto save takes 5 secs. trying to open db before 5 sec can cause error
                      this.walletService.saveWallet();
                      // set Setup complete
                      this.localStorageService.set(AppConstants.KEY_SETUP_COMPLETED, true);

                      // needs to done

                      // this.brmService.getKycInfoByAccountID(this.userProfile.AccountID).subscribe( result => {
                      //   if(result.Status == 'APPROVED_VERIFIED'){
                      //     this.kycVerified = true;
                      //   } else {
                      //     this.kycVerifiedFailed = true;
                      //   }
                      // });

                  //   } else {
                  //     this.brmService.accountActivatedFailed = true;
                  //   }
                  // });
                  } else if(connected == AppConstants.KEY_DISCONNECTED){
                    this.brmService.cscConnectedFailed = true;
                  }
                });
                // Save Users last state
                this.brmService.saveUser(this.userProfile).subscribe( result => {
                  this.logger.debug("### Save User Result: " + JSON.stringify(result));
                  this.localStorageService.set(AppConstants.KEY_BRM_USER, this.userProfile);
                });
              } else {
                // this.accountCreatedFailed = true;
              }
          });
      //   }
      // });
    } catch (error) {
      this.logger.debug("### Setup Error " + error);
      this.setupErrorShow = true;
    }
    
  }

  // restoreBRM() {
  //   this.navCtrl.setRoot(RestorePage);
  // }

  // backupCodeGenerate() {
  //   let accountID = this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID);
  //   this.backupData = JSON.stringify({
  //     "accountID": accountID,
  //     "accountData": this.walletService.getKey(accountID).secret,
  //     "brmPinData" : this.localStorageService.get(AppConstants.KEY_BRM_PIN),
  //     "walletUUID" : this.localStorageService.get(AppConstants.KEY_CURRENT_WALLET),
  //     "recoveryHash" : this.walletService.getDBMetadata().mnemonicRecovery,
  //     "touchId" : this.localStorageService.get(AppConstants.TOUCH_ID_ENABLE)
  //   });
  // }

  // copyBackupCode() {
  //   this.nextSlideEnabled = true;
  //   this.slider.lockSwipes(false);
  //   console.log("Copy Backup Code "+this.backupData);
  //   this.clipboard.copy(this.backupData);
  //   let toast = this.toastCtrl.create({
  //     message: 'Copied!',
  //     duration: 3000,
  //     position: 'top'
  //   });         
  //   toast.present();
  // }

  showContent(){
    if(this.toggleContent){
      this.toggleContent = false;
    }
    else{
      this.toggleContent = true;
    }
  }
}
