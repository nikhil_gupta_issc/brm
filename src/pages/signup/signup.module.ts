import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from './signup';
import { PincodeInputModule } from  'ionic2-pincode-input';

@NgModule({
  declarations: [],
  imports: [
    IonicPageModule.forChild(SignupPage),
    PincodeInputModule
  ],
})
export class SignupPageModule {

}
