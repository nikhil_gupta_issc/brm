import { Component, OnInit } from '@angular/core';
import { NavController, Platform, ModalController, Nav } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { WalletService } from '../../providers/wallet.service';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { BRMService } from '../../providers/brm.service';
import { AppConstants } from '../../domain/app-constants';
import { LokiAccount } from '../../domain/lokijs';
import { Operator } from '../../domain/brm-api';
import { CSCUtil } from '../../domain/csc-util';
import { OperatorDetailsPage } from '../operator-details/operator-details';
import Big from 'big.js';
import { TransferFundsPage } from '../transfer-funds/transfer-funds';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ItemSliding } from 'ionic-angular/components/item/item-sliding';
import { DepositPage } from '../deposit/deposit';
import { AccountDetailsComponent } from '../../components/account-details/account-details';
import { MarketService } from '../../providers/market.service';
import { LocalStorageService } from 'ngx-store';
import { JumioService } from '../../providers/jumio.service';
import { WebsocketService } from '../../providers/websocket.service';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-operators',
  templateUrl: 'operators.html'
})
export class OperatorsPage implements OnInit {
  
  connectedText: string = "Disconnected";
  connectedColor: string = "disconnected";
  
  defaultAccount: LokiAccount;
  walletBalance: string;
  operators: Operator[] = [];
  
  jumioToken: string = "654da134-d533-4d86-aabe-749568459529";
  jumioSecret: string = "OA9NKY8hW9R0HeX4yaZhrBSh3WINX5TY";

  constructor(public platform: Platform,
              public navCtrl: NavController,
              private nav: Nav,
              public modalCtrl: ModalController,
              private logger: LogService,
              public casinocoinService: CasinocoinService,
              private walletService: WalletService,
              public brmService: BRMService,
              private iab: InAppBrowser,
              public marketService: MarketService,
              public localStorageService: LocalStorageService,
              public jumioService: JumioService,
              public webSocketService: WebsocketService) {
    this.localStorageService.set(AppConstants.LOGGED_IN, true);
  }

  doRefresh(refresher) {
    this.logger.debug("### OPERATORS - Pulled to Refresh  ###");
    this.brmService.getAllOperatorsApi().subscribe( response => {
      this.operators = response;
      this.brmService.currentOperators = response;
      this.localStorageService.set(AppConstants.KEY_BRM_OPERATORS, this.brmService.currentOperators);
      setTimeout(() => {
        refresher.complete();
      }, 500);
    }, error => {
      refresher.complete();
      this.nav.setRoot(LoginPage);
      this.brmService.logOut().present();
    });
  }

  // ionViewDidLoad() {
  //   this.platform.ready().then(() => {
  //     this.logger.debug("### OPERATORS INIT ###");
  //     // Check for Jumio update
  //     if(this.brmService.currentUser.KYCStatus.Status == 'NOT_VERIFIED') {
  //       if(this.brmService.currentUser.KYCStatus.ScanReference){
  //         this.logger.debug("### OperatorsPage - KYC NOT_VERIFIED - Call Jumio api ###");
  //         // this.jumioService.kycVerify(this.jumioToken, this.jumioSecret, this.brmService.currentUser.KYCStatus.ScanReference).subscribe(jumioData => {
  //         this.brmService.getScanResult(this.brmService.currentUser.KYCStatus.ScanReference).subscribe(jumioData => {
  //           this.logger.debug("### Jumio Response: " + JSON.stringify(jumioData));
  //           this.localStorageService.set(AppConstants.JUMIO_RESULT, jumioData);
  //           this.brmService.currentUser.KYCStatus.Status = jumioData.document.status;
  //           this.brmService.currentUser.KYCStatus.ScanSubmittedDate = jumioData.transaction.date;
  //           // this.brmService.currentUser.KYCStatus.VerificationReceivedDate
  //           this.brmService.currentUser.KYCStatus.JumioFirstName = jumioData.document.firstName;
  //           this.brmService.currentUser.KYCStatus.JumioLastName = jumioData.document.lastName;
  //           this.brmService.currentUser.KYCStatus.JumioDOB = jumioData.document.dob;
  //           this.brmService.saveUser(this.brmService.currentUser).subscribe(result => {
  //             this.logger.debug("### Save User with KYC Update " + JSON.stringify(result));
  //           });
  //         });
  //       }
  //     }
  //   });
  // }

  ngOnInit(){
    // subscribe to the openWallet subject
    this.walletService.openWalletSubject.subscribe( result => {
      if(result == AppConstants.KEY_LOADED){
        this.logger.debug("### OPERATORS Wallet Open ###");
        let userAccounts = this.walletService.getAllAccounts();
        this.logger.debug("### OPERATORS Accounts: " + JSON.stringify(userAccounts));
        if(userAccounts.length > 0){
          this.defaultAccount = userAccounts[0];
          // start BRM session
          // this.brmService.startSession(this.defaultAccount.accountID);
          this.doBalanceUpdate();
          // get operators
          this.operators = this.brmService.getAllOperators();
        }
      }
    });
    // connect to casinocoin network
    this.casinocoinService.connect().subscribe(connected => {
      if(connected == AppConstants.KEY_CONNECTED){
        this.logger.debug("### OPERATORS - Casinocoin Connected ###");
      }
    });
    // subscribe to CasinoCoin connections
    this.casinocoinService.casinocoinConnectedSubject.subscribe( connected => {
      if(connected){
        // set connected header
        this.connectedText = "Connected";
        this.connectedColor = "connected";
        // subscribe to account updates
        this.casinocoinService.accountSubject.subscribe( account => {
            this.logger.debug("### OPERATORS Account Update: " + JSON.stringify(account));
            this.doBalanceUpdate();
        });
      } else {
        this.connectedText = "Disconnected";
        this.connectedColor = "disconnected";
      }
    });
    this.brmService.getAllOperatorsApi().subscribe( response => {
      this.operators = response;
      this.brmService.currentOperators = response;
      this.localStorageService.set(AppConstants.KEY_BRM_OPERATORS, this.brmService.currentOperators);
    });
    this.webSocketService.operatorUpdate.subscribe(operators => {
      if(operators){
        this.logger.debug("### OperatorsPage Updating currentOperators ###");
        this.operators = operators;
        this.brmService.currentOperators = operators;
      }
    });
  }

  doBalanceUpdate(){
    // let balance = new Big(this.walletService.getWalletBalance() ? this.walletService.getWalletBalance() : "0");
    let balance = new Big(this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) ? this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) : "0");
    // let balance = new Big(CSCUtil.cscToDrops();
    if(balance) {
      this.logger.debug("### Wallet Balance: " + balance);
      this.casinocoinService.serverStateSubject.subscribe( server =>{
        if(server.complete_ledgers.length > 0){
          let reserve = new Big(server.validated_ledger.reserve_base);
          this.logger.debug("### Account Reserve: " + reserve);
          this.walletBalance = CSCUtil.dropsToCsc(balance.minus(reserve));
          this.logger.debug("### OPERATORS Wallet Balance: " + this.walletBalance);
        }
      });
    }
  }

  openOperatorModal(selectedOperator) {
    let modal = this.modalCtrl.create(OperatorDetailsPage, { operatorID: selectedOperator});
    modal.present();
    modal.onDidDismiss(data => {
      if (data && data.status === 'error') {
        this.nav.setRoot(LoginPage);
        this.brmService.logOut().present();
      }
    });
  }

  operatorBalance(operator){
    let registration = this.brmService.getUserOperatorRegistration(operator.AccountID);
    if(registration !== undefined){
      let userOperatorAccountID = registration.UserAccountIDForOperator;
      let balance = new Big(this.walletService.getAccountBalance(userOperatorAccountID));
      return CSCUtil.dropsToCsc(balance.minus(this.brmService.accountReserveDrops).toString()) + " CSC";
    } else {
      return "Not Activated";
    }
  }

  transferFunds(slidingItem:ItemSliding, operator){
    this.logger.debug("### transferFunds: " + JSON.stringify(operator));
    let registration = this.brmService.getUserOperatorRegistration(operator.AccountID);
    let modal = this.modalCtrl.create(TransferFundsPage, 
      { userOperatorAccountID: registration.UserAccountIDForOperator, 
        defaultAccountID: this.brmService.currentUser.AccountID
      });
    modal.present();
    slidingItem.close();
  }

  visitOperator(slidingItem:ItemSliding, operator:Operator){
    this.logger.debug("### visitOperator: " + JSON.stringify(operator));
    let registration = this.brmService.getUserOperatorRegistration(operator.AccountID);
    this.logger.debug("### visitOperator: " + operator.Weblink);
    let link = operator.Weblink.replace(":action", "user");
    link = link.replace(":accountid", registration.UserAccountIDForOperator);
    this.logger.debug("### BRM Open Link: " + link);
    this.iab.create(link, "_system");
    slidingItem.close();
  }

  deposit(slidingItem:ItemSliding, operator: Operator){
    this.logger.debug("### deposit: " + JSON.stringify(operator));
    let registration = this.brmService.getUserOperatorRegistration(operator.AccountID);
    let modal = this.modalCtrl.create(DepositPage, { 
      userOperatorAccountID: registration.UserAccountIDForOperator,
      operatorAccountID: operator.AccountID,
      destinationTag: registration.DestinationTag
    });
    modal.present();
    slidingItem.close();
  }

  signup(slidingItem:ItemSliding, operator: Operator){
    this.openOperatorModal(operator.OperatorID);
    slidingItem.close();
  }

  accountDetails(){
    let profileModal = this.modalCtrl.create(AccountDetailsComponent);
    profileModal.present();
  }

}
