import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WalletService } from '../../providers/wallet.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
	versionNumber: any;

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				private walletService: WalletService,
				private iab: InAppBrowser) {
		this.versionNumber = this.walletService.appVersionString;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
	}
	
	onClick(option) {
		if (option === "TofS") {
			this.iab.create("https://casinocoin.org/terms-of-service/", "_system");
		} else {
			this.iab.create("https://casinocoin.org/privacy-policy/", "_system");
		}
	}

}
