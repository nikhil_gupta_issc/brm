import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { WalletService } from '../../providers/wallet.service';
import { BRMService } from '../../providers/brm.service';
import { PincodeController } from 'ionic2-pincode-input';
import { CSCCrypto } from '../../domain/csc-crypto';
import { LocalStorageService } from 'ngx-store';
import { AppConstants } from '../../domain/app-constants';
import { CustomModalComponent } from '../../components/custom-modal/custom-modal';

@IonicPage()
@Component({
  selector: 'page-change-pin',
  templateUrl: 'change-pin.html',
})
export class ChangePinPage {

  loader: any;
  pinCode: string;
  decryptedPIN = "";
  accountID: string = "";
  emailAsSalt: string = "";
  enteredPinCode: string = "";
  setPinDone: boolean = false;
  currentPinDone: boolean = false;
  showPinDialog: boolean = false;
  recoveryMnemonicWords: Array<string>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public logger: LogService,
    public walletService: WalletService,
    public brmService: BRMService,
    public pincodeCtrl: PincodeController,
    public localStorageService: LocalStorageService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public modalCtrl: ModalController) {
      this.showPinDialog = true;
      this.accountID = this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID);
      this.emailAsSalt = this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress;
      this.logger.debug("### ChangePinPage - AccountID: " + this.accountID + ", Email: " + this.emailAsSalt);
  }

  handlePinInput(pin: string) {
    this.enteredPinCode += pin;
    if (this.enteredPinCode.length === 6) {
      this.validatePincode();
    }
  }

  backspacePin() {
    this.enteredPinCode = this.enteredPinCode.substring(0, this.enteredPinCode.length - 1);
  }

  clearPin() {
    this.enteredPinCode = "";
  }

  validatePincode() {
    this.showPinDialog = false;
    if (!this.currentPinDone) {
      this.logger.debug("### Current PIN: " + this.enteredPinCode);
      let loader = this.loadingCtrl.create({spinner: 'crescent', content: 'Validating ...', duration: 60000});
      loader.present().then(() => {
        try {
          let cscCrypto = new CSCCrypto(this.accountID, this.emailAsSalt);
          let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
          this.decryptedPIN = cscCrypto.decrypt(storedPIN);
        } catch (e) {
          this.logger.debug("### Decrypting PIN Went wrong ###");
          this.decryptedPIN = this.enteredPinCode;
        }
        if(this.enteredPinCode === this.decryptedPIN){
          loader.setContent("Validated");
          this.currentPinDone = true;
        } else {
          let alert = this.alertCtrl.create({
            title: 'Invalid PIN',
            subTitle: 'You entered an invalid PIN. Please retry!',
            buttons: ['Ok']
          });
          alert.present();
        }
        loader.dismiss();
        this.enteredPinCode = "";          
        setTimeout(() => {
          this.showPinDialog = true;
        }, 100);
      });
    } else if (this.setPinDone) {
      if (this.pinCode === this.enteredPinCode) {
        this.logger.debug("### Confirm PIN: " + this.enteredPinCode);
        this.walletService.walletPIN = this.pinCode;
        let loader = this.loadingCtrl.create({spinner: 'crescent', content: 'Encrypting Wallet with new PIN Code', duration: 60000});
        loader.present().then(() => {
          let cscCrypto = new CSCCrypto(this.accountID, this.emailAsSalt);
          let encryptedPIN = cscCrypto.encrypt(this.pinCode);
          this.localStorageService.set(AppConstants.KEY_BRM_PIN, encryptedPIN);
          this.walletService.walletPIN = this.pinCode;
          // update password with existing Mnemonic words
          this.recoveryMnemonicWords = JSON.parse(cscCrypto.decrypt(this.localStorageService.get(AppConstants.KEY_WALLET_MNEMONIC_WORDS)));
          loader.setContent("This may take a while");
          let mnemonicHash = new CSCCrypto(this.recoveryMnemonicWords, this.emailAsSalt).encrypt(this.pinCode);
          this.walletService.changePin(this.decryptedPIN, this.pinCode, mnemonicHash);
          let alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: 'Your PIN has been changed Successfully.',
            buttons: ['Ok']
          });
          alert.onDidDismiss(() => {
            this.navCtrl.pop();
          });
          loader.dismiss();
          alert.present();
        });
      } else {
        this.logger.debug("### PIN doesn't Match: " + this.pinCode + ", " + this.enteredPinCode);
        let pinConfirmationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'PinConformation', Success: false });
        pinConfirmationModal.present();
        pinConfirmationModal.onDidDismiss(() => {
          setTimeout(() => {
            this.showPinDialog = true;
          }, 100);
        });
        this.enteredPinCode = "";
        this.setPinDone = false;
      }
    } else {
      this.logger.debug("### Entered PIN: " + this.enteredPinCode);
      this.pinCode = this.enteredPinCode;
      this.enteredPinCode = "";
      this.setPinDone = true;
      setTimeout(() => {
        this.showPinDialog = true;
      }, 100);
    }
  }

  // changePin() {    
  //   this.logger.debug("### Change PIN ");
  //   let pinCode =  this.pincodeCtrl.create({
  //     title:'Current BRM Pincode',
  //     hideForgotPassword: true,
  //     enableBackdropDismiss: false
  //   });
  //   pinCode.present();
  //   pinCode.onDidDismiss( (code,status) => {
  //     if(status === 'done'){
  //       this.navCtrl.pop();
  //       this.logger.debug("### PIN: " + JSON.stringify(code));            
  //       let userAccounts = this.walletService.getAllAccounts();
  //       this.logger.debug("### OPERATORS Accounts: " + JSON.stringify(userAccounts));
  //       if(userAccounts.length > 0){
  //         this.defaultAccount = userAccounts[0];
  //       }
  //       try {
  //         let cscCrypto = new CSCCrypto(this.defaultAccount.accountID, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
  //         let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
  //         this.decryptedPIN = cscCrypto.decrypt(storedPIN);
  //       } catch (e) {
  //           this.logger.debug("### Decrypting PIN Went wrong ###");
  //           this.decryptedPIN = code;
  //       }
  //       if(code === this.decryptedPIN){
  //         this.setWalletPIN();
  //       } else {
  //         let alert = this.alertCtrl.create({
  //           title: 'Invalid PIN',
  //           subTitle: 'You entered an invalid PIN. Please retry!',
  //           buttons: ['Dismiss']
  //         });
  //         alert.onDidDismiss(() => {
  //           this.changePin();
  //         });
  //         alert.present();
  //       }
  //     } else if (status === 'forgot'){
  //       this.logger.debug("### Forgot Password");
  //     }
  //   });  
  // }

  // setWalletPIN(){
  //   let pinCode =  this.pincodeCtrl.create({
  //     title:'New BRM Pincode',
  //     hideForgotPassword: true,
  //     enableBackdropDismiss: false
  //   });
  //   pinCode.present();
  //   pinCode.onDidDismiss( (code,status) => {
  //     if(status === 'done'){
  //       this.pin = code;
  //       this.logger.debug("### PIN: " + JSON.stringify(this.pin));
  //       this.verifyWalletPIN();
  //     } else if (status === 'forgot'){
  //       this.logger.debug("### Forgot Password");
  //     }
  //   });
  // }

  // verifyWalletPIN(){
  //   let pinCode =  this.pincodeCtrl.create({
  //     title:'Verify Pincode',
  //     hideForgotPassword: true,
  //     enableBackdropDismiss: false
  //   });
  //   pinCode.present();
  //   pinCode.onDidDismiss( (code,status) => {
  //     if(status === 'done'){
  //       this.logger.debug("### Verify PIN: " + JSON.stringify(code));
  //       if(this.pin === code){
  //         let cscCrypto = new CSCCrypto(this.defaultAccount.accountID, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
  //         let encryptedPIN = cscCrypto.encrypt(code);
  //         this.localStorageService.set(AppConstants.KEY_BRM_PIN, encryptedPIN);
  //         this.walletService.walletPIN = code;
  //         this.recoveryMnemonicWords = CSCCrypto.getRandomMnemonic();
  //         let mnemonicHash = new CSCCrypto(this.recoveryMnemonicWords, this.brmService.getUser().Emailaddress).encrypt(code);
  //         this.walletService.changePin(this.decryptedPIN, code, mnemonicHash);
  //         let alert = this.alertCtrl.create({
  //           title: 'Success',
  //           subTitle: 'Your PIN has been changed Successfully.',
  //           buttons: ['Ok']
  //         });
  //         alert.onDidDismiss(() => {
  //           this.mnemonicShow = true;
  //           // this.screenShot();
  //         });
  //         alert.present();          
  //       } else {
  //         let alert = this.alertCtrl.create({
  //           title: 'PIN not Match',
  //           subTitle: 'Your PIN do not match. Please retry!',
  //           buttons: ['Dismiss']
  //         });
  //         alert.onDidDismiss(() => {
  //           this.setWalletPIN();
  //         });
  //         alert.present();
  //       }
  //       this.logger.debug("### Stored PIN: " + code );
  //     }
  //   });
  // }

}
