import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecoveryPhrasePage } from './recovery-phrase';

@NgModule({
  declarations: [ ],
  imports: [
    IonicPageModule.forChild(RecoveryPhrasePage),
  ],
})
export class RecoveryPhrasePageModule {}
