import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { LocalStorageService } from 'ngx-store';
import { CSCCrypto } from '../../domain/csc-crypto';
import { AppConstants } from '../../domain/app-constants';
import { WalletService } from '../../providers/wallet.service';


@IonicPage()
@Component({
  selector: 'page-recovery-phrase',
  templateUrl: 'recovery-phrase.html',
})
export class RecoveryPhrasePage {
  accountID: string = "";
  emailAsSalt: string = "";
  enteredPinCode: string = "";
  showPinDialog: boolean = false;
  showWords: boolean = false;
  recoveryMnemonicWords: Array<string>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public logger: LogService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public localStorageService: LocalStorageService,
    public walletService: WalletService) {

    this.showPinDialog = true;
    this.accountID = this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID);
    this.emailAsSalt = this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress;
    this.logger.debug("### RecoveryPhrasePage - AccountID: " + this.accountID + ", Email: " + this.emailAsSalt)
  }

  ionViewDidLoad() {
    window['plugins'].preventscreenshot.disable(result => {
      this.logger.debug("### Signup Screen Shot Disabled: " + result);
    }, error => {
      this.logger.debug("### Signup Screen Shot Error: " + error);
    });
  }

  ionViewDidLeave() {
    window['plugins'].preventscreenshot.enable(result => {
      this.logger.debug("### Signup Screen Shot Enabled: " + result);
    }, error => {
      this.logger.debug("### Signup Screen Shot Error: " + error);
    });
  }

  onDismiss() {
    this.navCtrl.pop();
  }

  handlePinInput(pin: string) {
    this.enteredPinCode += pin;
    if (this.enteredPinCode.length === 6) {
      this.validatePincode();
    }
  }

  backspacePin() {
    this.enteredPinCode = this.enteredPinCode.substring(0, this.enteredPinCode.length - 1);
  }

  clearPin() {
    this.enteredPinCode = "";
  }

  validatePincode() {
    this.showPinDialog = false;
    this.logger.debug("### Entered PIN: " + this.enteredPinCode);
    if (this.walletService.walletPIN == this.enteredPinCode) {
      let loader = this.loadingCtrl.create({ spinner: 'crescent', content: 'Fetching Recovery Phrase ...', duration: 60000 });
      loader.present().then(() => {
        let cscCrypto = new CSCCrypto(this.accountID, this.emailAsSalt);
        let decryptedWords = cscCrypto.decrypt(this.localStorageService.get(AppConstants.KEY_WALLET_MNEMONIC_WORDS));
        this.recoveryMnemonicWords = JSON.parse(decryptedWords);
        this.showWords = true;
        loader.dismiss();
      });
    } else {
      let alert = this.alertCtrl.create({
        title: 'Invalid PIN',
        subTitle: 'You entered an invalid PIN. Please retry!',
        buttons: ['Ok']
      });
      alert.present();
      this.enteredPinCode = "";
      setTimeout(() => {
        this.showPinDialog = true;
      }, 100);
    }
  }

}
