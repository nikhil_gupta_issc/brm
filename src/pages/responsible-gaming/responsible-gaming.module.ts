import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResponsibleGamingPage } from './responsible-gaming';

@NgModule({
  declarations: [ ],
  imports: [
    IonicPageModule.forChild(ResponsibleGamingPage),
  ],
})
export class ResponsibleGamingPageModule {}
