import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, PopoverController, Nav } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { BRMService } from '../../providers/brm.service';
import { User, Operator, GamingLimit } from '../../domain/brm-api';
import { CSCUtil } from '../../domain/csc-util';
import Big from 'big.js';
import { Observable } from 'rxjs';
import { WebsocketService } from '../../providers/websocket.service';
import { ResponsibleGamingPopoverComponent } from '../../components/responsible-gaming-popover/responsible-gaming-popover';
import { LocalStorageService } from 'ngx-store';
import { AppConstants } from '../../domain/app-constants';
import { DatePipe } from '@angular/common';
import { LoginPage } from '../login/login';

export interface ShowLimits {
  value24H: number;
  enabled24H: boolean;
  timestamp24H: number;
  value7D: number;
  enabled7D: boolean;
  timestamp7D:number;
  value30D: number;
  enabled30D: boolean;
  timestamp30D: number;
};

@IonicPage()
@Component({
  selector: 'page-responsible-gaming',
  templateUrl: 'responsible-gaming.html',
})
export class ResponsibleGamingPage {

  operators: Array<Operator>;
  currentUser:User;
  currentLimitsType: string;
  previousLimitsType: string;
  currentLimits: Array<GamingLimit> = [];
  currentOperator: string;
  showLimits: ShowLimits;
  oldLimits: ShowLimits;
  message = "";
  saveMessage = "";
  selfExcludedTime: number = 1;
  selfExclusionText: string = '';
  selfExclusionTemporaryDisabled: boolean = false;
  selfExclusionMinutesTemp: number = 0;
  selfExclusionTimer: any;
  selfExclusionDays: number = 0;
  selfExclusionHours: number = 0;
  selfExclusionMinutes: number = 0;
  selfExclusionSeconds: number = 0;
  selfExclusionTime: number = null;
  popoverArrow: boolean = true;
  disableSave: boolean = true;
  selectedCasino: string = "All Casinos";
  selectedOperators: any;

  limitSetOn: string = "";
  limitExpiresOn: string = "";
  limitDuration: string = "";
  limitSetFor: string = "";

  activeColor: string = "active";
  inactiveColor: string = "inactive";

  constructor( public navCtrl: NavController, 
               public navParams: NavParams,
               private nav: Nav,
               public logger: LogService,
               public brmService: BRMService,
               public alertCtrl: AlertController,
               public webSocketService: WebsocketService,
               public popoverCtrl: PopoverController,
               public localStorageService: LocalStorageService,
               public datepipe: DatePipe ) {
    this.operators = this.brmService.getAllOperators();
    // this.currentUser =  this.localStorageService.get(AppConstants.KEY_BRM_USER);    
    this.currentUser = {...this.brmService.userWithUpdatedLimitsTime()};
    if(this.currentUser.UserGamingLimits == undefined){
      this.currentUser.UserGamingLimits = {
          AllEnabled: true,
          All24H: 0,
          All24HEnabled: false,
          All24Timestamp: 0,
          All7D: 0,
          All7DEnabled: false,
          All7DTimestamp: 0,
          All30D: 0,
          All30DEnabled: false,
          All30DTimestamp: 0,
          SelfExclusion: false,
          SelfExclusionTimestamp: 0,
          OperatorLimits: []
      }
    }
    this.logger.debug("### Limits currentUser: " + JSON.stringify(this.currentUser.UserGamingLimits));
    this.currentLimitsType = "all";
    this.previousLimitsType = "all";
    this.showLimits = {
      enabled24H: this.currentUser.UserGamingLimits.All24HEnabled,
      value24H: this.currentUser.UserGamingLimits.All24H,
      timestamp24H: this.currentUser.UserGamingLimits.All24Timestamp,
      enabled7D: this.currentUser.UserGamingLimits.All7DEnabled,
      value7D: this.currentUser.UserGamingLimits.All7D,
      timestamp7D: this.currentUser.UserGamingLimits.All7DTimestamp,
      enabled30D: this.currentUser.UserGamingLimits.All30DEnabled,
      value30D: this.currentUser.UserGamingLimits.All30D,
      timestamp30D: this.currentUser.UserGamingLimits.All30DTimestamp
    }
    this.oldLimits = {...this.showLimits};
    this.showLimitExpireStatus();
    if(this.currentUser.UserGamingLimits.SelfExclusion) {
      this.selfExclusionTime = this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes;
      if(this.selfExclusionTime == 1000000) {
        this.selfExclusionText = "You have opted for self exclusion permanently";
      } else {
        let timestamp = CSCUtil.casinocoinTimeToISO8601(this.currentUser.UserGamingLimits.SelfExclusionTimestamp);
        let compareDate = new Date(timestamp);
        compareDate.setMinutes(compareDate.getMinutes() + this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
        this.selfExclusionTimer = Observable.interval(1000).subscribe((val) => {   
          this.startTimer(compareDate);
        });
      }
    }
    // if(this.currentUser.UserGamingLimits.SelfExclusion){
    //   this.selfExclusionTemporaryDisabled = true;
    //   this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = true;
    //   this.currentUser.UserGamingLimits.SelfExclusionPermanentCheck = false;
    //   this.selfExclusionMinutesTemp = this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes;
    //   this.changeSelfExclusionDurationRange(this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
    //   let timestamp = CSCUtil.casinocoinTimeToISO8601(this.currentUser.UserGamingLimits.SelfExclusionTimestamp);
    //   let compareDate = new Date(timestamp);
    //   compareDate.setMinutes(compareDate.getMinutes() + this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
    //   this.selfExclusionTimer = Observable.interval(1000).subscribe((val) => {   
    //     this.startTimer(compareDate);
    //   });
    // }
    // if(this.currentUser.UserGamingLimits.SelfExclusionPermanent) {  
    //   this.selfExclusionTemporaryDisabled = true;
    //   this.selfExclusionText = "You have opted for self exclusion Permanent.";
    // }  
      
    // subscribe to User Update
    this.webSocketService.userUpdate.subscribe(user => {
      if(user){
        if(user.UserGamingLimits) {
          this.logger.debug("### ResponsibleGamingPage Updating currentUser ###");
          this.currentUser = user;
          if(this.currentUser.UserGamingLimits.SelfExclusion) {
            this.selfExclusionTime = this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes;
            if(this.selfExclusionTime == 1000000) {
              this.selfExclusionText = "You have opted for self exclusion permanently";
            } else {
              let timestamp = CSCUtil.casinocoinTimeToISO8601(this.currentUser.UserGamingLimits.SelfExclusionTimestamp);
              let compareDate = new Date(timestamp);
              compareDate.setMinutes(compareDate.getMinutes() + this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
              this.selfExclusionTimer = Observable.interval(1000).subscribe((val) => {   
                this.startTimer(compareDate);
              });
            }
          } else {
            this.selfExclusionText = "";
            this.selfExclusionTime = null;
            if(this.selfExclusionTimer){
              this.selfExclusionTimer.unsubscribe();
            }
          }
        }

        // if(user.UserGamingLimits.SelfExclusion){
        //   this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = true;
        //   this.selfExclusionTemporaryDisabled = true;
        //   if(this.selfExclusionTimer){
        //     this.selfExclusionTimer.unsubscribe();
        //   }
        //   this.selfExclusionMinutesTemp = this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes;
        //   this.changeSelfExclusionDurationRange(this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
        //   let timestamp = CSCUtil.casinocoinTimeToISO8601(this.currentUser.UserGamingLimits.SelfExclusionTimestamp);
        //   let compareDate = new Date(timestamp);
        //   compareDate.setMinutes(compareDate.getMinutes() + this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
        //   this.selfExclusionTimer = Observable.interval(1000).subscribe((val) => {   
        //     this.startTimer(compareDate);
        //   });
        // } else {
        //   this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = false;
        //   this.selfExclusionTemporaryDisabled = false;
        //   this.selfExclusionText = "";
        //   if(this.selfExclusionTimer){
        //     this.selfExclusionTimer.unsubscribe();
        //   }
        // }
        // if(user.UserGamingLimits.SelfExclusionPermanent){
        //   this.selfExclusionTemporaryDisabled = true;
        //   this.currentUser.UserGamingLimits.SelfExclusionPermanentCheck = true;
        // } else {
        //   this.currentUser.UserGamingLimits.SelfExclusionPermanentCheck = false;
        // }
      }
    });
  }

  startTimer(toDate) {
    let dateEntered = toDate;
    let now = new Date();
    let difference = dateEntered.getTime() - now.getTime();  
    if (difference <= 0) {  
      // Timer done
      this.selfExclusionTimer.unsubscribe();
      this.selfExclusionText = "";
      this.selfExclusionTemporaryDisabled = false;
      this.currentUser.UserGamingLimits.SelfExclusion = false;
      this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = false;
      this.brmService.saveUser(this.currentUser).subscribe( result => {
        this.logger.debug("### User Updated ###");
      });
    } else {      
      this.selfExclusionSeconds = Math.floor(difference / 1000);
      this.selfExclusionMinutes = Math.floor(this.selfExclusionSeconds / 60);
      this.selfExclusionHours = Math.floor(this.selfExclusionMinutes / 60);
      this.selfExclusionDays = Math.floor(this.selfExclusionHours / 24);
  
      this.selfExclusionHours %= 24;
      this.selfExclusionMinutes %= 60;
      this.selfExclusionSeconds %= 60;
      this.selfExclusionText = "You have opted for self exclusion till - " + this.selfExclusionDays + " days " + this.selfExclusionHours + " hours " + this.selfExclusionMinutes + " mins " + this.selfExclusionSeconds + " secs";
    }
  }

  showLimitExpireStatus() {
    if (this.showLimits.enabled24H) {
      this.limitSetOn = this.datepipe.transform(CSCUtil.casinocoinTimeToISO8601(this.showLimits.timestamp24H), 'dd-MMM-yyyy');
      this.limitExpiresOn = this.datepipe.transform(new Date(CSCUtil.casinocoinToUnixTimestamp(this.showLimits.timestamp24H) + (86400 * 1000)), 'dd-MMM-yyyy');
      this.limitDuration = "24 Hours";
      this.limitSetFor = this.showLimits.value24H + " CSC";
    } else if (this.showLimits.enabled7D) {
      this.limitSetOn = this.datepipe.transform(CSCUtil.casinocoinTimeToISO8601(this.showLimits.timestamp7D), 'dd-MMM-yyyy');
      this.limitExpiresOn = this.datepipe.transform(new Date(CSCUtil.casinocoinToUnixTimestamp(this.showLimits.timestamp7D) + (604800 * 1000)), 'dd-MMM-yyyy');
      this.limitDuration = "7 Days";
      this.limitSetFor = this.showLimits.value7D + " CSC";
    } else if (this.showLimits.enabled30D) {
      this.limitSetOn = this.datepipe.transform(CSCUtil.casinocoinTimeToISO8601(this.showLimits.timestamp30D), 'dd-MMM-yyyy');
      this.limitExpiresOn = this.datepipe.transform(new Date(CSCUtil.casinocoinToUnixTimestamp(this.showLimits.timestamp30D) + (2592000 * 1000)), 'dd-MMM-yyyy');
      this.limitDuration = "30 Days";
      this.limitSetFor = this.showLimits.value30D + " CSC";
    }
  }

  onLimitInputFocus(inputField: string) {
    if (inputField == "24H") {
      if (this.showLimits.value24H === 0) {
        this.showLimits.value24H = null;
      }
    }
    if (inputField == "7D") {
      if (this.showLimits.value7D === 0) {
        this.showLimits.value7D = null;
      }
    }
    if (inputField == "30D") {
      if (this.showLimits.value30D === 0) {
        this.showLimits.value30D = null;
      }
    }
  }

  onLimitInputBlur(inputField: string) {
    if (inputField == "24H") {
      if (this.showLimits.value24H == null || this.showLimits.value24H.toString() == "") {
        this.showLimits.value24H = 0;
      }
    }
    if (inputField == "7D") {
      if (this.showLimits.value7D == null || this.showLimits.value7D.toString() == "") {
        this.showLimits.value7D = 0;
      }
    }
    if (inputField == "30D") {
      if (this.showLimits.value30D == null || this.showLimits.value30D.toString() == "") {
        this.showLimits.value30D = 0;
      }
    }
  }

  ionViewDidLeave() {
    this.brmService.currentUser = this.localStorageService.get(AppConstants.KEY_BRM_USER);
    if(this.currentUser.UserGamingLimits.SelfExclusion) {
      if(this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes != 1000000) {
        this.selfExclusionTimer.unsubscribe();
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResponsibleGamingPage');
  }

  onLimitsTypeChange(){
    this.logger.debug("### Limits Type Change - old: " + this.previousLimitsType + " new: " + this.currentLimitsType);
    // save current limits
    this.saveLimits();
    // this.saveUser();
    // get operator for new limits type
    if(this.currentLimitsType != "all" && this.currentLimitsType != "multi"){
      // get operator limits
      let gamblingLimitsindex = this.currentUser.UserGamingLimits.OperatorLimits.findIndex(x => x.OperatorID === this.currentLimitsType);
      let operator = this.operators.find(x => x.OperatorID === this.currentLimitsType);
      if(operator != undefined){
        this.currentOperator = operator.DisplayName;
      }
      this.logger.debug("### Operator ID: " + operator.OperatorID + " Operator Name " + operator.DisplayName);
      if(gamblingLimitsindex < 0){
        this.showLimits.enabled24H = false;
        this.showLimits.value24H = 0;
        this.showLimits.timestamp24H = 0;
        this.showLimits.enabled7D = false;
        this.showLimits.value7D = 0;
        this.showLimits.timestamp7D = 0;
        this.showLimits.enabled30D = false;
        this.showLimits.value30D = 0;
        this.showLimits.timestamp30D = 0;
      } else {
        this.logger.debug("### Setting single operator limits");
        this.showLimits.enabled24H = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled24H;
        this.showLimits.value24H = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value24H;
        this.showLimits.timestamp24H = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet24H;
        this.showLimits.enabled7D = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled7D;
        this.showLimits.value7D = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value7D;
        this.showLimits.timestamp7D = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet7D;
        this.showLimits.enabled30D = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled30D;
        this.showLimits.value30D = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value30D;
        this.showLimits.timestamp30D = this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet30D;
      }
    } else {
      // get 'all' limits
      this.logger.debug("### Setting All Limits");
      this.showLimits.enabled24H = this.currentUser.UserGamingLimits.All24HEnabled;
      this.showLimits.value24H = this.currentUser.UserGamingLimits.All24H;
      this.showLimits.timestamp24H = this.currentUser.UserGamingLimits.All24Timestamp;
      this.showLimits.enabled7D = this.currentUser.UserGamingLimits.All7DEnabled;
      this.showLimits.value7D = this.currentUser.UserGamingLimits.All7D;
      this.showLimits.timestamp7D = this.currentUser.UserGamingLimits.All7DTimestamp;
      this.showLimits.enabled30D = this.currentUser.UserGamingLimits.All30DEnabled;
      this.showLimits.value30D = this.currentUser.UserGamingLimits.All30D;
      this.showLimits.timestamp30D = this.currentUser.UserGamingLimits.All30DTimestamp;
    }
    this.previousLimitsType = this.currentLimitsType;
    this.logger.debug("TypeChange Old Limits Values: " + JSON.stringify(this.oldLimits));
    this.logger.debug("TypeChange New Limits Values: " + JSON.stringify(this.showLimits));
    this.oldLimits = {...this.showLimits};
    this.showLimitExpireStatus();
  }

  onLimitValuesChange(){
    if (this.oldLimits.enabled24H != this.showLimits.enabled24H ||
        this.oldLimits.enabled7D != this.showLimits.enabled7D ||
        this.oldLimits.enabled30D != this.showLimits.enabled30D ||
        this.oldLimits.value24H !== this.showLimits.value24H ||
        this.oldLimits.value7D !== this.showLimits.value7D ||
        this.oldLimits.value30D !== this.showLimits.value30D) {
      this.disableSave = false;
      if (this.showLimits.value24H == null ||
          this.showLimits.value7D == null ||
          this.showLimits.value30D == null ||
          this.showLimits.value24H.toString() == "" ||
          this.showLimits.value7D.toString() == "" ||
          this.showLimits.value30D.toString() == "") {
        this.disableSave = true;
      }
    } else {
      this.disableSave = true;
    }
    // add timestamp if newly inserted
    if(this.showLimits.enabled24H) !this.showLimits.timestamp24H ? this.showLimits.timestamp24H = CSCUtil.casinocoinTimeNow() : '';
    if(this.showLimits.enabled7D) !this.showLimits.timestamp7D ? this.showLimits.timestamp7D = CSCUtil.casinocoinTimeNow() : '';
    if(this.showLimits.enabled30D) !this.showLimits.timestamp30D ? this.showLimits.timestamp30D = CSCUtil.casinocoinTimeNow() : '';

    // toggle to true if value entered
    // this.showLimits.value24H && new Big(this.showLimits.value24H).gte(0) ? this.showLimits.enabled24H = true : this.showLimits.enabled24H = false;
    // this.showLimits.value7D && new Big(this.showLimits.value7D).gte(0) ? this.showLimits.enabled7D = true : this.showLimits.enabled7D = false;
    // this.showLimits.value30D && new Big(this.showLimits.value30D).gte(0) ? this.showLimits.enabled30D = true : this.showLimits.enabled30D = false;

    // check increasing limits
    // if(this.showLimits.enabled24H && this.showLimits.enabled7D){
    //   // 7D >= 24H
    //   if(new Big(this.showLimits.value7D).lt(new Big(this.showLimits.value24H))){
    //     this.showLimits.value7D = this.showLimits.value24H;
    //   }
    // }
    // if(this.showLimits.enabled30D && (this.showLimits.enabled24H || this.showLimits.enabled7D)){
    //   // 30D >= 24H or 30D >= 7D
    //   if(this.showLimits.enabled7D && (new Big(this.showLimits.value30D).lt(new Big(this.showLimits.value7D)))){
    //     this.showLimits.value30D = this.showLimits.value7D;
    //   } else if(this.showLimits.enabled24H && (new Big(this.showLimits.value30D).lt(new Big(this.showLimits.value24H)))){
    //     this.showLimits.value30D = this.showLimits.value24H;
    //   }
    // }
    // save new limits
    // this.saveLimits();
    // this.updateUser();
  }

  saveLimits(){
    if(this.previousLimitsType == 'all'){
      // save on all
      this.currentUser.UserGamingLimits.All24H = this.showLimits.value24H;
      this.currentUser.UserGamingLimits.All24HEnabled = this.showLimits.enabled24H;
      this.currentUser.UserGamingLimits.All24Timestamp = this.showLimits.timestamp24H;
      this.currentUser.UserGamingLimits.All7D = this.showLimits.value7D;
      this.currentUser.UserGamingLimits.All7DEnabled = this.showLimits.enabled7D;
      this.currentUser.UserGamingLimits.All7DTimestamp = this.showLimits.timestamp7D;
      this.currentUser.UserGamingLimits.All30D = this.showLimits.value30D;
      this.currentUser.UserGamingLimits.All30DEnabled = this.showLimits.enabled30D;
      this.currentUser.UserGamingLimits.All30DTimestamp = this.showLimits.timestamp30D;
    } else {
      if(this.previousLimitsType != 'multi') {
        let gamblingLimitsindex = this.currentUser.UserGamingLimits.OperatorLimits.findIndex(x => x.OperatorID === this.previousLimitsType);
        if(gamblingLimitsindex < 0){
          // add new limits item
          let newLimits: GamingLimit = {
            Value24H: this.showLimits.value24H,
            Enabled24H: this.showLimits.enabled24H,
            TimestampSet24H: this.showLimits.timestamp24H,
            Value7D: this.showLimits.value7D,
            Enabled7D: this.showLimits.enabled7D,
            TimestampSet7D: this.showLimits.timestamp7D,
            Value30D: this.showLimits.value30D,
            Enabled30D: this.showLimits.enabled30D,
            TimestampSet30D: this.showLimits.timestamp30D,
            OperatorID: this.previousLimitsType,
            OperatorName: this.currentOperator
          }
          this.currentUser.UserGamingLimits.OperatorLimits.push(newLimits);
        } else {
          //update existing item
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value24H = this.showLimits.value24H;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled24H = this.showLimits.enabled24H;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet24H = this.showLimits.timestamp24H;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value7D = this.showLimits.value7D;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled7D = this.showLimits.enabled7D;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet7D = this.showLimits.timestamp7D;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value30D = this.showLimits.value30D;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled30D = this.showLimits.enabled30D;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet30D = this.showLimits.timestamp30D;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].OperatorID = this.previousLimitsType;
          this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].OperatorName = this.currentOperator;
        }
      }
    }
  }

  showPopover(myEvent) {
    if(this.disableSave) {
      this.popoverArrow = false;
      let popover: any;
      // if(this.currentUser.UserGamingLimits.OperatorLimits.length){
      //   popover = this.popoverCtrl.create(ResponsibleGamingPopoverComponent, { showCheckbox: false });
      // } else {
      //   if (!this.brmService.currentUser.UserGamingLimits) {
      //     popover = this.popoverCtrl.create(ResponsibleGamingPopoverComponent, { showCheckbox: true });
      //   } else {
      //     popover = this.popoverCtrl.create(ResponsibleGamingPopoverComponent, { showCheckbox: false });
      //   }
      // }
      if (!this.brmService.currentUser.UserGamingLimits) {
        popover = this.popoverCtrl.create(ResponsibleGamingPopoverComponent, { showCheckbox: true });
      } else {
        if (this.currentUser.UserGamingLimits.OperatorLimits.length) {
          popover = this.popoverCtrl.create(ResponsibleGamingPopoverComponent, { showCheckbox: false });
        } else {
          popover = this.popoverCtrl.create(ResponsibleGamingPopoverComponent, { showCheckbox: true });
        }
      }
      popover.present({
        ev: myEvent
      });   
      popover.onDidDismiss((data) => {
        this.popoverArrow = true;
        if(Array.isArray(data)) {
          if(data.length) {
            if(data.length == 1) {
              this.selectedOperators = data;
              data.forEach(operator => { 
                this.selectedCasino = operator.DisplayName;
                this.currentLimitsType = operator.OperatorID;
              });
            } else if(data.length === this.operators.length) {
              this.selectedCasino = "All Casinos";
              this.currentLimitsType = "all";
            } else {
              this.selectedCasino = "";
              this.currentLimitsType = "multi";
              this.selectedOperators = data;
              data.forEach(operator => { this.selectedCasino += operator.DisplayName + ", "; });
              this.selectedCasino = this.selectedCasino.substr(0, this.selectedCasino.length - 2);
            }
          } else {
            this.selectedCasino = "All Casinos";
            this.currentLimitsType = "all";
          }
        } else {
          if(data) {
            this.selectedCasino = data.DisplayName;
            this.currentLimitsType = data.OperatorID;
          } else {
            this.selectedCasino = "All Casinos";
            this.currentLimitsType = "all";
          }
        }
        this.onLimitsTypeChange();        
      });
    } else {
      let alert = this.alertCtrl.create({
        subTitle: "Please Save or Cancel before changing Operator.",
        enableBackdropDismiss: false,
        buttons: ['Ok']
      });
      alert.present();
    }
  }

  selfExclusionSelect() {
    if(this.brmService.currentUser.UserGamingLimits){
      if(this.brmService.currentUser.UserGamingLimits.SelfExclusion) {
        if(this.brmService.currentUser.UserGamingLimits.SelfExclusionDurationMinutes > this.selfExclusionTime) {
          let alert = this.alertCtrl.create({
            title: "Not possible to reduce Timer.",
            buttons: ['Ok']
          });
          alert.present();
          alert.onDidDismiss(()=> {
            this.selfExclusionTime = this.brmService.currentUser.UserGamingLimits.SelfExclusionDurationMinutes;
          });
        } else {
          this.showAlert();
        }
      } else {
        this.showAlert();
      }
    } else {
      this.showAlert();
    }
  }

  showAlert() {
    if(this.selfExclusionTime){
      let alert = this.alertCtrl.create({
        title: "Are you sure?",
        subTitle: "This cannot be reversed under any circumstances.",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.disableSave = false;
              this.currentUser.UserGamingLimits.SelfExclusion = true;
              this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = parseInt(this.selfExclusionTime.toString());
              this.currentUser.UserGamingLimits.SelfExclusionTimestamp = CSCUtil.casinocoinTimeNow();
              if(this.selfExclusionTime == 1000000) {
                if(this.selfExclusionTimer) {
                  this.selfExclusionTimer.unsubscribe();
                }
                this.selfExclusionText = "You have opted for self exclusion permanently";
              } else {
                let compareDate = new Date();
                compareDate.setMinutes(compareDate.getMinutes() + this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
                if(this.selfExclusionTimer) {
                  this.selfExclusionTimer.unsubscribe();
                }
                this.selfExclusionTimer = Observable.interval(1000).subscribe((val) => {   
                  this.startTimer(compareDate);
                });
              }
            }
          },
          { text: 'Cancel',
            handler: () => {
              if(this.brmService.currentUser.UserGamingLimits) {
                if(this.brmService.currentUser.UserGamingLimits.SelfExclusion) {
                  this.selfExclusionTime = this.brmService.currentUser.UserGamingLimits.SelfExclusionDurationMinutes;
                } else {
                  this.selfExclusionTime = null;
                }
              } else {
                this.selfExclusionTime = null;
              }
            }
          }
        ]
      });
      alert.present();
    }
  }

  isCoolOffPeriodCompleted(): boolean {
    // let user modify after 48 hours - 48H = 172,800 seconds
    if (this.showLimits.enabled7D) {
      if ((CSCUtil.casinocoinToUnixTimestamp(this.showLimits.timestamp7D) + (172800 * 1000)) < Date.now()) {
        this.logger.debug("48 hours completed for 7d - letting user to increase limits as well");
        return true;        
      }
    }
    if (this.showLimits.enabled30D) {
      if ((CSCUtil.casinocoinToUnixTimestamp(this.showLimits.timestamp30D) + (172800 * 1000)) < Date.now()) {
        this.logger.debug("48 hours completed for 30d - letting user to increase limits as well");
        return true;        
      }
    }
    return false;
  }

  validateLimitsAndRevertToPossible(): boolean {
    let changeMade = false;
    if (this.isCoolOffPeriodCompleted()) {
      this.oldLimits.timestamp24H = 0;
      this.oldLimits.timestamp7D = 0;
      this.oldLimits.timestamp30D = 0;
    }
    if (new Big(this.oldLimits.timestamp24H).gt(0) || new Big(this.oldLimits.timestamp7D).gt(0) || new Big(this.oldLimits.timestamp30D).gt(0)) {
      // can not increase limits if already there
      if (new Big(this.oldLimits.timestamp24H).gt(0) && 
          this.showLimits.value24H !== this.oldLimits.value24H && 
          new Big(this.showLimits.value24H).gt(new Big(this.oldLimits.value24H))) {
        this.logger.debug("24h decreased from " + this.showLimits.value24H + " to " + this.oldLimits.value24H);
        this.showLimits.value24H = this.oldLimits.value24H; 
        changeMade = true;
      }
      if (new Big(this.oldLimits.timestamp7D).gt(0) && 
          this.showLimits.value7D !== this.oldLimits.value7D && 
          new Big(this.showLimits.value7D).gt(new Big(this.oldLimits.value7D))) {
        this.logger.debug("7d decreased from " + this.showLimits.value7D + " to " + this.oldLimits.value7D);
        this.showLimits.value7D = this.oldLimits.value7D;
        changeMade = true;
      }
      if (new Big(this.oldLimits.timestamp30D).gt(0) && 
          this.showLimits.value30D !== this.oldLimits.value30D && 
          new Big(this.showLimits.value30D).gt(new Big(this.oldLimits.value30D))) {
        this.logger.debug("30d decreased from " + this.showLimits.value30D + " to " + this.oldLimits.value30D);
        this.showLimits.value30D = this.oldLimits.value30D;
        changeMade = true;
      }

      // 
      if (this.showLimits.enabled30D) {
        if (this.showLimits.enabled7D && (new Big(this.showLimits.value7D).gt(new Big(this.showLimits.value30D)))) {
          this.logger.debug("7d changed from " + this.showLimits.value7D + " to " + this.showLimits.value30D);
          this.showLimits.value7D = this.showLimits.value30D;
          changeMade = true;
        }
        if (this.showLimits.enabled24H && (new Big(this.showLimits.value24H).gt(new Big(this.showLimits.value30D)))) {
          this.logger.debug("24h changed from " + this.showLimits.value24H + " to " + this.showLimits.value30D);
          this.showLimits.value24H = this.showLimits.value30D;
          changeMade = true;
        }
      }
      if (this.showLimits.enabled7D) {
        if (this.showLimits.enabled24H && (new Big(this.showLimits.value24H).gt(new Big(this.showLimits.value7D)))) {
          this.logger.debug("24h changed from " + this.showLimits.value24H + " to " + this.showLimits.value7D);
          this.showLimits.value24H = this.showLimits.value7D;
          changeMade = true;
        }
      }
    } else {
      // check increasing limits
      if (this.showLimits.enabled24H && this.showLimits.enabled7D) {
        // 7D >= 24H
        if (new Big(this.showLimits.value7D).lt(new Big(this.showLimits.value24H))) {
          this.logger.debug("7d - 24h " + this.showLimits.value7D + " to " + this.showLimits.value24H);
          this.showLimits.value7D = this.showLimits.value24H;
          changeMade = true;
        }
      }
      if (this.showLimits.enabled30D && (this.showLimits.enabled24H || this.showLimits.enabled7D)) {
        // 30D >= 24H or 30D >= 7D
        if (this.showLimits.enabled7D && (new Big(this.showLimits.value30D).lt(new Big(this.showLimits.value7D)))) {
          this.logger.debug("30d - 7d " + this.showLimits.value30D + " to " + this.showLimits.value7D);
          this.showLimits.value30D = this.showLimits.value7D;
          changeMade = true;
        } else if (this.showLimits.enabled24H && (new Big(this.showLimits.value30D).lt(new Big(this.showLimits.value24H)))) {
          this.logger.debug("30d - 24h " + this.showLimits.value30D + " to " + this.showLimits.value24H);
          this.showLimits.value30D = this.showLimits.value24H;
          changeMade = true;
        }
      }
    }

    // save new limits
    this.saveLimits();

    if (changeMade) {
      this.logger.debug("Old Limits Values: " + JSON.stringify(this.oldLimits));
      this.logger.debug("New Limits Value: " + JSON.stringify(this.showLimits));
      return false;
    } else {
      return true;
    }
  }

  // showLimitsAlert() {
  //   let alert = this.alertCtrl.create({
  //     subTitle: "Not possible to set Limits as per your input. Reverting to possible Limit changes.",
  //     enableBackdropDismiss: false,
  //     buttons: ['Ok']
  //   });
  //   alert.present();
  // }

  cancelUpdates() {
    this.showLimits = {...this.oldLimits};
  }

  saveUpdates() {
    if(this.validateLimitsAndRevertToPossible()) {
      this.disableSave = true;
      if(this.currentLimitsType == 'multi') {
        this.selectedOperators.forEach(operator => {
          let newLimits: GamingLimit = {
            Value24H: this.showLimits.value24H,
            Enabled24H: this.showLimits.enabled24H,
            TimestampSet24H: this.showLimits.timestamp24H,
            Value7D: this.showLimits.value7D,
            Enabled7D: this.showLimits.enabled7D,
            TimestampSet7D: this.showLimits.timestamp7D,
            Value30D: this.showLimits.value30D,
            Enabled30D: this.showLimits.enabled30D,
            TimestampSet30D: this.showLimits.timestamp30D,
            OperatorID: operator.OperatorID,
            OperatorName: operator.DisplayName
          }
          this.currentUser.UserGamingLimits.OperatorLimits.push(newLimits);        
        });
      } else if (this.currentLimitsType == 'all') {
        if (this.currentUser.UserGamingLimits.OperatorLimits.length > 0) {
          this.operators.forEach(operator => {
            let gamblingLimitsindex = this.currentUser.UserGamingLimits.OperatorLimits.findIndex(x => x.OperatorID === operator.OperatorID);
            if (gamblingLimitsindex >= 0) {
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value24H = this.showLimits.value24H;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled24H = this.showLimits.enabled24H;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet24H = this.showLimits.timestamp24H;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value7D = this.showLimits.value7D;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled7D = this.showLimits.enabled7D;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet7D = this.showLimits.timestamp7D;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Value30D = this.showLimits.value30D;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].Enabled30D = this.showLimits.enabled30D;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].TimestampSet30D = this.showLimits.timestamp30D;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].OperatorID = operator.OperatorID;
              this.currentUser.UserGamingLimits.OperatorLimits[gamblingLimitsindex].OperatorName = operator.DisplayName;
            } else {
              this.addNewOperatorLimit();
            }
          });
        } else {
          this.addNewOperatorLimit();
        }
      }
      this.oldLimits = {...this.showLimits};
      this.showLimitExpireStatus();
      this.brmService.saveUser(this.currentUser).subscribe( result => {
        this.saveMessage = "Successfully Saved.";
        setTimeout(() => {
          this.saveMessage = "";
        }, 2000);
        this.logger.debug("### User Updated: " + JSON.stringify(result));
      }, error => {
        this.nav.setRoot(LoginPage);
        this.brmService.logOut().present();
      });
    } else {
      let alert = this.alertCtrl.create({
        subTitle: "Not possible to set Limits as per your input. Reverting to possible Limit changes.",
        enableBackdropDismiss: false,
        buttons: ['Ok']
      });
      alert.present();
    }
  }

  addNewOperatorLimit() {    
    this.operators.forEach(operator => {
      let newLimits: GamingLimit = {
        Value24H: this.showLimits.value24H,
        Enabled24H: this.showLimits.enabled24H,
        TimestampSet24H: this.showLimits.timestamp24H,
        Value7D: this.showLimits.value7D,
        Enabled7D: this.showLimits.enabled7D,
        TimestampSet7D: this.showLimits.timestamp7D,
        Value30D: this.showLimits.value30D,
        Enabled30D: this.showLimits.enabled30D,
        TimestampSet30D: this.showLimits.timestamp30D,
        OperatorID: operator.OperatorID,
        OperatorName: operator.DisplayName
      }
      this.currentUser.UserGamingLimits.OperatorLimits.push(newLimits);        
    });
  }

  /*
  onSelfExclusionPermanentChange(){
    if(this.currentUser.UserGamingLimits.SelfExclusionPermanent){
      let alert = this.alertCtrl.create({
        title: 'Are you sure?',
        message: "You are opting for self exclusion Permanently.",
        buttons: [{
          text: 'Disagree',
          role: 'cancel',
          handler: () => {
            this.currentUser.UserGamingLimits.SelfExclusionPermanent = false;
          }
        },
        {
          text: 'Agree',
          handler: () => {
            this.currentUser.UserGamingLimits.SelfExclusion = false;
            if(this.selfExclusionTemporaryDisabled) {
              this.selfExclusionTimer.unsubscribe();
            }
            this.selfExclusionTemporaryDisabled = true;
            this.selfExclusionText = "You have opted for self exclusion Permanent.";
            // this.saveUser();
          }
        }]
      });
      alert.present();
    }
  }

  onSelfExclusionTemporaryChange() {
    if(this.currentUser.UserGamingLimits.SelfExclusion){
      this.currentUser.UserGamingLimits.SelfExclusionPermanent = false;
      this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 1440;
      this.selfExcludedTime = 1;
      this.currentUser.UserGamingLimits.SelfExclusionTimestamp = CSCUtil.casinocoinTimeNow();
      this.selfExclusionDurationChange();
    }
  }

  selfExclusionTemporaryClick() {
    if(this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck) {
      if(this.currentUser.UserGamingLimits.SelfExclusionPermanentCheck) {
        this.currentUser.UserGamingLimits.SelfExclusionPermanentCheck = false;
      }
      let checked = false;
      let alert = this.alertCtrl.create({
        title: 'IMPORTANT!',
        message: "Are your certain to self exclude temporarily (can't be reversed before set time).",
        inputs: [{
          type: 'checkbox',
          label:'I have read and understood.',
          handler: (data) =>  { 
            if(data.checked) {
              checked = true;
            } else {
              checked = false;
            }
          }
        }],
        buttons: [{
          text: 'Disagree',
          role: 'cancel',
          handler: () => {
            this.logger.debug('### Responsible Gaming Self Exclusion Temporary - Disagree clicked');
            this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = false;
          }
        },
        {
          text: 'Agree',
          handler: data => {
            if(checked){
              this.logger.debug('### Responsible Gaming Self Exclusion Temporary - Agree clicked');
            } else {
              this.logger.debug('### Responsible Gaming Self Exclusion Temporary - Agree clicked without checkbox Checked');
              this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = false;
            }
          }
        }]
      });
      alert.present();
    }
  }

  selfExclusionPermanentClick() {
    if(this.currentUser.UserGamingLimits.SelfExclusionPermanentCheck) {
      let checked = false;
      let alert = this.alertCtrl.create({
        title: 'IMPORTANT!',
        message: "Are you CERTAIN you wish to permanently exclude yourself. This is an irreversible process.",
        inputs: [{
          type: 'checkbox',
          label:'I have read and understood.',
          handler: (data) =>  { 
            if(data.checked) {
              checked = true;
            } else {
              checked = false;
            }
          }
        }],
        buttons: [{
          text: 'Disagree',
          role: 'cancel',
          handler: () => {
            this.logger.debug('### Responsible Gaming Self Exclusion Permanent  - Disagree clicked');
            this.currentUser.UserGamingLimits.SelfExclusionPermanentCheck = false;
          }
        },
        {
          text: 'Agree',
          handler: data => {
            if(checked){
              this.logger.debug('### Responsible Gaming Self Exclusion Permanent  - Agree clicked');
              if(this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck){
                this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = false;
              }
            } else {
              this.logger.debug('### Responsible Gaming Self Exclusion Permanent  - Agree clicked without checkbox Checked');
              this.currentUser.UserGamingLimits.SelfExclusionPermanentCheck = false;
            }
          }
        }]
      });
      alert.present();
    } else {
      if(this.currentUser.UserGamingLimits.SelfExclusion){
        this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = true;
        this.changeSelfExclusionDurationRange(this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
      }
    }
  }

  selfExclusionDurationChange() {
    switch (this.selfExcludedTime) {
      case 1:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 1440;
        this.message = "1 day.";
        break;
      case 2:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 2880;
        this.message = "2 days.";
        break;
      case 3:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 4320;
        this.message = "3 days.";
        break;
      case 4:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 10080;
        this.message = "7 days.";
        break;
      case 5:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 43200;
        this.message = "1 month.";
        break;
      case 6:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 129600;
        this.message = "3 months.";
        break;
      case 7:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 259200;
        this.message = "6 months.";
        break;
      case 8:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 525600;
        this.message = "1 year.";
        break;
      case 9:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 1576800;
        this.message = "3 years.";
        break;
      case 10:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 2628000;
        this.message = "5 years.";
        break;      
      default:
        this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes = 1440;
        this.message = "1 day.";
        break;
    }
  }

  selfExclusionDurationClick() {
    if(this.selfExclusionTemporaryDisabled) {
      if(this.selfExclusionMinutesTemp > this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes) {
        this.changeSelfExclusionDurationRange(this.selfExclusionMinutesTemp);
        let alert = this.alertCtrl.create({
          title: "Not possible to reduce Timer.",
          buttons: ['Ok']
        });
        alert.present();
        return;
      }
    }
    if(!this.message) 
      this.message = "1 day.";
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      message: "You are opting for self exclusion for " + this.message,
      buttons: [{
        text: 'Disagree',
        role: 'cancel',
        handler: () => {
          if(this.selfExclusionTemporaryDisabled) {
            this.changeSelfExclusionDurationRange(this.selfExclusionMinutesTemp);
          } else {
            this.currentUser.UserGamingLimits.SelfExclusion = false;
          }
        }
      },
      {
        text: 'Agree',
        handler: () => {
          this.currentUser.UserGamingLimits.SelfExclusionTimestamp = CSCUtil.casinocoinTimeNow();
          this.selfExclusionMinutesTemp = this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes;
          let timestamp = CSCUtil.casinocoinTimeToISO8601(this.currentUser.UserGamingLimits.SelfExclusionTimestamp);
          let compareDate = new Date(timestamp);
          compareDate.setMinutes(compareDate.getMinutes() + this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
          if(this.selfExclusionTemporaryDisabled){
            this.selfExclusionTimer.unsubscribe();
          }
          this.selfExclusionTemporaryDisabled = true;
          this.selfExclusionTimer = Observable.interval(1000).subscribe((val) => {   
            this.startTimer(compareDate);
          });
          // this.saveUser();
        }
      }]
    });
    alert.present();
  }  

  changeSelfExclusionDurationRange(range) {
    switch (range) {
      case 1440:
        this.selfExcludedTime = 1;
        break;
      case 2880:
        this.selfExcludedTime = 2;
        break;
      case 4320:
        this.selfExcludedTime = 3;
        break;
      case 10080:
        this.selfExcludedTime = 4;
        break;
      case 43200:
        this.selfExcludedTime = 5;
        break;
      case 129600:
        this.selfExcludedTime = 6;
        break;
      case 259200:
        this.selfExcludedTime = 7;
        break;
      case 525600:
        this.selfExcludedTime = 8;
        break;
      case 1576800:
        this.selfExcludedTime = 9;
        break;
      case 2628000:
        this.selfExcludedTime = 10;
        break;      
      default:
        this.selfExcludedTime = 1;
        break;
    }
  }
  */
}
