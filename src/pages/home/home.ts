import { Component, ViewChild } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, Tabs, MenuController, IonicApp ,App, ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { LogService } from '../../providers/log.service';
import { OperatorsPage } from '../operators/operators';
import { SendPage } from '../send/send';
import { ReceivePage } from '../receive/receive';
import { TransactionsPage } from '../transactions/transactions';
import { ContactsPage } from '../contacts/contacts';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { AppConstants } from '../../domain/app-constants';
import { LocalStorageService } from 'ngx-store';
import { BRMService } from '../../providers/brm.service';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild('brmTabs') tabRef: Tabs;

  tabOperators = OperatorsPage;
  tabSend = SendPage;
  tabReceive = ReceivePage;
  tabContacts= ContactsPage;
  tabTransactions = TransactionsPage;
  selectedTab: number= 0;

  constructor(public platform: Platform,
              public navCtrl: NavController, 
              public navParams: NavParams,
              private statusBar: StatusBar,
              private logger: LogService, 
              public casinocoinService: CasinocoinService,
              public menuCtrl:MenuController,
              public ionicApp:IonicApp,
              public app:App,
              public toastCtrl:ToastController,
              public localStorageService: LocalStorageService,
              public brmService: BRMService) {
    // set status bar to white
    // this.statusBar.backgroundColorByHexString('#ffffff');
    this.selectedTab = 0;
    this.statusBar.styleLightContent();
    var lastTimeBackPress=0;
    var timePeriodToExit = 2000;

    //register to the back button
    platform.registerBackButtonAction(() => {
      if (this.menuCtrl.isOpen()) {
        this.menuCtrl.toggle();
        return false;
      } else if (this.ionicApp._modalPortal.getActive() || this.ionicApp._overlayPortal.getActive() || this.ionicApp._loadingPortal.getActive()) {
        let activePortal = this.ionicApp._modalPortal.getActive() || this.ionicApp._overlayPortal.getActive() || this.ionicApp._loadingPortal.getActive();
        activePortal.dismiss();
        return false;
      } else {
        let nav = this.app.getActiveNav();
        if (nav.canGoBack()) { //Can we go back?
          nav.pop();
        } else if (this.navCtrl.length() > 1) {
          this.navCtrl.pop();
          return false;
        } else {
          let tabPrv = this.tabRef.previousTab(true);
          if (tabPrv) this.tabRef.select(tabPrv.index);
          if (tabPrv == null) {
            if ((new Date().getTime() - lastTimeBackPress) < timePeriodToExit) {
              this.platform.exitApp(); //Exit from app
            } else {
              const toast = this.toastCtrl.create({
                message: 'Press again to exit',
                duration: 3000,
                position: 'bottom'
              });              
              toast.onDidDismiss(() => {
                console.log('Dismissed toast');
              });              
              toast.present();
              lastTimeBackPress = new Date().getTime();
            }
          }
          return false;
        }
      }
    });
  }

  ionViewDidLoad() {
    this.logger.debug('### ionViewDidLoad HomePage');
  }

  ionViewWillEnter() {
    if(this.localStorageService.get(AppConstants.BRM_MODE) === "advance") {
      this.brmService.brmDefaultMode = false;
    } else {
      this.brmService.brmDefaultMode = true;
    }
    if(this.navParams.data.id){
      this.selectedTab = this.navParams.data.id;
    } else {
      this.selectedTab = 0;
    }
    // this.selectedTab = this.navParams.data.id ? this.navParams.data.id : 0;
    // this.navCtrl.parent.select(this.selectedTab);
    this.tabRef.select(this.selectedTab); 
  }

}
