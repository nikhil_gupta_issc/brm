import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController, ToastController } from 'ionic-angular';
import { CSCUtil } from '../../domain/csc-util';
import { BRMService } from '../../providers/brm.service';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { LogService } from '../../providers/log.service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';

@IonicPage()
@Component({
  selector: 'page-receive',
  templateUrl: 'receive.html',
})
export class ReceivePage {

  connectedText: string = "Disconnected";
  connectedColor: string = "disconnected";

  accountID: string;
  cscReceiveURI: string = "";
  sendAmount:string;
  destinationTag: number;
  label: string;
  canEmail: boolean = false;

  constructor( public navCtrl: NavController, 
               public navParams: NavParams,
               public platform: Platform,
               private logger: LogService,
               private casinocoinService: CasinocoinService,
               public brmService: BRMService,
               private socialSharing: SocialSharing,
               public actionSheetCtrl: ActionSheetController,
               public clipboard: Clipboard,
               public toastCtrl: ToastController) {
    // subscribe to CasinoCoin connections
    this.casinocoinService.casinocoinConnectedSubject.subscribe( connected => {
      if(connected){
        // set connected header
        this.connectedText = "Connected";
        this.connectedColor = "connected";
      } else {
        // set connected header
        this.connectedText = "Disconnected";
        this.connectedColor = "disconnected";
      }
    });
  }

  ionViewDidLoad() {
    this.logger.debug('### Receive CSC ###');
    // get default account id
    this.accountID = this.brmService.currentUser.AccountID;
    this.cscReceiveURI = CSCUtil.generateCSCQRCodeURI({ address: this.accountID });
  }

  updateQRCode(){
    let uriObject = { address: this.accountID };
    if(this.sendAmount && this.sendAmount.length > 0){
      uriObject['amount'] = this.sendAmount;
    }
    if(this.destinationTag && (this.destinationTag > 0 && this.destinationTag < 2147483647)){
      uriObject['destinationTag'] = this.destinationTag;
    }
    if(this.label && this.label.length > 0){
      uriObject['label'] = this.label;
    }
    this.cscReceiveURI = CSCUtil.generateCSCQRCodeURI(uriObject);
  }

  disableReset(): boolean {
    if (this.label || this.destinationTag || this.sendAmount) {
      return false;
    } else {
      return true;
    }
  }

  resetQRCode(){
    this.sendAmount = "";
    this.destinationTag = null;
    this.label = "";
    this.updateQRCode();
  }

  shareReceiveAddress(){
     this.logger.debug("### Share: " + this.accountID);
     this.socialSharing.share("CasinoCoin BRM Account: " + this.accountID, "CasinoCoin BRM Account");
  }

  showActionSheet() {
    this.logger.debug('### Receive - Press QR code showing Action Sheet ###');
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Copy Account Address',
          handler: () => {
            this.copyAccountID();
          }
        },
        { text: 'Cancel' }
      ]
    }); 
    actionSheet.present();
    actionSheet.onDidDismiss(() => {});
  }

  copyAccountID() {    
    this.clipboard.copy(this.accountID);
    let toast = this.toastCtrl.create({
      message: 'Copied!',
      duration: 3000,
      position: 'top'
    });         
    toast.present();
  }
}
