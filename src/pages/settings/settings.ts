import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Nav } from 'ionic-angular';
import { KycPage } from '../kyc/kyc';
import { LinkAccountPage } from '../link-account/link-account';
import { TranslateService } from '@ngx-translate/core';
import { BRMService } from '../../providers/brm.service';
import { User } from '../../domain/brm-api';
import { LocalStorageService } from 'ngx-store';
import { AppConstants } from '../../domain/app-constants';
import { LokiAccount } from '../../domain/lokijs';
import { PincodeController } from  'ionic2-pincode-input';
import { LogService } from '../../providers/log.service';
import { AlertController } from 'ionic-angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { MarketService } from '../../providers/market.service';
import { LoginPage } from '../login/login';
import { ChangePinPage } from '../change-pin/change-pin';
import { RecoveryPhrasePage } from '../recovery-phrase/recovery-phrase';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  userProfile: User;
  defaultLanguage: string;
  defaultCurrency: string;  
  defaultAccount: LokiAccount;
  pin: string;
  decryptedPIN = "";
  faioValue: string;
  codeGenerated: boolean = false;
  backupData: any;
  mnemonicShow: boolean = false;
  recoveryMnemonicWords: Array<string>;
  brmMode: string;

  constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private nav: Nav,
        private translateService: TranslateService,
        private brmService: BRMService,
        public localStorageService: LocalStorageService,
        public pincodeCtrl: PincodeController,
        private alertCtrl: AlertController,
        private logger: LogService,
        private faio: FingerprintAIO,
        public platform: Platform,
        public marketService: MarketService
      ) {
        this.userProfile = this.localStorageService.get(AppConstants.KEY_BRM_USER);
        this.defaultLanguage = this.userProfile.Language;
        this.defaultCurrency = this.userProfile.Currency;
        this.faioValue = this.localStorageService.get(AppConstants.TOUCH_ID_ENABLE);
        this.brmMode = this.localStorageService.get(AppConstants.BRM_MODE);
        if(!this.brmMode) {
          this.brmMode = "default";
          this.localStorageService.set(AppConstants.BRM_MODE, this.brmMode);
        }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  gotoKyc(){
    this.navCtrl.push(KycPage);
  }

  languageChanged(value) {
    this.userProfile.Language = value;
    this.brmService.saveUser(this.userProfile).subscribe( result => {
      this.translateService.use(value);
    }, error => {
      this.nav.setRoot(LoginPage);
      this.brmService.logOut().present();
    });
  }

  currencyChanged(value) {
    this.userProfile.Currency = value;
    this.brmService.saveUser(this.userProfile).subscribe( result => {
      // this.translateService.use(value);
      this.marketService.changeCurrency(value);
    }, error => {
      this.nav.setRoot(LoginPage);
      this.brmService.logOut().present();
    });
  }

  modeChange() {
    this.localStorageService.set(AppConstants.BRM_MODE, this.brmMode);
  }

  touchId() {
    if(this.faioValue === "true"){
      this.faio.isAvailable().then(result =>{
        if(result == "finger"){
          this.logger.debug("### Fingerprint Enabled " + JSON.stringify(result));
          this.localStorageService.set(AppConstants.TOUCH_ID_ENABLE, true); 
        } else {
          this.logger.debug("### Fingerprint Not Compatible " + JSON.stringify(result));
          let alert = this.alertCtrl.create({
            title: 'Touch ID',
            subTitle: 'Your Phone doesnt have Fingerprint feature.',
            buttons: ['Dismiss']
          });
          alert.onDidDismiss(() => { });
          alert.present();
          this.faioValue = "false";
        }
      }).catch(err => {
        this.logger.debug("### Fingerprint Not Compatible Error: " + JSON.stringify(err));
        let alert = this.alertCtrl.create({
          title: 'Touch ID',
          subTitle: 'Fingerprint is not Set in your phone.',
          buttons: ['Dismiss']
        });
        alert.present();
        this.faioValue = "false";
      });
    } else {
      this.localStorageService.set(AppConstants.TOUCH_ID_ENABLE, false); 
    }
  }

  linkAccount() {
    this.navCtrl.push(LinkAccountPage);
  }

  gotoRecoveryPhrase() {
    this.navCtrl.push(RecoveryPhrasePage);
  }

  changePin() {
    this.navCtrl.push(ChangePinPage);
    // let pinCode =  this.pincodeCtrl.create({
    //   title:'Current BRM PIN Code',
    //   hideForgotPassword: true
    // });
    // pinCode.present();
    // pinCode.onDidDismiss( (code,status) => {
    //   if(status === 'done'){
    //     this.logger.debug("### PIN: " + JSON.stringify(code));            
    //     let userAccounts = this.walletService.getAllAccounts();
    //     this.logger.debug("### OPERATORS Accounts: " + JSON.stringify(userAccounts));
    //     if(userAccounts.length > 0){
    //       this.defaultAccount = userAccounts[0];
    //     }
    //     try {
    //       let cscCrypto = new CSCCrypto(this.defaultAccount.accountID, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
    //       let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
    //       this.decryptedPIN = cscCrypto.decrypt(storedPIN);
    //     } catch (e) {
    //         this.logger.debug("### Decrypting PIN Went wrong ###");
    //         this.decryptedPIN = code;
    //     }
    //     if(code === this.decryptedPIN){
    //       this.setWalletPIN();
    //     } else {
    //       let alert = this.alertCtrl.create({
    //         title: 'Invalid PIN',
    //         subTitle: 'You entered an invalid PIN. Please retry!',
    //         buttons: ['Dismiss']
    //       });
    //       alert.onDidDismiss(() => {
    //         this.changePin();
    //       });
    //       alert.present();
    //     }
    //   } else if (status === 'forgot'){
    //     this.logger.debug("### Forgot Password");
    //   }
    // });   
  }

  // setWalletPIN(){
  //   let pinCode =  this.pincodeCtrl.create({
  //     title:'New BRM PIN Code',
  //     hideForgotPassword: true
  //   });
  //   pinCode.present();
  //   pinCode.onDidDismiss( (code,status) => {
  //     if(status === 'done'){
  //       this.pin = code;
  //       this.logger.debug("### PIN: " + JSON.stringify(this.pin));
  //       this.verifyWalletPIN();
  //     } else if (status === 'forgot'){
  //       this.logger.debug("### Forgot Password");
  //     }
  //   });
  // }

  // verifyWalletPIN(){
  //   let pinCode =  this.pincodeCtrl.create({
  //     title:'Verify PIN Code',
  //     hideForgotPassword: true
  //   });
  //   pinCode.present();
  //   pinCode.onDidDismiss( (code,status) => {
  //     if(status === 'done'){
  //       this.logger.debug("### Verify PIN: " + JSON.stringify(code));
  //       if(this.pin === code){
  //         let loader = this.loadingCtrl.create({spinner: 'crescent', content: 'Processing ...',duration: 60000});
  //         loader.present().then(() => {
  //           let cscCrypto = new CSCCrypto(this.defaultAccount.accountID, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
  //           let encryptedPIN = cscCrypto.encrypt(code);
  //           this.localStorageService.set(AppConstants.KEY_BRM_PIN, encryptedPIN);
  //           this.walletService.walletPIN = code;
  //           // this.recoveryMnemonicWords = CSCCrypto.getRandomMnemonic();
  //           // update password with existing Mnemonic words
  //           this.recoveryMnemonicWords = JSON.parse(cscCrypto.decrypt(this.localStorageService.get(AppConstants.KEY_WALLET_MNEMONIC_WORDS)));
  //           let mnemonicHash = new CSCCrypto(this.recoveryMnemonicWords, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress).encrypt(code);
  //           this.walletService.changePin(this.decryptedPIN, code, mnemonicHash);
  //           let alert = this.alertCtrl.create({
  //             title: 'Success',
  //             subTitle: 'Your PIN has been changed Successfully.',
  //             buttons: ['Ok']
  //           });
  //           // alert.onDidDismiss(() => {
  //           //   this.mnemonicShow = true;
  //           //   // this.screenShot();
  //           // });
  //           loader.dismiss();
  //           alert.present();          
  //         });
  //       } else {
  //         let alert = this.alertCtrl.create({
  //           title: 'PIN not Match',
  //           subTitle: 'Your PIN do not match. Please retry!',
  //           buttons: ['Dismiss']
  //         });
  //         alert.onDidDismiss(() => {
  //           this.setWalletPIN();
  //         });
  //         alert.present();
  //       }
  //       this.logger.debug("### Stored PIN: " + code );
  //     }
  //   });
  // }

  // recoveryButton() {
  //   this.mnemonicShow = false;
  // }

  // backupBRM() {
  //   this.logger.debug("### Backup BRM ");    
  //   let accountID = this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID);

  //   let pinCode =  this.pincodeCtrl.create({
  //     title:'BRM PIN Code',
  //     hideForgotPassword: true
  //   });
  //   pinCode.present();
    
  //   pinCode.onDidDismiss( (code,status) => {
  //     if(status === 'done'){
  //       this.logger.debug("### PIN: " + JSON.stringify(code));          
  //       let cscCrypto = new CSCCrypto(accountID, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
  //       let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
  //       this.decryptedPIN = cscCrypto.decrypt(storedPIN);
  //       if(code === this.decryptedPIN){
  //         cscCrypto = new CSCCrypto(this.decryptedPIN, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
  //         let key = this.walletService.getKey(accountID);
  //         let encryptedSecret = key.encrypted ? key.secret : cscCrypto.encrypt(key.secret);
  //         this.generateBackupCode(accountID, encryptedSecret);
  //       } else {
  //         let alert = this.alertCtrl.create({
  //           title: 'Invalid PIN',
  //           subTitle: 'You entered an invalid PIN. Please retry!',
  //           buttons: ['Dismiss']
  //         });
  //         alert.present();
  //       }
  //     }
  //   });   
  // }

  // generateBackupCode(accountID: string, encryptedSecret: string) {    
  //   this.backupData = JSON.stringify({
  //     "accountID": accountID,
  //     "accountData": encryptedSecret,
  //     "brmPinData" : this.localStorageService.get(AppConstants.KEY_BRM_PIN),
  //     "walletUUID" : this.localStorageService.get(AppConstants.KEY_CURRENT_WALLET),
  //     "recoveryHash" : this.walletService.getDBMetadata().mnemonicRecovery,
  //     "touchId" : this.localStorageService.get(AppConstants.TOUCH_ID_ENABLE)
  //   });
  //   this.codeGenerated = true;
  // }

  // copyBackupCode(){
  //   console.log("Copy Backup Code "+this.backupData);
  //   this.clipboard.copy(this.backupData);
  //   let toast = this.toastCtrl.create({
  //     message: 'Copied!',
  //     duration: 3000,
  //     position: 'top'
  //   });         
  //   toast.present();
  // }

  // copyCancel(){
  //   this.codeGenerated = false;
  // }
}
