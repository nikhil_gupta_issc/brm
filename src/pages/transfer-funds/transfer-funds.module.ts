import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransferFundsPage } from './transfer-funds';

@NgModule({
  declarations: [],
  imports: [
    IonicPageModule.forChild(TransferFundsPage),
  ],
})
export class TransferFundsPageModule {}
