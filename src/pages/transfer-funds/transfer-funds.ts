import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { BRMService } from '../../providers/brm.service';
import { WalletService } from '../../providers/wallet.service';
import { CSCUtil } from '../../domain/csc-util';
import Big from 'big.js';
import { CSCAmountPipe } from '../../app/app-pipes.module';

@IonicPage()
@Component({
  selector: 'page-transfer-funds',
  templateUrl: 'transfer-funds.html',
})
export class TransferFundsPage {

  userOperatorAccountID: string;
  defaultAccountID: string;
  displayName: string;
  transferType:string = "DEPOSIT";
  amount:number;
  accountBalance: string = "0.00";
  fees: string = "0.00";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private logger: LogService,
              private brmService: BRMService,
              private walletService: WalletService,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private cscAmountPipe: CSCAmountPipe) {

    // get the operator to show
    this.userOperatorAccountID = this.navParams.get("userOperatorAccountID");
    this.defaultAccountID = this.navParams.get("defaultAccountID");
    this.displayName = this.navParams.get("displayName");
    this.logger.debug("### User AccountID for Operator: " + this.userOperatorAccountID + " Default AccountID: " + this.defaultAccountID + " Display Name " + this.displayName);
    let balance = new Big(this.walletService.getAccountBalance(this.defaultAccountID));
    this.accountBalance = CSCUtil.dropsToCsc(balance.minus(this.brmService.accountReserveDrops).toString());
    this.fees = CSCUtil.dropsToCsc(new Big(this.brmService.feesDrops));
  }

  ionViewDidLoad() {
    this.logger.debug("### ionViewDidLoad TransferFundsPage");
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onTransferTypeChange(selectedValue: any) {
    this.logger.debug("Transfer Type: " + selectedValue);
    if(this.transferType == "DEPOSIT"){
      let balance = new Big(this.walletService.getAccountBalance(this.defaultAccountID));
      this.accountBalance = CSCUtil.dropsToCsc(balance.minus(this.brmService.accountReserveDrops).toString());
    } else {
      let balance = new Big(this.walletService.getAccountBalance(this.userOperatorAccountID));
      this.accountBalance = CSCUtil.dropsToCsc(balance.minus(this.brmService.accountReserveDrops).toString());
    }
  }

  onAmountChange(inputValue: any) {
    this.logger.debug("Amount: " + inputValue);
  }

  transferFunds(){
    this.logger.debug("### Tansfer: " + this.amount + " as: "+ this.transferType);
    if(new Big(CSCUtil.cscToDrops(this.amount.toString())).lte(new Big(CSCUtil.cscToDrops(this.accountBalance)).minus(this.brmService.feesDrops))) {
      let loader = this.loadingCtrl.create({spinner: 'crescent', content: 'Transferring Coins ...',duration: 5000});
      loader.present().then( () => {
        let amountToSend;
        // to remove 999,999.99998001 csc submitted error, if user enter more than 8 decimals with zeros
        if (this.amount.toString().split('.')[1]) {
          amountToSend = this.cscAmountPipe.transform(CSCUtil.cscToDrops(new Big(this.amount)), false, true);
        } else {
          amountToSend = this.amount.toString();
        }
        if(this.transferType == "DEPOSIT"){
          // deposit from default to operator
          this.brmService.signAndSubmit(this.defaultAccountID, this.userOperatorAccountID, CSCUtil.cscToDrops(amountToSend)); 
        } else {
          // deposit from operator to default
          this.brmService.signAndSubmit(this.userOperatorAccountID, this.defaultAccountID, CSCUtil.cscToDrops(amountToSend)); 
        }
        loader.dismiss();
        this.dismiss();
      });
    } else {
      let alert = this.alertCtrl.create({
        title: 'Insuficient Balance',
        subTitle: 'You do not have suficient funds to transfer the amount entered!',
        buttons: ['Dismiss']
      });
      alert.onDidDismiss(() => {
        this.amount = new Big(this.accountBalance).minus(CSCUtil.dropsToCsc(this.brmService.feesDrops));
      });
      alert.present();
    }
  }
}
