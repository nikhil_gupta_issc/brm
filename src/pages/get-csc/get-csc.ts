import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MarketService } from '../../providers/market.service';
import { LogService } from '../../providers/log.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-get-csc',
  templateUrl: 'get-csc.html',
})
export class GetCscPage {

    constructor( public navCtrl: NavController, 
               public navParams: NavParams,
               private logger: LogService,
               public marketService: MarketService,
               public iab: InAppBrowser) {
  }

  ionViewDidLoad() {
    this.logger.debug('### ionViewDidLoad GetCscPage');
  }

  visitExchange(link) {
    this.logger.debug('### Exchange GetCscPage visit Exchange: ' + link);
    this.iab.create(link, "_system");
  }

}
