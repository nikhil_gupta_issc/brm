import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetCscPage } from './get-csc';

@NgModule({
  declarations: [ ],
  imports: [
    IonicPageModule.forChild(GetCscPage),
  ],
})
export class GetCscPageModule {}
