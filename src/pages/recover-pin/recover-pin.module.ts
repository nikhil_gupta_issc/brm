import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecoverPinPage } from './recover-pin';

@NgModule({
  declarations: [],
  imports: [
    IonicPageModule.forChild(RecoverPinPage),
  ],
})
export class RecoverPinPageModule {}
