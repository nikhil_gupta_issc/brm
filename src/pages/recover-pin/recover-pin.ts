import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { LocalStorageService } from 'ngx-store';
import { AppConstants } from '../../domain/app-constants';
import { CSCCrypto } from '../../domain/csc-crypto';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-recover-pin',
  templateUrl: 'recover-pin.html',
})
export class RecoverPinPage {
  message: string = "";
  recoveryWords = {
    word1: "",
    word2: "",
    word3: "",
    word4: "",
    word5: "",
    word6: "",
    word7: "",
    word8: "",
    word9: "",
    word10: ""
  }

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public logger: LogService,
              public alertCtrl: AlertController,
              public localStorageService: LocalStorageService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecoverPinPage');
  }

  recoverPin() {
    this.message = "";
    if (this.recoveryWords.word1.length == 0 ||
        this.recoveryWords.word2.length == 0 ||
        this.recoveryWords.word3.length == 0 ||
        this.recoveryWords.word4.length == 0 ||
        this.recoveryWords.word5.length == 0 ||
        this.recoveryWords.word6.length == 0 ||
        this.recoveryWords.word7.length == 0 ||
        this.recoveryWords.word8.length == 0 ||
        this.recoveryWords.word9.length == 0 ||
        this.recoveryWords.word10.length == 0){
            this.message = "Please enter all 10 words!"
    } else {
      this.logger.debug("### Recover with words: " + JSON.stringify(this.recoveryWords));
      let recoveryArray: Array<string> = [];
      recoveryArray.push(this.recoveryWords.word1.toLowerCase(), 
                          this.recoveryWords.word2.toLowerCase(),
                          this.recoveryWords.word3.toLowerCase(),
                          this.recoveryWords.word4.toLowerCase(),
                          this.recoveryWords.word5.toLowerCase(),
                          this.recoveryWords.word6.toLowerCase(),
                          this.recoveryWords.word7.toLowerCase(),
                          this.recoveryWords.word8.toLowerCase(),
                          this.recoveryWords.word9.toLowerCase(),
                          this.recoveryWords.word10.toLowerCase()
                        );
      try{
        // let mnemonicRecovery = this.walletService.getMnemonicRecovery();
        let mnemonicRecovery = this.localStorageService.get(AppConstants.KEY_WALLET_MNEMONIC_RECOVERY);
        this.logger.debug("### Mnemonic Recovery: " + mnemonicRecovery);
        let pin = new CSCCrypto(recoveryArray, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress).decrypt(mnemonicRecovery);
        this.logger.debug("### Mnemonic Password: " + pin);
        let alert = this.alertCtrl.create({
          title: 'Recover PIN',
          subTitle: 'Your BRM PIN is: ' + pin,
          buttons: ['OK']
        });
        alert.onDidDismiss(() => {
          this.cancel();
        });
        alert.present();
      } catch (error) {
        this.logger.error("### Decrypt Error: " + error);
        this.message = "Can not decrypt pin with entered words.";
      }
    }
  }

  cancel() {
    this.navCtrl.setRoot(LoginPage);
  }

}
