import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, ModalController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { AlertController } from 'ionic-angular';
import { PincodeController } from  'ionic2-pincode-input';
import { LogService } from '../../providers/log.service';
import { WalletService } from '../../providers/wallet.service';
import { BRMService } from '../../providers/brm.service';
import { CSCCrypto } from '../../domain/csc-crypto';
import { AppConstants } from '../../domain/app-constants';
import { LocalStorageService } from 'ngx-store';
import { LokiKey } from '../../domain/lokijs';
import { HomePage } from '../home/home';
import { MainPage } from '../main/main';
import { User } from '../../domain/brm-api';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { WebsocketService } from '../../providers/websocket.service';
import { RecoverPinPage } from '../recover-pin/recover-pin';
import { CasinocoinService } from '../../providers/casinocoin.service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild('pinCodeViewChild') pinCodeViewChild;

  keyPair: LokiKey;
  defaultAccount: string;
  walletUUID: string;
  pinCode: any;
  recoveryShow: boolean = false;
  message: string = "";
  enteredPinCode: string = "";
  loginEntry: boolean = false;
  loader: any;
  displayCustomPin: boolean = false;
  loginDisable: boolean = false;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public platform: Platform,
              private statusBar: StatusBar,
              public pincodeCtrl: PincodeController,
              public logger: LogService,
              public localStorageService: LocalStorageService,
              private walletService: WalletService,
              public brmService: BRMService,
              private alertCtrl: AlertController,
              private faio: FingerprintAIO,
              private webSocketService: WebsocketService,
              private loadingCtrl: LoadingController,
              public modalCtrl: ModalController,
              private toastCtrl: ToastController,
              private casinocoinService: CasinocoinService) {
    // set light text on dark status bar
    this.statusBar.styleLightContent();
    this.defaultAccount = this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID);
    this.walletUUID = this.localStorageService.get(AppConstants.KEY_CURRENT_WALLET);
  }

  ionViewDidLoad() {
    this.logger.debug('### Login ionViewDidLoad ###');
    // subscribe to open wallet subject
  }

  // createPINPad(){
  //   // define pinpad
  //   this.pinCode =  this.pincodeCtrl.create({
  //     title:'BRM PIN Code',
  //     cancelButtonText: 'Cancel',
  //     forgotPasswordText: 'Forgot PIN Code'
  //   });
  //   this.pinCode.onDidDismiss( (code,status) => {
  //     this.logger.debug("### Pinpad status: " + status);
  //     if(status === 'done'){
  //       let decryptedPIN = "";
  //       try {
  //         let cscCrypto = new CSCCrypto(this.defaultAccount, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
  //         let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
  //         decryptedPIN = cscCrypto.decrypt(storedPIN);
  //       } catch (e) {
  //           this.logger.debug("### Decrypting PIN Went wrong ###");
  //           decryptedPIN = code;
  //       }
  //       if(code == decryptedPIN){
  //         this.logger.debug("### Correct PIN ###");
  //         this.verifyPinAndLogin(decryptedPIN);
  //       } else {
  //         let alert = this.alertCtrl.create({
  //           title: 'Invalid PIN',
  //           subTitle: 'You entered an invalid PIN. Please retry!',
  //           buttons: ['Dismiss']
  //         });
  //         alert.onDidDismiss(() => {
  //           this.createPINPad();
  //         });
  //         alert.present();
  //       }
  //       // 
  //       // this.pinSubmitted = true;
  //       // this.verifyWalletPIN();
  //     } else if (status === 'forgot'){
  //       this.logger.debug("### Forgot Password");
  //       this.forgotPin();
  //     } else if (status === 'backdrop'){
  //       // dismissed
  //     } else if (status === 'cancel'){
  //       // cancelled
  //     }
  //   });
  //   this.pinCode.present();
  // }

  enterPIN(){
    this.loginDisable = true;
    if (this.platform.is('mobileweb') || this.casinocoinService.networkConnected) {
      if(this.localStorageService.get(AppConstants.TOUCH_ID_ENABLE)) {
        this.faio.show({
          clientId: 'CasinoCoin BRM',
          clientSecret: 'password'
        })
        .then((result: any) => {
          this.loginDisable = false;
          this.loader = this.loadingCtrl.create({spinner: 'crescent',content: 'Validating Fingerprint', duration: 60000});
          this.loader.present().then(() => {
            let decryptedPIN = "";
            try {
              let cscCrypto = new CSCCrypto(this.defaultAccount, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
              let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
              decryptedPIN = cscCrypto.decrypt(storedPIN);
            } catch (e) {
              this.logger.debug("### Decrypting PIN Went wrong ###");
            }
            this.logger.debug("### Fingerprint Login ###");
            this.verifyPinAndLogin(decryptedPIN);
          })
          .catch((error: any) => {
            // create PIN pad
            // this.createPINPad();
            // this.showPinInput();
            this.showCustomPin();
          });
        }, error => {
          this.loginDisable = false;
        });
      } else {
        // create PIN pad
        // this.createPINPad();
        // this.showPinInput();
        this.showCustomPin();
      }
    } else {
      let alert = this.alertCtrl.create({
        title: 'Network Error',
        subTitle: 'Please check your Internet connection.',
        buttons: ['Dismiss']
      });
      alert.present();
      this.loginDisable = false;
    }
  }

  showCustomPin() {
    this.displayCustomPin = true;
    // let modal = this.modalCtrl.create(CustomPinComponent, { pageTitle: "Enter PIN code" });
    // modal.present();
    // modal.onDidDismiss(data => {
    //   if(data) {
    //     this.enteredPinCode = data;
    //     this.validatePincode();
    //   }
    // });
  }

  handlePinInput(pin: string) {
    this.enteredPinCode += pin;
    if (this.enteredPinCode.length === 6) {
      this.validatePincode();
    }
  }

  backspacePin() {
    this.enteredPinCode = this.enteredPinCode.substring(0, this.enteredPinCode.length - 1);
  }

  cancelPin() {
    this.enteredPinCode = "";
    this.displayCustomPin = false;
    this.loginDisable = false;
  }

  verifyPinAndLogin(decryptedPIN) {
    this.loader.setContent("Decrypting Wallet");
    this.walletService.walletPIN = decryptedPIN;
    let userObject: User = this.localStorageService.get(AppConstants.KEY_BRM_USER);
    this.walletService.openWallet(this.walletUUID).subscribe( result => {
      if(result == AppConstants.KEY_LOADED){
        let accountKey: LokiKey = this.walletService.getKey(userObject.AccountID);
        if (accountKey.encrypted) {
          accountKey.encrypted = false;
          accountKey.secret = this.walletService.getDecryptSecret(decryptedPIN, accountKey);
        }
        this.loader.setContent("Authenticating. Please wait...");
        this.brmService.loginUser(accountKey).subscribe(result => {
          this.logger.debug("Logged in: " + result);
          if(result == AppConstants.KEY_FINISHED){
            // encrypt secret key
            this.walletService.encryptSecretKey(decryptedPIN);
            // get the current operators
            this.brmService.updateCurrentOperators();
            this.brmService.updateCurrentUser();
            this.webSocketService.connectToBrmWebsocket();
            this.loader.dismiss();
            // let msg: NotificationType = {severity: SeverityType.info, title:'Wallet Message', body:'Successfully opened the wallet.'};
            // this.notificationService.addMessage(msg);
            let toast = this.toastCtrl.create({
              message: "Successfully opened the wallet.",
              duration: 2000,
              position: 'top'
            });         
            toast.present();
            this.navCtrl.setRoot(HomePage);
          } else if (result == AppConstants.KEY_ERRORED) {
            this.loader.dismiss();
            this.cancelPin();
          }
        });
      }
    });
  }

  // showPinInput() {
  //   this.loginEntry = true;
  //   setTimeout(() => {
  //     this.pinCodeViewChild.setFocus(); 
  //   }, 100);
  // }

  validatePincode() {
    if(this.enteredPinCode.length === 6){
      // this.pinCodeViewChild.setBlur();
      this.loader = this.loadingCtrl.create({spinner: 'crescent', content: 'Validating PIN', duration: 60000});
      this.loader.present().then(() => {
        // setTimeout(() => {
          let decryptedPIN = "";
          try {
            let cscCrypto = new CSCCrypto(this.defaultAccount, this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
            let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
            decryptedPIN = cscCrypto.decrypt(storedPIN);
          } catch (e) {
            this.logger.debug("### Decrypting PIN Went wrong ###");
            decryptedPIN = this.enteredPinCode;
          }
          if(this.enteredPinCode === decryptedPIN) {
            this.logger.debug("### Correct PIN ###");
            this.verifyPinAndLogin(decryptedPIN);
          } else {
            this.loader.dismiss();
            let alert = this.alertCtrl.create({
              title: 'Invalid PIN',
              subTitle: 'You entered an invalid PIN. Please retry!',
              buttons: ['Dismiss']
            });
            alert.onDidDismiss(() => {
              this.enteredPinCode = "";
              this.showCustomPin();
              // setTimeout(() => {
              //   this.pinCodeViewChild.setFocus(); 
              // }, 200);
            });
            alert.present();
          }
        // }, 300);
      });
    }
  }

  forgotPin() {
    this.navCtrl.setRoot(RecoverPinPage);
    // this.recoveryShow = true;
  }

  // cancel(){
  //   this.recoveryWords = {
  //     word1: "",
  //     word2: "",
  //     word3: "",
  //     word4: "",
  //     word5: "",
  //     word6: "",
  //     word7: "",
  //     word8: "",
  //     word9: "",
  //     word10: ""
  //   }
  //   this.recoveryEmail = "";
  //   this.message = "";
  //   this.recoveryShow = false;
  // }

  loginCancel() {    
    this.loginEntry = false; 
  }

  // recoverWallet() {
  //   this.message = "";
  //   if (this.recoveryWords.word1.length == 0 ||
  //       this.recoveryWords.word2.length == 0 ||
  //       this.recoveryWords.word3.length == 0 ||
  //       this.recoveryWords.word4.length == 0 ||
  //       this.recoveryWords.word5.length == 0 ||
  //       this.recoveryWords.word6.length == 0 ||
  //       this.recoveryWords.word7.length == 0 ||
  //       this.recoveryWords.word8.length == 0 ||
  //       this.recoveryWords.word9.length == 0 ||
  //       this.recoveryWords.word10.length == 0 ||
  //       this.recoveryEmail.length == 0){
  //         this.message = "Please enter all 10 words and your email address!"
  //   } else {
  //     this.logger.debug("### Recover with words: " + JSON.stringify(this.recoveryWords) + " and email: " + this.recoveryEmail);
  //     let recoveryArray = [];
  //     recoveryArray.push([this.recoveryWords.word1.toLowerCase(), 
  //                         this.recoveryWords.word2.toLowerCase(),
  //                         this.recoveryWords.word3.toLowerCase(),
  //                         this.recoveryWords.word4.toLowerCase(),
  //                         this.recoveryWords.word5.toLowerCase(),
  //                         this.recoveryWords.word6.toLowerCase(),
  //                         this.recoveryWords.word7.toLowerCase(),
  //                         this.recoveryWords.word8.toLowerCase(),
  //                         this.recoveryWords.word9.toLowerCase(),
  //                         this.recoveryWords.word10.toLowerCase()
  //                       ]);
  //     try{
  //       // create CSCCrypto object with recovery words and email address as salt
  //       let cscCrypto = new CSCCrypto(recoveryArray, this.recoveryEmail);
  //       // loop over accounts and check if they are activated
  //       for(let i=0; i<=999; i++){
  //         // generate keypair for sequence
  //         let keyPair:CSCKeyPair = cscCrypto.generateKeyPair(i);
  //         this.logger.debug("### KeyPair["+i+"]: " + JSON.stringify(keyPair));
  //         // check account balance

  //         // if balance > 0 then add to local wallet and continue with next

  //         // if balance == 0 stop checking
  //         break;
  //       }
  //     } catch (error) {
  //       this.logger.error("### Decrypt Error: " + error);
  //       this.message = "Can not restore wallet with entered words and email.";
  //     }
  //   }
  // }

  // recoverPin() {
  //   this.message = "";
  //   if (this.recoveryWords.word1.length == 0 ||
  //       this.recoveryWords.word2.length == 0 ||
  //       this.recoveryWords.word3.length == 0 ||
  //       this.recoveryWords.word4.length == 0 ||
  //       this.recoveryWords.word5.length == 0 ||
  //       this.recoveryWords.word6.length == 0 ||
  //       this.recoveryWords.word7.length == 0 ||
  //       this.recoveryWords.word8.length == 0){
  //           this.message = "Please enter all 8 words!"
  //   } else {
  //     this.logger.debug("### Recover with words: " + JSON.stringify(this.recoveryWords));
  //     let recoveryArray = [];
  //     recoveryArray.push([this.recoveryWords.word1.toLowerCase(), 
  //                         this.recoveryWords.word2.toLowerCase(),
  //                         this.recoveryWords.word3.toLowerCase(),
  //                         this.recoveryWords.word4.toLowerCase(),
  //                         this.recoveryWords.word5.toLowerCase(),
  //                         this.recoveryWords.word6.toLowerCase(),
  //                         this.recoveryWords.word7.toLowerCase(),
  //                         this.recoveryWords.word8.toLowerCase()
  //                       ]);
  //     try{
  //       // let mnemonicRecovery = this.walletService.getMnemonicRecovery();
  //       let mnemonicRecovery = this.localStorageService.get(AppConstants.KEY_WALLET_MNEMONIC_RECOVERY);
  //       this.logger.debug("### Mnemonic Recovery: " + mnemonicRecovery);
  //       let pin = new CSCCrypto(recoveryArray, this.recoveryEmail).decrypt(mnemonicRecovery);
  //       this.logger.debug("### Mnemonic Password: " + pin);
  //       let alert = this.alertCtrl.create({
  //         title: 'Recover PIN',
  //         subTitle: 'Your BRM PIN is: ' + pin,
  //         buttons: ['OK']
  //       });
  //       alert.onDidDismiss(() => {
  //         this.cancel();
  //       });
  //       alert.present();
  //     } catch (error) {
  //       this.logger.error("### Decrypt Error: " + error);
  //       this.message = "Can not decrypt pin with entered words.";
  //     }
  //   }
  // }

  gotoMainPage(){
    this.navCtrl.setRoot(MainPage);
  }
}
