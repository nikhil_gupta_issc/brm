import { Component } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { BRMService } from '../../providers/brm.service';
import { LokiAccount, LokiTransaction } from '../../domain/lokijs';
import { LogService } from '../../providers/log.service';
import { WalletService } from '../../providers/wallet.service';
import { MarketService } from '../../providers/market.service';
import { AppConstants } from '../../domain/app-constants';
import { CSCUtil } from '../../domain/csc-util';
import Big from 'big.js';
import { TransactionDetailsPage } from '../transaction-details/transaction-details';

@IonicPage()
@Component({
  selector: 'page-transactions',
  templateUrl: 'transactions.html',
})
export class TransactionsPage {

  connectedText: string = "Disconnected";
  connectedColor: string = "disconnected";

  defaultAccount: LokiAccount;
  walletBalance: string;
  fiatBalance: string;
  transaction_count: number = 0;
  transactions: Array<LokiTransaction> = [];

  constructor( public navCtrl: NavController, 
               public navParams: NavParams,
               private casinocoinService: CasinocoinService,
               private logger: LogService,
               private walletService: WalletService,
               public brmService: BRMService,
               public marketService: MarketService,
               private currencyPipe: CurrencyPipe) {
    // subscribe to the openWallet subject
    let openWalletSubject = this.walletService.openWalletSubject;
    openWalletSubject.subscribe( result => {
      if(result == AppConstants.KEY_LOADED){
        this.logger.debug("### TX Wallet Open ###");
        let userAccounts = this.walletService.getAllAccounts();
        this.logger.debug("### TX Accounts: " + JSON.stringify(userAccounts));
        if(userAccounts.length > 0){
          this.defaultAccount = userAccounts[0];
          // load all transactions
          this.transactions = this.walletService.getAllTransactions();
          // update balance
          this.doBalanceUpdate();
          // update transaction count
          this.doTransacionUpdate();
        }
        this.logger.debug("### TX currentUser: " + JSON.stringify(this.brmService.currentUser));
      }
    });
    // subscribe to CasinoCoin connections
    this.casinocoinService.casinocoinConnectedSubject.subscribe( connected => {
      if(connected){
        // set connected header
        this.connectedText = "Connected";
        this.connectedColor = "connected";
         // subscribe to transaction updates
         this.casinocoinService.transactionSubject.subscribe( tx => {
            this.doTransacionUpdate();
          });
          // subscribe to account updates
          this.casinocoinService.accountSubject.subscribe( account => {
            this.doBalanceUpdate();
          });
      } else {
        // set connected header
        this.connectedText = "Disconnected";
        this.connectedColor = "disconnected";
      }
    });
  }

  ionViewDidLoad() {
    this.logger.debug('### TX ionViewDidLoad TransactionsPage');
  }

  trasactionDetails(tx) {
    this.navCtrl.push(TransactionDetailsPage, {source: tx.accountID, destination: tx.destination, hash: tx.txID, amount: tx.amount, legderIndex: tx.inLedger});
  }  

  doTransacionUpdate(){
    this.transaction_count = this.walletService.getWalletTxCount() ? this.walletService.getWalletTxCount() : 0;
    if(this.transaction_count > 0){
      // load all transactions
      this.transactions = this.walletService.getAllTransactions();
    }
    // let lastTX = this.walletService.getWalletLastTx();
    // if(lastTX != null){
    //     this.last_transaction = lastTX.timestamp;
    // }
  }

  doBalanceUpdate(){
    // let balance = new Big(this.walletService.getWalletBalance() ? this.walletService.getWalletBalance() : "0");
    let balance = new Big(this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) ? this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) : "0");
    // let balance = new Big(CSCUtil.cscToDrops();
    this.logger.debug("### TX Wallet Balance: " + balance);
    this.casinocoinService.serverStateSubject.subscribe( server =>{
      if(server.complete_ledgers.length > 0){
        let reserve = new Big(server.validated_ledger.reserve_base);
        this.logger.debug("### TX Account Reserve: " + reserve);
        this.walletBalance = CSCUtil.dropsToCsc(balance.minus(reserve));
        this.logger.debug("### TX balance: " + this.walletBalance);
        if (this.marketService.coinMarketInfo) {
          this.logger.debug("### TX fiat value: " + this.marketService.coinMarketInfo.price_fiat);
          // this.fiatBalance = "$ " + new Big(this.walletBalance).times(new Big(this.marketService.coinMarketInfo.price_usd)).toFixed(4);
          let fiatValue = new Big(this.walletBalance).times(new Big(this.marketService.coinMarketInfo.price_fiat)).toFixed(4);
          this.fiatBalance = this.currencyPipe.transform(fiatValue, this.marketService.fiatCurrency, true, "1.2-4");
        }
        this.logger.debug("### TX Wallet Balance: " + this.walletBalance);
      }
    });
  }

  getTransactionIcon(rowData: LokiTransaction, index:number){
    if (rowData.transactionType === "KYCSet") {
      return "person";
    } else if(rowData.direction == AppConstants.KEY_WALLET_TX_OUT){
      // outgoing tx - check if operator or not
      if(this.brmService.getOperatorByAccountID(rowData.destination) == undefined){
        // not an operator so normal out
        return "icon-minus";
      } else {
        // operator so Deposit
        // svg was created with reverse name, need to change it (UI fix)
        return "icon-withdrawal";
      }
    } else if(rowData.direction == AppConstants.KEY_WALLET_TX_IN){
      // incomming tx
      let operator = this.brmService.getOperatorByAccountID(rowData.accountID);
      // check if activation tx
      if(index == (this.transaction_count-1)){
        return "icon-csc";
      } else if(operator == undefined){
        // not an operator so normal in
        return "icon-plus";
      } else {
        // operator so Withdaw
        return "icon-deposit";
      }
    } else {
      // wallet tx
      return "swap";
    }
  }

  getTransactionText(rowData: LokiTransaction, index: number){
    if (rowData.transactionType === "KYCSet") {
      return "KYC Set";
    } else if(rowData.direction == AppConstants.KEY_WALLET_TX_OUT){
      // outgoing tx - check if operator or not
      let operator = this.brmService.getOperatorByAccountID(rowData.destination);
      if(operator == undefined){
        // not an operator so normal out
        return "Outgoing";
      } else {
        // operator deposit so operator name
        return operator.DisplayName;
      }
    } else if(rowData.direction == AppConstants.KEY_WALLET_TX_IN){
      // incomming tx
      let operator = this.brmService.getOperatorByAccountID(rowData.accountID);
      // check if activation tx
      if(index == (this.transaction_count-1)){
        return "BRM Activation";
      } else if(operator == undefined){
        // not an operator so normal in
        return "Incoming";
      } else {
        // operator withdaw so operator name
        return operator.DisplayName;
      }
    } else {
      // wallet tx
      return "Wallet Transaction";
    }
  }

  getTransactionColor(rowData: LokiTransaction, index: number){
    if(rowData.direction == AppConstants.KEY_WALLET_TX_OUT){
        return "brand";
    } else if(rowData.direction == AppConstants.KEY_WALLET_TX_IN){
      if(index == this.transaction_count-1){
        return "brand";
      } else {
        return "connected";
      }
    } else {
      // wallet tx
      return "dark";
    }
  }
}
