import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermsOfServicePage } from './terms-of-service';

@NgModule({
  declarations: [ ],
  imports: [
    IonicPageModule.forChild(TermsOfServicePage),
  ],
})
export class TermsOfServicePageModule {}
