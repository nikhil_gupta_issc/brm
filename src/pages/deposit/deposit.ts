import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { BRMService } from '../../providers/brm.service';
import { WalletService } from '../../providers/wallet.service';
import { LokiTransaction } from '../../domain/lokijs';
import { CSCUtil } from '../../domain/csc-util';
import Big from 'big.js';
import { AppConstants } from '../../domain/app-constants';
import { LocalStorageService } from 'ngx-store';
import { CSCAmountPipe } from '../../app/app-pipes.module';

@IonicPage()
@Component({
  selector: 'page-deposit',
  templateUrl: 'deposit.html',
})
export class DepositPage {

  userOperatorAccountID: string;
  operatorAccountID: string;
  operatorID: string;
  destinationTag: number;
  amount:number;
  maxToSend: string = "0.00";
  fees: string = "0.00";
  accountBalance: string = "0.00";
  brmDefaultMode: boolean = true;
  
  constructor( public navCtrl: NavController, 
               public navParams: NavParams,
               public viewCtrl: ViewController,
               private loadingCtrl: LoadingController,
               private logger: LogService,
               private walletService: WalletService,
               public brmService: BRMService,
               private alertCtrl: AlertController,
               public localStorageService: LocalStorageService,
               private cscAmountPipe: CSCAmountPipe ) {
      // get brm mode
      if(this.localStorageService.get(AppConstants.BRM_MODE) === "advance") {
        this.brmDefaultMode = false;
      } else {
        this.brmDefaultMode = true;
      }
      // get navigation params
      this.userOperatorAccountID = this.navParams.get("userOperatorAccountID");
      this.operatorAccountID = this.navParams.get("operatorAccountID");
      this.destinationTag = this.navParams.get("destinationTag");
      this.logger.debug("### User AccountID for Operator: " + this.userOperatorAccountID + 
                        " Operator AccountID: " + this.operatorAccountID +
                        " DestinationTag: " + this.destinationTag);
      if(this.brmDefaultMode) {
        let balance = new Big(this.walletService.getAccountBalance(this.brmService.currentUser.AccountID));
        this.logger.debug("### doBalanceUpdate - " + this.brmService.currentUser.AccountID + ": " + balance.toString());
        this.accountBalance = CSCUtil.dropsToCsc(balance.minus(this.brmService.accountReserveDrops).toString());
        this.fees = CSCUtil.dropsToCsc(new Big(this.brmService.feesDrops).plus(this.brmService.feesDrops));
      } else {
        let balance = new Big(this.walletService.getAccountBalance(this.userOperatorAccountID));
        this.logger.debug("### doBalanceUpdate - " + this.userOperatorAccountID + ": " + balance.toString());
        this.accountBalance = CSCUtil.dropsToCsc(balance.minus(this.brmService.accountReserveDrops).toString());
        this.fees = CSCUtil.dropsToCsc(new Big(this.brmService.feesDrops));
      }
      this.operatorID = this.brmService.getAllOperators().find(x => x.AccountID == this.operatorAccountID).OperatorID;
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DepositPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  checkCoinsAvailability(): boolean {
    if (this.brmDefaultMode) {
      this.maxToSend = CSCUtil.dropsToCsc(new Big(CSCUtil.cscToDrops(this.accountBalance)).minus(this.brmService.feesDrops).minus(this.brmService.feesDrops));
    } else {
      this.maxToSend = CSCUtil.dropsToCsc(new Big(CSCUtil.cscToDrops(this.accountBalance)).minus(this.brmService.feesDrops));
    }
    return new Big(this.amount).lte(this.maxToSend);
  }

  depositFunds(){
    if(this.amount != null && this.amount > 0) {
      if(this.checkCoinsAvailability()) {
        if(this.checkGamblingLimits()) {
          let alert = this.alertCtrl.create({
            title: 'Gaming Limits Exceeded.',
            subTitle: 'You can send up to ' + this.amount + ' coins.',
            buttons: ['OK']
          });
          this.logger.debug("### Deposit - Limit Exceeded. You can send up to " + this.amount);
          alert.present();
        } else {
          let loader = this.loadingCtrl.create({spinner: 'crescent', content: 'Depositing Coins ...',duration: 5000});
          loader.present().then( () => {
            let amountToSend;
            // to remove 999,999.99998001 csc submitted error, if user enter more than 8 decimals with zeros
            if (this.amount.toString().split('.')[1]) {
              amountToSend = this.cscAmountPipe.transform(CSCUtil.cscToDrops(new Big(this.amount)), false, true);
            } else {
              amountToSend = this.amount.toString();
            }
            if(this.brmDefaultMode) {
              // this.amount = parseFloat(this.amount.toString()) + parseFloat(CSCUtil.dropsToCsc(this.brmService.feesDrops));
              // this.brmService.signAndSubmit(this.brmService.currentUser.AccountID, this.userOperatorAccountID, CSCUtil.cscToDrops(this.amount.toString()), this.destinationTag);
              let newAmountDrops = new Big(CSCUtil.cscToDrops(amountToSend)).plus(this.brmService.feesDrops).toString();
              this.brmService.signAndSubmit(this.brmService.currentUser.AccountID, this.userOperatorAccountID, newAmountDrops, this.destinationTag);
            } else {
              this.brmService.signAndSubmit(this.userOperatorAccountID, this.operatorAccountID, CSCUtil.cscToDrops(amountToSend), this.destinationTag);
            }
            loader.dismiss();
            this.dismiss();
          });
        }
      } else {
        let alert = this.alertCtrl.create({
          title: 'Insuficient Balance',
          subTitle: 'You do not have suficient funds to deposit the amount entered!',
          buttons: ['Dismiss']
        });
        alert.onDidDismiss(() => {
          this.amount = parseFloat(this.maxToSend);
        });
        alert.present();
      }
    }
  }

  checkGamblingLimits(): boolean{
    let max24H = -1;
    let max7D  = -1;
    let max30D = -1;
    let max24HTimestamp = 0;
    let max7DTimestamp = 0;
    let max30DTimestamp = 0;
    let useOperatorLimits: boolean = false;
    this.logger.debug("### Deposit - Checking Gaming Limits");
    this.brmService.userWithUpdatedLimitsTime();
    if(this.brmService.currentUser.UserGamingLimits) {
      // check if we need to use AllLimits or Operator Limits
      // check if All is enabled
      if(this.brmService.currentUser.UserGamingLimits.AllEnabled){
        if(this.brmService.currentUser.UserGamingLimits.All24HEnabled) {
          max24H = this.brmService.currentUser.UserGamingLimits.All24H;
          max24HTimestamp = this.brmService.currentUser.UserGamingLimits.All24Timestamp;
        }
        if(this.brmService.currentUser.UserGamingLimits.All7DEnabled) {
          max7D = this.brmService.currentUser.UserGamingLimits.All7D;
          max7DTimestamp = this.brmService.currentUser.UserGamingLimits.All7DTimestamp;
        }
        if(this.brmService.currentUser.UserGamingLimits.All30DEnabled) {
          max30D = this.brmService.currentUser.UserGamingLimits.All30D;
          max30DTimestamp = this.brmService.currentUser.UserGamingLimits.All30DTimestamp;
        }
      }
      let operatorLimits = this.brmService.currentUser.UserGamingLimits.OperatorLimits.find(x => x.OperatorID === this.operatorID);
      if(operatorLimits != undefined){
        // check if enabled
        if(operatorLimits.Enabled24H){
          max24H = operatorLimits.Value24H;
          max24HTimestamp = operatorLimits.TimestampSet24H;
          useOperatorLimits = true;
        }
        if(operatorLimits.Enabled7D){
          max7D = operatorLimits.Value7D;
          max7DTimestamp = operatorLimits.TimestampSet7D;
          useOperatorLimits = true;
        }
        if(operatorLimits.Enabled30D){
          max30D = operatorLimits.Value30D;
          max30DTimestamp = operatorLimits.TimestampSet30D;
          useOperatorLimits = true;
        }
      }
      this.logger.debug("### Deposit - max24H: " + max24H + " max7D: " + max7D + " max30D: " + max30D);
      // check if we need to set limit
      if (max24H >= 0 || max7D >= 0 || max30D >=0) {
        // calculate tx totals for 24H, 7D and 24H
        // 24H = 86,400 seconds
        // 7D = 604,800 seconds
        // 30D = 2,592,000 seconds
        let setTimestamp = 0;
        let setLimit = 0;
        let time = "";
        if (max24H >= 0) {
          time = "24 hours";
          setLimit = max24H;
          setTimestamp = max24HTimestamp;
        } else if (max7D >= 0) {
          time = "7 days";
          setLimit = max7D;
          setTimestamp = max7DTimestamp;
        } else  if (max30D >= 0) {
          time = "30 days";
          setLimit = max30D;
          setTimestamp = max30DTimestamp;
        }
        this.logger.debug("### Deposit - Set Coins Limit: " + setLimit 
          + ", Duration: " + time
          + ", Set Time: " + CSCUtil.casinocoinTimeToISO8601(setTimestamp)
          + ", Current Time: " + new Date().toISOString());
        let accountTxArray:Array<LokiTransaction> = this.walletService.getAccountTransactions(this.userOperatorAccountID);
        let totalBalance: Big = new Big("0");
        accountTxArray.forEach(tx => {
          if (tx.accountID == this.userOperatorAccountID && tx.destination == this.operatorAccountID) {
            if (setTimestamp < tx.timestamp) {
              this.logger.debug("### Deposit - List of Sent Coins with in limit " + CSCUtil.dropsToCsc(tx.amount));
              totalBalance = totalBalance.plus(tx.amount);
            }
          }
        });
        this.logger.debug("### Deposit - Total Coins sent within Limit time: " + CSCUtil.dropsToCsc(totalBalance));
        let totalSentPlusCurrentSending = new Big(CSCUtil.dropsToCsc(totalBalance)).plus(this.amount);
        if (totalSentPlusCurrentSending.gt(setLimit)) {
          this.amount = new Big(this.amount).minus(totalSentPlusCurrentSending.minus(setLimit));
          if (new Big(this.amount).lt(new Big(0))) {
            this.amount = new Big(0);
          }
          return true;
        } else {
          this.logger.debug("### Deposit - Sent coins did not exceed. Continue transaction");
          return false;
        }
      } else {
        this.logger.debug("### Deposit - gaming limits less than zero or not active");
        return false;
      }
    } else {
      this.logger.debug("### Deposit - no gaming limits added to user");
      return false;
    }
  }
}
