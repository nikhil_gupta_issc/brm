import { Component, ViewChild, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ModalController, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidatorService } from '../../providers/validator.service';
import { LogService } from '../../providers/log.service';
import { MainPage } from '../main/main';
import { User, VerificationStatus, BASE_PATH } from '../../domain/brm-api';
import { WebsocketService } from '../../providers/websocket.service';
import { DatePicker } from '@ionic-native/date-picker';
import { DatePipe } from '@angular/common';
import { CountrySearchComponent } from '../../components/country-search/country-search';
import * as CountryStates from 'node-countries';
import * as LokiTypes from '../../domain/lokijs';
import { AppConstants } from '../../domain/app-constants';
import { LocalStorageService } from 'ngx-store';
import { BRMService } from '../../providers/brm.service';
import { CSCCrypto, CSCKeyPair } from '../../domain/csc-crypto';
import { HttpClient } from '@angular/common/http';
import { CustomModalComponent } from '../../components/custom-modal/custom-modal';
import { WalletService } from '../../providers/wallet.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { UUID } from 'angular2-uuid';
import { HomePage } from '../home/home';
import { Keyboard } from '@ionic-native/keyboard';

declare var Jumio;

@IonicPage()
@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html',
})
export class SetupPage {

  @ViewChild('fullName') fullName;
  @ViewChild('birthDate') birthDate;

  progressBarPercent: string = "10%";
  appTitle: string = "Create Account";

  fgPersonalInfo: FormGroup;
  fgContactInfo: FormGroup;
  fgAddressInfo: FormGroup;
  fgRecoveryPhaseCheck: FormGroup;

  stepIndex = 0;
  showHeader = true;
  walletCreationSuccess = false;
  walletCreationFail = false;
  previousStepEnabled: boolean = true;
  nextStepEnabled: boolean = false;

  pinSubmitted: boolean = false;
  pinVerified: boolean = false;
  showCustomPin: boolean = false;
  enteredPinCode: string = "";
  submittedPinCode: string = "";
  private decryptedPIN: string;

  walletUUID: string;
  userProfile: User;
  defaultKeyPair: LokiTypes.LokiKey;

  defaulLanguage: string = "en";
  defaultCurrency: string = "USD";

  isCordova: boolean = false;
  selectedCountry: any;

  statesAvailable: boolean = true;
  countryStates: Array<string> = [];

  // final status for SignUp 
  accountCreated: boolean = false;
  accountCreatedFailed:boolean = false;
  cscConnected: boolean = false;
  cscConnectedFailed: boolean = false;
  accountActivated: boolean = false;
  accountActivatedFailed: boolean = false;
  kycVerified: boolean = false;
  kycVerifiedFailed: boolean = false;

  kycVerificationDone: boolean = false;

  recoveryMnemonicWords: Array<string>;

  terms_of_service: string = "";
  privacy_policy: string = "";

  // jumio api token and secret (MOVE out of source code !!!)
  jumioToken: string = "654da134-d533-4d86-aabe-749568459529";
  jumioSecret: string = "OA9NKY8hW9R0HeX4yaZhrBSh3WINX5TY";
  jumioCallbackUrl = "";
  jumioDocUploaded = 0;

  accountCreationModal: any;
  loader: any;
  emailAddressAsSalt: string = "";
  recoverLastInputsBoolean: boolean = false;

  recoveryPhaseOneAgreed: boolean = false;
  recoveryPhaseTwoAgreed: boolean = false;

  randomNumberWordOne: number;
  randomNumberWordTwo: number;
  randomNumberWordOneCorrect: boolean = false;
  randomNumberWordTwoCorrect: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public datePicker: DatePicker,
    public datepipe: DatePipe,
    public modalCtrl: ModalController,
    public platform: Platform,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public keyboard: Keyboard,
    public http: HttpClient,
    public logger: LogService,
    public brmService: BRMService,
    public walletService: WalletService,
    public webSocketService: WebsocketService,
    private casinocoinService: CasinocoinService,
    public localStorageService: LocalStorageService,
    @Inject(BASE_PATH) basePath: string) {

    // Init user profile object
    this.userProfile = this.initUserProfile();

    this.fgPersonalInfo = this.formBuilder.group({
      fullName: ['', [Validators.required, ValidatorService.nameValidator]],
      emailaddress: ['', [Validators.required, ValidatorService.emailValidator]],
      telephone: ['', [Validators.required, Validators.minLength(7)]]
    });
    this.fgPersonalInfo.valueChanges.subscribe(value => {
      if (this.recoverLastInputsBoolean || this.kycVerificationDone) {
        let email = this.fgPersonalInfo.value.emailaddress.toLowerCase().trim();
        if (email !== this.emailAddressAsSalt) {
          let alert = this.alertCtrl.create({
            subTitle: "Your email address is associated with Account ID. Modification is not possible.",
            buttons: [{
              text: 'Ok'
            }]
          });
          alert.present();
          alert.onDidDismiss(() => {
            this.fgPersonalInfo.controls['emailaddress'].patchValue(this.userProfile.Emailaddress);
          });
        }
      }
      this.nextStepEnabled = this.fgPersonalInfo.valid;
    });

    this.fgContactInfo = this.formBuilder.group({
      gender: ['', Validators.required],
      dateOfBirth: ['', [Validators.required, ValidatorService.ageValidator]],
      dobDisplay: [''],
      nationality: ['', Validators.required],
      nationalityISOCode: ['', Validators.required],
      country: ['', Validators.required],
      countryISOCode: ['', Validators.required],
      countryCallingCode: ['']
    });
    this.fgContactInfo.valueChanges.subscribe(value => {
      if (this.fgContactInfo.value.gender !== '' && this.fgContactInfo.value.dateOfBirth !== '' && this.fgContactInfo.value.nationality !== '' && this.fgContactInfo.value.country !== '') {
        this.nextStepEnabled = this.fgContactInfo.valid;
      }
    });
    // check age valid
    this.fgContactInfo.controls['dateOfBirth'].valueChanges.subscribe(value => {
      this.logger.debug("### fgContactInfo dateOfBirth changed: " + JSON.stringify(value));
      this.logger.debug("### fgContactInfo.dateOfBirth.valid: " + this.fgContactInfo.controls['dateOfBirth'].valid);
      this.logger.debug("### fgContactInfo.dateOfBirth.touched: " + this.fgContactInfo.controls['dateOfBirth'].touched);
    });

    this.fgAddressInfo = this.formBuilder.group({
      housenumber: ['', Validators.required],
      street: [''],
      postalcode: ['', [Validators.required, ValidatorService.postalcodeValidator]],
      cca2Code: [''],
      city: ['', Validators.required],
      state: ['']
    });
    this.fgAddressInfo.valueChanges.subscribe(value => {
      if (this.fgAddressInfo.value.housenumber !== '' && this.fgAddressInfo.value.postalcode !== '' && this.fgAddressInfo.value.city !== '') {
        this.nextStepEnabled = this.fgAddressInfo.valid;
      }
    });

    this.fgRecoveryPhaseCheck = this.formBuilder.group({
      wordOne: ['', Validators.required],
      wordTwo: ['', Validators.required]
    });
    this.fgRecoveryPhaseCheck.valueChanges.subscribe(value => {
      if (this.fgRecoveryPhaseCheck.value.wordOne.toLowerCase().trim() === this.recoveryMnemonicWords[this.randomNumberWordOne - 1]) {
        this.randomNumberWordOneCorrect = true;
      } else {
        this.randomNumberWordOneCorrect = false;
      }
      if (this.fgRecoveryPhaseCheck.value.wordTwo.toLowerCase().trim() === this.recoveryMnemonicWords[this.randomNumberWordTwo - 1]) {
        this.randomNumberWordTwoCorrect = true;
      } else {
        this.randomNumberWordTwoCorrect = false;
      }
      if (this.randomNumberWordOneCorrect && this.randomNumberWordTwoCorrect) {
        this.nextStepEnabled = true;
      } else {
        this.nextStepEnabled = false;
      }
    });

    // register back button
    let lastTimeBackPress = 0;
    let timePeriodToExit = 2000;
    platform.registerBackButtonAction(() => {
      if ((new Date().getTime() - lastTimeBackPress) < timePeriodToExit) {
        this.platform.exitApp(); //Exit from app
      } else {
        const toast = this.toastCtrl.create({
          message: 'Press again to exit',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        lastTimeBackPress = new Date().getTime();
      }
    });

    // terms of service & privacy policy    
    this.http.get('assets/terms-of-services.html', { responseType: 'text' }).subscribe(data => {
      this.terms_of_service = data;
    });
    this.http.get('assets/privacy-policy.html', { responseType: 'text' }).subscribe(data => {
      this.privacy_policy = data;
    });

    this.recoveryMnemonicWords = CSCCrypto.getRandomMnemonic();
    this.logger.debug("### SIGNUP recovery mnemonic: " + this.recoveryMnemonicWords);

    this.jumioCallbackUrl = basePath + '/jumio/callback';

    if (this.localStorageService.get(AppConstants.KEY_BRM_USER)) {
      let alert = this.alertCtrl.create({
        title: "Do you want to Recover from last Session?",
        buttons: [
          { text: 'No', role: 'cancel' },
          {
            text: 'Yes', handler: () => {
              this.loader = this.loadingCtrl.create({ spinner: 'crescent', cssClass: 'black-spinner', duration: 60000 });
              this.loader.present().then(() => {
                this.recoverLastInputs(this.localStorageService.get(AppConstants.KEY_BRM_USER)); 
              });
            }
          }]
      });
      alert.present();
    }
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      this.logger.debug("### Signup INIT ###");
      this.logger.debug("### Signup Platforms: " + this.platform.platforms());
      setTimeout(() => {
        this.fullName.setFocus();
        if (this.platform.is('android')) 
          this.keyboard.show();
      }, 150);
      // add platform to user
      if (this.platform.is('cordova')) {
        if (this.platform.is('android')) {
          this.isCordova = true;
          this.userProfile.Platform = 'android';
        } else if (this.platform.is('ios')) {
          this.userProfile.Platform = 'ios';
        } else {
          this.userProfile.Platform = 'cordova';
        }
      } else {
        this.userProfile.Platform = 'mobileweb';
      }
    });
    this.webSocketService.userUpdate.subscribe(user => {
      if (user) {
        this.logger.debug("### SignupPage Updating userProfile ###");
        this.userProfile = user;
      }
    });
  }

  previousStep() {
    if (this.stepIndex === 0) {
      let alert = this.alertCtrl.create({
        title: 'Are you sure you want to cancel Signup?',
        buttons: [
          { text: 'No', role: 'cancel' },
          {
            text: 'Yes', handler: () => { this.navCtrl.setRoot(MainPage); }
          }]
      });
      alert.present();
    } else if (this.stepIndex == 5) {
      if (this.pinSubmitted || this.pinVerified) {
        this.enteredPinCode = "";
        this.pinSubmitted = false;
        this.pinVerified = false;
        this.showCustomPin = true;
      } else {
        this.stepIndex--;
        this.stepChanged();
      }
    } else {
      this.stepIndex--;
      this.stepChanged();
    }
  }

  nextStep() {
    if (this.stepIndex == 6) {
      if (this.recoveryPhaseTwoAgreed) {
        this.stepIndex++;
        this.stepChanged();
      } else {
        let checked = false;
        let alert = this.alertCtrl.create({
          title: 'IMPORTANT!',
          message: "Check the below box to confirm you've written down and safely stored your ten-word recovery phrase, and you understand that if you forget your PIN and lose your recovery phrase, you will lose access to your wallet and the CasinoCoin stored in it.",
          inputs: [{
            type: 'checkbox',
            label: 'I have read and understood.',
            handler: (data) => { if (data.checked) { checked = true; } else { checked = false; } }
          }],
          buttons: [{
            text: 'Continue'
          }]
        });
        alert.present();
        alert.onDidDismiss(() => {
          if (checked) {
            this.logger.debug('### SignUpPage Recovery Passphrase 2 - Agree clicked');
            this.recoveryPhaseTwoAgreed = true;
            this.stepIndex++;
            this.stepChanged();
          } else {
            this.logger.debug('### SignUpPage Recovery Passphrase 2 - Agree clicked without checkbox Checked');
            let checkboxAlert = this.alertCtrl.create({
              title: 'Please check the checkbox to continue',
              buttons: [{
                text: 'Continue'
              }]
            });
            checkboxAlert.present();
          }
        });
      }
    } else if (this.stepIndex == 3) {
      let checked = false;
      let alert = this.alertCtrl.create({
        title: 'CasinoCoin Terms of Service and Privacy Policy',
        message: this.terms_of_service + "<br>" + this.privacy_policy,
        enableBackdropDismiss: false,
        inputs: [{
          type: 'checkbox',
          label: 'I agree to the CasinoCoin Foundation Terms of Service and Privacy Policy',
          disabled: false,
          handler: (data) => { if (data.checked) { checked = true; } else { checked = false; } }
        }],
        buttons: [{
          text: 'Continue'
        }]
      });
      alert.present();
      alert.onDidDismiss(data => {
        if (checked) {
          this.logger.debug('### SignUpPage Terms of Service - Continue clicked redirecting to SignUp Page');
          this.stepIndex++;
          this.stepChanged();
        } else {
          this.logger.debug('### SignUpPage Terms of Service - Continue clicked without checkbox Checked');
          let checkboxAlert = this.alertCtrl.create({
            title: 'Please accept the Terms and Conditions to continue.',
            buttons: [{
              text: 'Ok'
            }]
          });
          checkboxAlert.present();
        }
      });
    } else {
      this.stepIndex++;
      this.stepChanged();
    }
  }

  stepChanged() {
    this.logger.debug("### Current Step: " + this.stepIndex);
    if (this.stepIndex < 4) {
      this.updateUserProfile();
    }

    if (this.stepIndex === 0) {
      // Personal Info
      this.progressBarPercent = "10%";
      this.appTitle = "Create Account";
      this.previousStepEnabled = true;
      this.nextStepEnabled = this.fgPersonalInfo.valid;
    } else if (this.stepIndex === 1) {
      // Contact Info
      this.progressBarPercent = "20%";
      this.previousStepEnabled = true;
      this.nextStepEnabled = this.fgContactInfo.valid;
      let email = this.fgPersonalInfo.value.emailaddress.toLowerCase().trim();
      if (this.emailAddressAsSalt !== email) {
        this.emailAddressAsSalt = email;
        this.generateAccountID();
      }
    } else if (this.stepIndex == 2) {
      // Address Info
      this.progressBarPercent = "35%";
      this.appTitle = "Create Account";
      this.previousStepEnabled = true;
      this.nextStepEnabled = this.fgAddressInfo.valid;
    } else if (this.stepIndex == 3) {
      // KYC Info
      this.progressBarPercent = "50%";
      this.appTitle = "Confirmation";
      this.previousStepEnabled = true;
      this.nextStepEnabled = true;
    } else if (this.stepIndex == 4) {
      // Upload Document
      this.progressBarPercent = "60%";
      this.appTitle = "Confirmation";
      this.previousStepEnabled = true;
      this.showCustomPin = false;
      // skip jumio step in browser version
      if (this.platform.is('cordova')) {
        if (this.kycVerificationDone) {
          this.nextStepEnabled = true;
          this.previousStepEnabled = false;
        } else {
          this.nextStepEnabled = false;
          this.previousStepEnabled = true;
        }
      } else {
        this.nextStepEnabled = true;
      }
      this.activateAccountID();
    } else if (this.stepIndex == 5) {
      // Set PIN
      this.progressBarPercent = "70%";
      this.appTitle = "Wallet PIN";
      this.previousStepEnabled = true;
      if (!this.pinSubmitted && !this.pinVerified) {
        this.showCustomPin = true;
      }
      this.nextStepEnabled = (this.pinSubmitted && this.pinVerified);
      this.blockScreenShot(false);
    } else if (this.stepIndex == 6) {
      // Recovery Passphrase Info
      this.progressBarPercent = "80%";
      this.appTitle = "PIN Code Recovery Phrase";
      this.previousStepEnabled = true;
      this.blockScreenShot(true);
      this.showRecoveryPhraseAlert();
    } else if (this.stepIndex == 7) {
      // Confirm Recovery Phrase
      this.progressBarPercent = "90%";
      this.appTitle = "Confirm Recovery Phrase";
      this.previousStepEnabled = true;
      this.nextStepEnabled = false;
      this.blockScreenShot(false);
      this.fgRecoveryPhaseCheck.controls['wordOne'].patchValue('');
      this.fgRecoveryPhaseCheck.controls['wordTwo'].patchValue('');
      this.randomNumberWordOne = Math.floor((Math.random() * 10) + 1);
      this.randomNumberWordTwo = Math.floor((Math.random() * 10) + 1);
      this.logger.debug("### Confirm Recovery Phrase Random Words - " + this.randomNumberWordOne + "." + this.recoveryMnemonicWords[this.randomNumberWordOne - 1] + 
        ", " + this.randomNumberWordTwo + "." + this.recoveryMnemonicWords[this.randomNumberWordTwo - 1]);
    } else if (this.stepIndex == 8) {
      // Finising Setup
      this.progressBarPercent = "100%";
      this.appTitle = "Final Review";
      this.previousStepEnabled = false;
      this.nextStepEnabled = false;
      this.showHeader = false;
      setTimeout(() => {
        this.finalizeSetup();        
      }, 500);
    }
  }

  blockScreenShot(status: boolean) {
    if (this.platform.is('cordova')) {
      if (status) {
        window['plugins'].preventscreenshot.disable(result => {
          this.logger.debug("### Signup Screen Shot Disabled: " + result);
        }, error => {
          this.logger.debug("### Signup Screen Shot Error: " + error);
        });
      } else {
        window['plugins'].preventscreenshot.enable(result => {
          this.logger.debug("### Signup Screen Shot Enabled: " + result);
        }, error => {
          this.logger.debug("### Signup Screen Shot Error: " + error);
        });
      }
    }
  }

  editFields() {
    this.stepIndex = 0;
    this.stepChanged();
  }

  showRecoveryPhraseAlert() {
    if (this.recoveryPhaseOneAgreed) {
      this.nextStepEnabled = true;
    } else {
      let checked = false;
      let alert = this.alertCtrl.create({
        title: 'IMPORTANT!',
        message: "The following ten-word recovery phrase is the ONLY way you can reset your PIN code if you forget it and restore your wallet. The CasinoCoin Foundation cannot assist with this. Please check the below box to confirm you've read and understand the importance of saving the ten-word recovery phrase.",
        inputs: [{
          type: 'checkbox',
          label: 'I have read and understood.',
          handler: (data) => { if (data.checked) { checked = true; } else { checked = false; } }
        }],
        buttons: [{
          text: 'Continue'
        }]
      });
      alert.present();
      alert.onDidDismiss(() => {
        if (checked) {
          this.logger.debug('### SignUpPage Recovery Passphrase 1 - Agree clicked');
          this.recoveryPhaseOneAgreed = true;
          this.nextStepEnabled = true;
        } else {
          this.logger.debug('### SignUpPage Recovery Passphrase 1 - Agree clicked without checkbox Checked');
          let checkboxAlert = this.alertCtrl.create({
            title: 'Please check the checkbox to continue',
            buttons: [{
              text: 'Continue'
            }]
          });
          checkboxAlert.present();
          checkboxAlert.onDidDismiss(() => {
            this.previousStep();
          });
        }
      });
    }
  }

  generateAccountID() {
    let loader = this.loadingCtrl.create({spinner: 'crescent', content: 'Generating Account ID with Email address', duration: 60000});
    loader.present().then(() => {
      // We finished page 1 and have the users email address
      // Generate and create the main users account using sequence number 0
      let cscCrypto = new CSCCrypto(this.recoveryMnemonicWords, this.userProfile.Emailaddress);
      // generate default keypair for sequence 0
      let cscKeyPair: CSCKeyPair = cscCrypto.generateKeyPair(0);
      this.defaultKeyPair = {
        privateKey: cscKeyPair.privateKey,
        publicKey: cscKeyPair.publicKey,
        accountID: cscKeyPair.accountID,
        secret: cscKeyPair.seed,
        encrypted: false
      }
      this.logger.debug("### Signup Default Account: " + JSON.stringify(this.defaultKeyPair));
      this.userProfile.AccountID = this.defaultKeyPair.accountID;
      this.localStorageService.set(AppConstants.KEY_DEFAULT_ACCOUNT_ID, this.defaultKeyPair.accountID);
      // store mnemonic_hash for later usage
      this.localStorageService.set(AppConstants.KEY_WALLET_MNEMONIC_HASH, cscCrypto.getPasswordKey());
      // encrypt and store mnemonic words
      let crypto = new CSCCrypto(this.defaultKeyPair.accountID, this.userProfile.Emailaddress);
      let encryptedMnemonicWords = crypto.encrypt(JSON.stringify(this.recoveryMnemonicWords));
      this.localStorageService.set(AppConstants.KEY_WALLET_MNEMONIC_WORDS, encryptedMnemonicWords);
      this.localStorageService.set(AppConstants.KEY_BRM_USER, this.userProfile);
  
      this.brmService.loginUser(this.defaultKeyPair).subscribe(loginResult => {
        this.logger.debug("### LoginResult: " + loginResult);
        loader.dismiss();
        if (loginResult == AppConstants.KEY_FINISHED) {
          // connect to BRM websocket
          this.webSocketService.connectToBrmWebsocket();
          if (!this.recoverLastInputsBoolean) {
            this.stepIndex = 1;
            this.stepChanged();
          }
        } else if (loginResult == AppConstants.KEY_ERRORED) {
          this.emailAddressAsSalt = "";
          this.stepIndex = 0;
          this.stepChanged();
          if (!this.casinocoinService.networkConnected) {
            let alert = this.alertCtrl.create({
              title: 'Network Error',
              subTitle: 'Please check your Internet connection.',
              buttons: ['Dismiss']
            });
            alert.present();
          }
        }
      });
    });
  }

  activateAccountID() {
    // activate user accountID on blockchain
    if (!this.accountActivated) {
      this.brmService.activateAccount(this.userProfile).subscribe(result => {
        this.logger.debug("### Account Activation Result: " + JSON.stringify(result));
        if (result['status'] == "OK" || result['status'] == "Account Already Activated") {
          this.accountActivated = true;
        } else {
          this.accountActivatedFailed = true;
        }
      });
    }
  }

  initUserProfile(): User {
    return {
      AccountID: "",
      Active: true,
      Fullname: "",
      Firstname: "",
      Middlename: "",
      Lastname: "",
      DateOfBirth: "",
      Gender: "",
      Nationality: "",
      NationalityISOCode: "",
      Street: "",
      Housenumber: "",
      PostalCode: "",
      State: "",
      City: "",
      Country: "",
      CountryISOCode: "",
      Emailaddress: "",
      Telephonenumber: "",
      KYCStatus: {
        Status: VerificationStatus.NOTVERIFIED
      },
      Registrations: [],
      CreationDate: new Date(),
      Language: "en",
      Currency: "USD",
      LastUpdateDate: new Date()
    };
  }

  updateUserProfile() {
    this.userProfile.Fullname = this.fgPersonalInfo.value.fullName.trim();
    this.userProfile.DateOfBirth = this.fgContactInfo.value.dateOfBirth;
    this.userProfile.Gender = this.fgContactInfo.value.gender;
    this.userProfile.NationalityISOCode = this.fgContactInfo.value.nationalityISOCode;
    this.userProfile.Nationality = this.fgContactInfo.value.nationality;
    this.userProfile.Street = this.fgAddressInfo.value.street;
    this.userProfile.Housenumber = this.fgAddressInfo.value.housenumber;
    this.userProfile.PostalCode = this.fgAddressInfo.value.postalcode.toUpperCase();
    this.userProfile.City = this.fgAddressInfo.value.city;
    this.userProfile.State = this.fgAddressInfo.value.state;
    this.userProfile.CountryISOCode = this.fgContactInfo.value.countryISOCode;
    this.userProfile.Country = this.fgContactInfo.value.country;
    this.userProfile.Emailaddress = this.fgPersonalInfo.value.emailaddress.toLowerCase().trim();
    this.userProfile.Telephonenumber = "+" + this.fgContactInfo.value.countryCallingCode + " " + this.fgPersonalInfo.value.telephone;
    this.userProfile.Language = this.defaulLanguage;
    this.userProfile.Currency = this.defaultCurrency;
    this.localStorageService.set(AppConstants.KEY_BRM_USER, this.userProfile);
    this.logger.debug("### User Profile: " + JSON.stringify(this.userProfile));
  }

  selectDateOfBirth() {
    this.logger.debug("### onClick dateOfBirth");
    this.fgContactInfo.controls['dateOfBirth'].markAsTouched();
  }

  selectCordovaDateOfBirth() {
    this.logger.debug("### onClick dateOfBirth");
    this.fgContactInfo.controls['dateOfBirth'].markAsTouched();
    let selectedDate = this.fgContactInfo.value.dateOfBirth != '' ? new Date(this.fgContactInfo.value.dateOfBirth) : new Date();
    this.datePicker.show({
      mode: 'date',
      date: selectedDate,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        this.logger.debug(" datePicker Got date: " + date);
        this.fgContactInfo.controls['dobDisplay'].patchValue(this.datepipe.transform(date, 'dd-MMM-yyyy'));
        let datePipe = this.datepipe.transform(date, 'yyyy-MM-dd');
        this.fgContactInfo.controls['dateOfBirth'].patchValue(datePipe);
        this.birthDate.setBlur();
      },
      err => {
        this.birthDate.setBlur();
        this.logger.debug("Error occurred while getting date: " + err)
      }
    );
  }

  selectCountry(field) {
    this.logger.debug("### selectCountry: " + field);
    let countryModal;
    if (field == 'nationality') {      
      this.fgContactInfo.controls['nationality'].markAsTouched();
      countryModal = this.modalCtrl.create(CountrySearchComponent, {type: 'nationality'});
      countryModal.present();
      countryModal.onDidDismiss(data => {
        if (data != null) {
          let nationality = data.demonym === "" ? data.name.common : data.demonym;
          this.logger.debug("### Selected Nationality: " + nationality);
          this.fgContactInfo.controls['nationality'].patchValue(nationality);
          this.fgContactInfo.controls['nationalityISOCode'].patchValue(data.cca3);
        }
      });
    } else if (field == 'country') {      
      this.fgContactInfo.controls['country'].markAsTouched();
      countryModal = this.modalCtrl.create(CountrySearchComponent, {type: 'country'});
      countryModal.present();
      countryModal.onDidDismiss(data => {
        // for mobile number caller code & flag selection
        if (data != null) {
          this.logger.debug("### Selected Country: " + data.name.common);
          this.fgContactInfo.controls['country'].patchValue(data.name.common);
          this.fgContactInfo.controls['countryISOCode'].patchValue(data.cca3);
          this.fgContactInfo.controls['countryCallingCode'].patchValue(data.callingCode);
          this.fgAddressInfo.controls['cca2Code'].patchValue(data.cca2);
          this.fgAddressInfo.controls['state'].patchValue('');
          if (this.fgAddressInfo.value.postalcode !== '')
            this.fgAddressInfo.controls['postalcode'].patchValue('');
          this.selectedCountry = data;
          let countryDetails = CountryStates.getCountryByNameOrShortName(data.cca2);
          let countryProvinces = countryDetails.provinces;
          if (countryProvinces) {
            this.statesAvailable = true;
            this.logger.debug("### Country States Added ");
            this.countryStates = [];
            countryProvinces.forEach(element => {
              this.countryStates.push(element.name);
            });
          } else {
            this.statesAvailable = false;
          }
        }
      });
    }
  }

  onStateCancel() {
    this.fgAddressInfo.controls['state'].patchValue('');
  }

  startNetverify() {
    this.logger.debug("### Init Netverify ###");

    Jumio.initNetverify(this.jumioToken, this.jumioSecret, "EU", {
      requireVerification: true,
      requireFaceMatch: true,
      customerId: this.defaultKeyPair.accountID,
      preselectedCountry: this.fgContactInfo.value.countryISOCode,
      cameraPosition: "BACK",
      documentTypes: ["DRIVER_LICENSE", "PASSPORT", "IDENTITY_CARD"],
      callbackUrl: this.jumioCallbackUrl
    });
    this.logger.debug("### Start Netverify");
    
    if (this.platform.is('ios')) {
      setTimeout(() => {
        this.jumioDocUploaded++;
        this.kycVerificationDone = true;
        this.stepIndex++;
        this.stepChanged();
      }, 2000);
    }
    Jumio.startNetverify((documentData, error) => {
      if (error) {
        this.logger.debug("### Netverify Error: " + JSON.stringify(error));
      } else {
        this.logger.debug("### Netverify DocumentData: " + JSON.stringify(documentData));
        this.userProfile.KYCStatus.ScanReference = documentData.scanReference;
        this.userProfile.KYCStatus.ScanSubmittedDate = new Date();
        
        if (!this.platform.is('ios')) {
          this.jumioDocUploaded++;
          this.kycVerificationDone = true;
          this.stepIndex++;
          this.stepChanged();
        }

        this.brmService.saveUser(this.userProfile).subscribe(result => {
          this.logger.debug("### Save User Result: " + JSON.stringify(result));
        });

        // Jumio.initDocumentVerification(this.jumioToken, this.jumioSecret, "EU", {
        //   type: "UB",
        //   customerId: this.defaultKeyPair.accountID,
        //   country: this.fgAddressInfo.value.countryISOCode,
        //   merchantScanReference: documentData.scanReference,
        //   cameraPosition: "BACK"
        // });
        // Jumio.startDocumentVerification((documentData, error) => {
        //   if(error){
        //     this.logger.debug("### DocumentVerification Error: " + JSON.stringify(error));
        //   } else {
        //     this.logger.debug("### DocumentVerification documentData: " + JSON.stringify(documentData));            
        //     this.nextStepEnabled = true;
        //     this.stepIndex++;
        //     this.stepChanged();
        //   }
        // }); 
      }
    });
  }

  handlePinInput(pin: string) {
    this.enteredPinCode += pin;
    if (this.enteredPinCode.length === 6) {
      this.validatePincode();
    }
  }

  validatePincode() {
    if (this.pinSubmitted) {
      if (this.enteredPinCode.length === 6) {
        this.loader = this.loadingCtrl.create({ spinner: 'crescent', cssClass: 'black-spinner', duration: 60000 });
        this.loader.present().then(() => {
          if (this.submittedPinCode === this.enteredPinCode) {
            // pin match
            this.showCustomPin = false;
            let pinConfirmationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'PinConformation', Success: true });
            pinConfirmationModal.present();

            this.pinVerified = true;
            this.walletService.walletPIN = this.submittedPinCode;

            let cscCrypto = new CSCCrypto(this.defaultKeyPair.accountID, this.userProfile.Emailaddress);
            // encrypt and store pin
            let encryptedPIN = cscCrypto.encrypt(this.submittedPinCode);
            this.localStorageService.set(AppConstants.KEY_BRM_PIN, encryptedPIN);

            pinConfirmationModal.onDidDismiss(data => {
              this.loader.dismiss();
              this.nextStepEnabled = (this.pinSubmitted && this.pinVerified);
              this.stepIndex++;
              this.stepChanged();
            });
          } else {
            // pin doesn't match
            this.loader.dismiss();
            this.showCustomPin = false;
            let pinConfirmationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'PinConformation', Success: false });
            pinConfirmationModal.present();
            pinConfirmationModal.onDidDismiss(() => {
              setTimeout(() => {
                this.showCustomPin = true;
              }, 100);
            });

            this.enteredPinCode = "";
            this.pinVerified = false;
            this.pinSubmitted = false;
          }
        });
      }
    } else {
      if (this.enteredPinCode.length === 6) {
        this.submittedPinCode = this.enteredPinCode;
        this.enteredPinCode = "";
        this.pinSubmitted = true;
        this.showCustomPin = false;
        setTimeout(() => {
          this.showCustomPin = true;
        }, 100);
      }
    }
  }

  backspacePin() {
    this.enteredPinCode = this.enteredPinCode.substring(0, this.enteredPinCode.length - 1);
  }

  clearPin() {
    this.enteredPinCode = "";
  }

  createWalletIfNotExists(): Observable<boolean> {
    this.logger.debug("### SIGNUP createWalletIfNotExists");
    let walletCreationCompleteSubject = new BehaviorSubject<boolean>(false);
    // get PIN to set as wallet password
    let cscCrypto = new CSCCrypto(this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID), this.userProfile.Emailaddress);
    this.decryptedPIN = cscCrypto.decrypt(this.localStorageService.get(AppConstants.KEY_BRM_PIN));
    this.walletUUID = UUID.UUID();    
    let recoveryHash = new CSCCrypto(this.recoveryMnemonicWords, this.userProfile.Emailaddress).encrypt(this.decryptedPIN);
    this.logger.debug("### SIGNUP recovery hash: " + recoveryHash);
    this.walletService.createWallet(
      this.walletUUID, // wallet UUID
      this.decryptedPIN,
      LokiTypes.LokiDBEnvironment.test, // CHANGE THIS WHEN GOING TO PRODUCTION !!!!
      recoveryHash).subscribe(createResult => {
        if (createResult == AppConstants.KEY_FINISHED) {
          this.logger.debug("### SIGNUP - Create new Account");
          if (this.defaultKeyPair.accountID.length > 0) {
            this.walletService.addKey(this.defaultKeyPair);
            // create new account
            let walletAccount: LokiTypes.LokiAccount = {
              accountID: this.defaultKeyPair.accountID,
              accountSequence: 0,
              balance: "0",
              lastSequence: 0,
              label: "Default Account",
              activated: false,
              ownerCount: 0,
              lastTxID: "",
              lastTxLedger: 0
            };
            this.walletService.addAccount(walletAccount);
            this.logger.debug("### SIGNUP - Accounts: " + JSON.stringify(this.walletService.getAllAccounts()));
            this.logger.debug("### SIGNUP - Encrypt Wallet Keys");
            this.walletService.encryptAllKeys(this.decryptedPIN).subscribe(result => {
              if (result == AppConstants.KEY_FINISHED) {
                this.logger.debug("### SIGNUP - Key Encryption Finished")
              }
            });
            this.logger.debug("### SIGNUP - Walet Creation Complete");
            this.logger.debug("### SIGNUP - Default AccountID: " + this.userProfile.AccountID);
            this.localStorageService.set(AppConstants.KEY_CURRENT_WALLET, this.walletUUID);
            walletCreationCompleteSubject.next(true);
          }
        }
      });
    return walletCreationCompleteSubject.asObservable();
  }

  finalizeSetup() {
    try {
      this.createWalletIfNotExists().subscribe(result => {
        this.logger.debug("### SIGNUP - Create Wallet If Not Exists: " + result);
        if (result) {
          this.accountCreated = true;
          // create casinocoin connection
          this.casinocoinService.connect().subscribe(connected => {
            if (connected == AppConstants.KEY_CONNECTED) {
              this.logger.debug("### SIGNUP Connected ###");
              this.cscConnected = true;
            } else if (connected == AppConstants.KEY_DISCONNECTED) {
              this.cscConnectedFailed = true;
            }
          });
          this.kycVerified = true;
          this.walletCreationSuccess = true;
          this.nextStepEnabled = true;

          // save created loki db before opening db. auto save takes 5 secs. trying to open db before 5 sec can cause error
          this.walletService.saveWallet();
          // set Setup complete
          this.localStorageService.set(AppConstants.KEY_SETUP_COMPLETED, true);
          this.localStorageService.set(AppConstants.TOUCH_ID_ENABLE, false);

          this.accountCreationModal = this.modalCtrl.create(CustomModalComponent, { Type: 'CreationSuccess', Success: true });
          this.accountCreationModal.present();
          this.accountCreationModal.onDidDismiss(data => {
            // open wallet and navigate to home page
            let walletUUID = this.localStorageService.get(AppConstants.KEY_CURRENT_WALLET);
            this.walletService.openWallet(walletUUID).subscribe(result => {
              this.logger.debug('### SignUp Open Wallet ' + result);
              if (result == AppConstants.KEY_LOADED) {
                this.navCtrl.setRoot(HomePage);
              }
            });
          });
          // needs to done
          // this.brmService.getKycInfoByAccountID(this.userProfile.AccountID).subscribe( result => {
          //   if(result.Status == 'APPROVED_VERIFIED'){
          //     this.kycVerified = true;
          //   } else {
          //     this.kycVerifiedFailed = true;
          //   }
          // });
          this.brmService.saveUser(this.userProfile).subscribe(result => {
            this.logger.debug("### Save User Result: " + JSON.stringify(result));
          });
        }
      });
    } catch (error) {
      this.logger.debug("### Setup Error " + error);
      this.walletCreationFail = true;
      this.accountCreatedFailed = true;
    }
  }

  recoverLastInputs(user: User) {
    this.logger.debug("### Setup - Recover User: " + JSON.stringify(user));
    this.userProfile = user;

    this.fgPersonalInfo.controls['fullName'].patchValue(this.userProfile.Fullname);
    this.fgPersonalInfo.controls['emailaddress'].patchValue(this.userProfile.Emailaddress);
    this.fgPersonalInfo.controls['telephone'].patchValue(this.userProfile.Telephonenumber.split(" ")[1]);

    this.fgContactInfo.controls['gender'].patchValue(this.userProfile.Gender);
    this.fgContactInfo.controls['dateOfBirth'].patchValue(this.userProfile.DateOfBirth);
    if (this.userProfile.DateOfBirth !== "")
      this.fgContactInfo.controls['dobDisplay'].patchValue(this.datepipe.transform(new Date(this.userProfile.DateOfBirth), 'dd-MMM-yyyy'));
    this.fgContactInfo.controls['nationality'].patchValue(this.userProfile.Nationality);
    this.fgContactInfo.controls['nationalityISOCode'].patchValue(this.userProfile.NationalityISOCode);
    this.fgContactInfo.controls['country'].patchValue(this.userProfile.Country);
    this.fgContactInfo.controls['countryISOCode'].patchValue(this.userProfile.CountryISOCode);

    this.fgAddressInfo.controls['housenumber'].patchValue(this.userProfile.Housenumber);
    this.fgAddressInfo.controls['street'].patchValue(this.userProfile.Street);
    this.fgAddressInfo.controls['city'].patchValue(this.userProfile.City);
    this.fgAddressInfo.controls['state'].patchValue(this.userProfile.State);

    if (this.userProfile.Country !== "") {
      let country = this.brmService.countries.filter((item) => {
        return (item.name.common.toLowerCase().startsWith(this.userProfile.Country.toLowerCase()));
      });
      this.selectedCountry = country[0];
      this.fgAddressInfo.controls['cca2Code'].patchValue(country[0].cca2);
      let countryDetails = CountryStates.getCountryByNameOrShortName(country[0].cca2);
      let countryProvinces = countryDetails.provinces;
      if (countryProvinces) {
        this.statesAvailable = true;
        this.logger.debug("### Country States Added ");
        this.countryStates = [];
        countryProvinces.forEach(element => {
          this.countryStates.push(element.name);
        });
      } else {
        this.statesAvailable = false;
      }
    }
    this.fgAddressInfo.controls['postalcode'].patchValue(this.userProfile.PostalCode);

    if (this.userProfile.AccountID !== "") {
      // decrypt and get mnemonic words
      let cscCrypto = new CSCCrypto(this.userProfile.AccountID, this.userProfile.Emailaddress);
      this.recoveryMnemonicWords = JSON.parse(cscCrypto.decrypt(this.localStorageService.get(AppConstants.KEY_WALLET_MNEMONIC_WORDS)));
      this.logger.debug("### SIGNUP recovered mnemonic: " + this.recoveryMnemonicWords);
      this.emailAddressAsSalt = this.userProfile.Emailaddress;
      this.generateAccountID();
    }

    if (this.userProfile.KYCStatus.ScanReference) {
      this.kycVerificationDone = true;
      this.jumioDocUploaded++;
    }

    if (this.localStorageService.get(AppConstants.KEY_BRM_PIN)) {
      let cscCrypto = new CSCCrypto(this.userProfile.AccountID, this.userProfile.Emailaddress);
      this.decryptedPIN = cscCrypto.decrypt(this.localStorageService.get(AppConstants.KEY_BRM_PIN));
      this.logger.debug("### SIGNUP recovered decryptedPIN: " + this.decryptedPIN);
      this.enteredPinCode = this.decryptedPIN;
      this.submittedPinCode = this.decryptedPIN;
      this.walletService.walletPIN = this.decryptedPIN;
      this.pinSubmitted = true;
      this.pinVerified = true;
    }
    this.recoverLastInputsBoolean = true;
    this.nextStepEnabled = true;
    this.loader.dismiss();
  }
}
