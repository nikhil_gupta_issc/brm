import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LogService } from '../../providers/log.service';
import { CSCURI } from '../../domain/csc-types';
import { CSCUtil } from '../../domain/csc-util';
import { LocalStorageService } from 'ngx-store';
import { AppConstants } from '../../domain/app-constants';
import { User } from '../../domain/brm-api';
import { DatePipe } from '@angular/common';
import { BRMService } from '../../providers/brm.service';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-link-account',
  templateUrl: 'link-account.html',
})
export class LinkAccountPage {

  accountID: string = "";
  lastUpdated: string;
  userProfile: User;
  linkDetailShow: boolean = false;
  minDate: string;
  maxDate: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private nav: Nav,
              private barcodeScanner: BarcodeScanner,
              public logger: LogService,
              public localStorageService: LocalStorageService,
              public datepipe: DatePipe,
              public brmService: BRMService) {
      this.minDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
      this.maxDate = this.datepipe.transform(new Date().setDate(new Date().getDate() + 100), 'yyyy-MM-dd'); 
      this.userProfile = this.localStorageService.get(AppConstants.KEY_BRM_USER);
      if(this.userProfile.LinkedAccount == undefined){
        this.userProfile.LinkedAccount = {
          AccountID: "",
          Active: false,
          LastUpdated: this.datepipe.transform(new Date(), 'yyyy-MM-dd')
        };
        this.localStorageService.set(AppConstants.KEY_BRM_USER, this.userProfile);
      } else {        
        this.linkDetailShow = true;
        this.accountID = this.userProfile.LinkedAccount.AccountID;
        this.lastUpdated = this.userProfile.LinkedAccount.LastUpdated;
      }     
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LinkAccountPage');
  }

  scanQRCode(){
    // Scan CasinoCoin QRCode
    this.barcodeScanner.scan().then((barcodeData) => {
      this.logger.debug('### SEND - QR Scanned: ' + JSON.stringify(barcodeData));
      let cscUri: CSCURI = CSCUtil.decodeCSCQRCodeURI(barcodeData.text);
      try {        
        this.accountID = cscUri.address;   
        // let alert = this.alertCtrl.create({
        //   title: 'Not a CSC Account',
        //   subTitle: 'You can only Link CSC Account.',
        //   buttons: ['Dismiss']
        // });
        // alert.present();
        // this.accountID = "";
      } catch (error) {
        this.accountID = barcodeData.text;  
      }
    }, (err) => {
      // An error occurred
      this.logger.error('### SEND QR Error: ' + JSON.stringify(err));
    });
  }

  linkAccount() {
    this.lastUpdated = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    this.userProfile.LinkedAccount = {
      AccountID: this.accountID,
      Active: true,
      LastUpdated: this.lastUpdated
    };
    this.brmService.saveUser(this.userProfile).subscribe(result => {
      this.logger.debug("### LinkAccount User Updated ###");
    }, error => {
      this.nav.setRoot(LoginPage);
      this.brmService.logOut().present();
    });
    this.linkDetailShow = true;
  }

  onLinkActiveChange() {
    if(this.userProfile.LinkedAccount.Active) {
      if(this.userProfile.LinkedAccount.AccountID === ""){
        this.linkDetailShow = false;
      } else {
        this.linkDetailShow = true;
        this.accountID = this.userProfile.LinkedAccount.AccountID;
        this.lastUpdated = this.userProfile.LinkedAccount.LastUpdated;
        this.userProfile.LinkedAccount.Active = true;      
        this.localStorageService.set(AppConstants.KEY_BRM_USER, this.userProfile);    
      }      
    } else {
      this.userProfile.LinkedAccount.Active = false;
      this.localStorageService.set(AppConstants.KEY_BRM_USER, this.userProfile);    
    }
  }

  changeLinkDetails() {   
    this.accountID = "";
    this.linkDetailShow = false;
  }
}
