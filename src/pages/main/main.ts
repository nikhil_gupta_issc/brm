import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Modal } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { LogService } from '../../providers/log.service';
// import { SignupPage } from '../signup/signup';
import { RestorePage } from '../restore/restore';
import { SetupPage } from '../setup/setup';
// import { TermsConditionsComponent } from '../../components/terms-conditions/terms-conditions';

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {

  termsConditions: Modal;
  alert: any;
  checked: boolean = false;
    
  constructor(public navCtrl: NavController, 
            public navParams: NavParams,
            private statusBar: StatusBar,
            public logger: LogService,
            public modalCtrl: ModalController
        ) {
        // set light text on dark status bar
        this.statusBar.styleLightContent();
    }

  ionViewDidLoad() {
    this.logger.debug('### ionViewDidLoad MainPage');
  }

  gotoSignup(){
    // this.navCtrl.setRoot(SignupPage);
    this.navCtrl.setRoot(SetupPage);

    /*
    this.alert = this.alertCtrl.create({
      title: 'BRM Customer Agreement',
      message: 'Please check to indicate that you have read and agree to the terms of the BRM terms and conditions.',
      inputs: [
        {
          type: 'checkbox',
          label:'I Agree ...',
          handler: (data) =>  { 
            if(data.checked) {
              this.checked = true;
            } else {
              this.checked = false;
            }
          }
        }
      ],
      buttons: [
        {
          text: 'Continue',
          handler: data => {
            if(this.checked){
              this.logger.debug('### MainPage - Continue clicked redirecting to SignUp Page');
              this.navCtrl.setRoot(SignupPage);
            } else {
              this.logger.debug('### MainPage - Continue clicked without checkbox Checked');
            }
          }
        }
      ]
    });   
    this.alert.present(); */

    // 
    // this.termsConditions = this.modalCtrl.create(TermsConditionsComponent);
    // this.termsConditions.onDidDismiss(data => {
    //   this.logger.debug("### Terms & Conditions Accepted : " + JSON.stringify(data));
    //   if(data){
    //     this.navCtrl.setRoot(SignupPage);
    //   }
    // });
    // this.termsConditions.present();
  }

  restoreBRM() {
    this.navCtrl.setRoot(RestorePage);
  }
  
}
