import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, 
         NavParams, LoadingController, ModalController,
         AlertController, Platform } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { BRMService } from '../../providers/brm.service';
import { WalletService } from '../../providers/wallet.service';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { LokiAccount } from '../../domain/lokijs';
import { Operator, Registration, User } from '../../domain/brm-api';
import { CSCUtil } from '../../domain/csc-util';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { TransferFundsPage } from '../transfer-funds/transfer-funds';
import { DepositPage } from '../deposit/deposit';
import Big from 'big.js';
import { WebsocketService } from '../../providers/websocket.service';
import { LocalStorageService } from 'ngx-store';
import { AppConstants } from '../../domain/app-constants';

@IonicPage()
@Component({
  selector: 'page-operator-details',
  templateUrl: 'operator-details.html',
})
export class OperatorDetailsPage {

  currentOperator: Operator;
  currentUser: User;
  accountBalance: string = "0.00";
  accountReserve: string = "0.00";
  accountReserveDrops: Big;
  signupAccountReserveDrops: Big;
  userOperatorAccountID: string;
  registration: Registration;
  brmDefaultMode: boolean = true;
  defaultAccountBalance: string = "0.00";

  constructor( public navCtrl: NavController, 
               public navParams: NavParams,
               public viewCtrl: ViewController,
               public alertCtrl: AlertController,
               private platform: Platform,
               private logger: LogService,
               private brmService: BRMService,
               private walletService: WalletService,
               private casinocoinService: CasinocoinService,
               private iab: InAppBrowser,
               public loadingCtrl: LoadingController,
               public modalCtrl: ModalController,
               public webSocketService: WebsocketService,
               public localStorageService: LocalStorageService ) {
    // get brm mode
    if(this.localStorageService.get(AppConstants.BRM_MODE) === "advance") {
      this.brmDefaultMode = false;
    } else {
      this.brmDefaultMode = true;
    }
    // get account reserve
    this.casinocoinService.serverStateSubject.subscribe(state => {
      if(state.complete_ledgers.length > 0){
        this.accountReserveDrops = new Big(state.validated_ledger.reserve_base);
        this.signupAccountReserveDrops = new Big(state.validated_ledger.reserve_base + state.validated_ledger.reserve_base);
        this.accountReserve = CSCUtil.dropsToCsc(this.accountReserveDrops);
        if(this.brmDefaultMode) {
          this.defaultAccountBalance = CSCUtil.dropsToCsc(
            new Big( this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) ).minus(this.accountReserveDrops)
          );
        }
      }
    });
    // get the operator to show
    let operatorID = this.navParams.get('operatorID');
    this.logger.debug("### Operator Detail ID: " + JSON.stringify(operatorID));
    this.currentOperator = this.brmService.getOperatorByID(operatorID);
    this.currentUser = this.brmService.currentUser;
    if(this.brmService.isRegisteredToOperator(this.currentOperator.AccountID)){
      this.setUserAccountIDForOperator();
      this.doBalanceUpdate();
    } else if(this.isUserAccountActivated()){
      this.registration = this.brmService.getUserOperatorRegistration(this.currentOperator.AccountID);
      this.logger.debug("### Operator Detail Registration: " + JSON.stringify(this.registration));
      this.userOperatorAccountID = this.registration.UserAccountIDForOperator;
    }
    // subscribe to CasinoCoin connections
    this.casinocoinService.casinocoinConnectedSubject.subscribe( connected => {
      if(connected){
        // subscribe to account updates
        this.casinocoinService.accountSubject.subscribe( account => {
          this.doBalanceUpdate();
        });
      }
    });
    // subscribe user update through websocket
    this.webSocketService.userUpdate.subscribe(user => {
      this.logger.debug("### OperatorDetailsPage Updating currentUser registration ###");
      if(user){
        this.registration = this.brmService.getUserOperatorRegistration(this.currentOperator.AccountID);
      }
    });
  }

  setUserAccountIDForOperator(){
    if(this.currentOperator !== undefined && this.userOperatorAccountID == undefined){
      this.registration = this.brmService.getUserOperatorRegistration(this.currentOperator.AccountID);
      if(this.registration){
        this.userOperatorAccountID = this.registration.UserAccountIDForOperator;
      }
    }
  }

  doBalanceUpdate(){
    // get the users account balance for the operator
    this.setUserAccountIDForOperator();
    if(this.userOperatorAccountID){
      this.logger.debug("### doBalanceUpdate() - userOperatorAccountID: " + this.userOperatorAccountID);
      let dbBalance = this.walletService.getAccountBalance(this.userOperatorAccountID);
      this.logger.debug("### doBalanceUpdate() - dbBalance: " + dbBalance);
      let balance = new Big(dbBalance);
      this.logger.debug("### doBalanceUpdate - " + this.userOperatorAccountID + ": " + balance.toString());
      this.accountBalance = CSCUtil.dropsToCsc(balance.minus(this.accountReserveDrops).toString());
    }
    if(this.brmDefaultMode) {
      let balance = new Big(this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) ? this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) : "0");
      this.casinocoinService.serverStateSubject.subscribe( server =>{
        if(server.complete_ledgers.length > 0){
          let reserve = new Big(server.validated_ledger.reserve_base);
          this.logger.debug("### Account Reserve: " + reserve);
          this.defaultAccountBalance = CSCUtil.dropsToCsc(balance.minus(reserve));
          this.logger.debug("### SEND Wallet Balance: " + this.defaultAccountBalance);
        }
      });
    }
  }

  isSignupPossible(){
    let balance = new Big(this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) ? this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) : "0");
    if(balance.minus(this.signupAccountReserveDrops) > 0){
      return true;
    } else {
      if(this.brmService.getUserOperatorRegistration(this.currentOperator.AccountID)){
        return true;
      } else {
        return false;
      }
    }
  }

  isDepositPossible(){
    if(new Big(this.accountBalance) > 0){
      return true;
    } else {
      return false;
    }
  }

  isDefaultDepositPossible() {
    if(new Big(this.defaultAccountBalance) > 1){
      return true;
    } else {
      return false;
    }
  }

  isUserAccountActivated(){
    // get the user account from the db
    let reg:Registration = this.brmService.getUserOperatorRegistration(this.currentOperator.AccountID);
    if(this.userOperatorAccountID == undefined && reg !== undefined){
      this.userOperatorAccountID = reg.UserAccountIDForOperator;
    }
    // this.logger.debug("### isUserAccountActivated - userOperatorAccountID: " + this.userOperatorAccountID);
    let accountArray: LokiAccount[] = this.walletService.getAllAccounts();
    // this.logger.debug("### isUserAccountActivated - Accounts: " + JSON.stringify(accountArray));
    let account = accountArray.find(x => x.accountID === this.userOperatorAccountID);
    if(account == undefined){
      // this.logger.debug("### Operator User Account not Found");
      return false;
    } else {
      // this.logger.debug("### isUserAccountActivated: " + account.accountID);
      let accountBalance = new Big(this.walletService.getAccountBalance(account.accountID));
      // this.logger.debug("### isUserAccountActivated - balance: " + accountBalance.toString());
      // let balance = new Big(this.walletService.getAccountBalance(account.accountID) ? this.walletService.getAccountBalance(account.accountID) : "0");
      // this.logger.debug("### isUserAccountActivated - balance: " + accountBalance + " reserve: " + this.accountReserveDrops);
      if(accountBalance.minus(this.accountReserveDrops) >= 0){
        return true;
      } else {
        return false;
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OperatorDetailsPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  activateAccount(){
    // let loading = this.loadingCtrl.create({
    //   content: 'Activating account ...'
    // });
    // loading.present();
    // let loader = setTimeout(() => {
    //   loading.dismiss();
    //   this.notificationService.addMessage({title: "Activation Error", body: "It took too long to activate your account. Please check your network connection and reopen this page to retry."});
    // }, 10000);
    let loader = this.loadingCtrl.create({spinner: 'crescent', content: 'Depositing Coins ...',duration: 5000});
    loader.present().then( () => {
      let keyPair = this.brmService.generateOperatorAccount();
      this.logger.debug("### activateAccount - userOperatorAccountID: " + keyPair.accountID);
      this.brmService.initializeOperatorRegistration(this.currentOperator.AccountID, keyPair.accountID).subscribe(result => {
        this.userOperatorAccountID = this.brmService.activateOperatorAccount(keyPair, this.currentOperator.AccountID);
        this.brmService.registerToOperator(this.currentOperator.AccountID, this.userOperatorAccountID);
        this.registration = this.brmService.getUserOperatorRegistration(this.currentOperator.AccountID);
        this.logger.debug("### BRM Update User Registration " + JSON.stringify(result));
      }, error => {
        this.viewCtrl.dismiss({ status: 'error' });
      });
      loader.dismiss();
      // this.dismiss();
    });
    // clearTimeout(loader);
    // loading.dismissAll();
  }

  registerToOperator(){
    if(!this.isUserAccountActivated()){
      // for first time - create account activate, add to Registration array and save user
      this.activateAccount();
    } else {
      if(this.registration.RegistrationDeleted){
        // when deleted - make RegistrationDeleted false and save user
        let regIndex = this.currentUser.Registrations.findIndex(x => x.UserAccountIDForOperator === this.userOperatorAccountID);
        this.currentUser.Registrations[regIndex].RegistrationDeleted = false;
        this.currentUser.Registrations[regIndex].RegistrationInitiatedDate = new Date();
        this.brmService.saveUser(this.currentUser).subscribe(result => {
          this.logger.debug("### BRM Updated User Registration Deleted to false ###" + JSON.stringify(result));
        }, error => {
          this.viewCtrl.dismiss({ status: 'error' });
        });
      } else {
        // when pending - forward user again to the operator supplying the accountid/token
        this.logger.debug("### BRM User Signup Pending - Redirect again to Operator ###");
        this.brmService.registerToOperator(this.currentOperator.AccountID, this.userOperatorAccountID);
      }
    }
    if (!this.platform.is('cordova')) {
      let prompt = this.alertCtrl.create({
        title: 'Operator Destination Tag',
          message: "Enter operator Destination Tag.",
          inputs: [
            {
              name: 'destinationTag',
              placeholder: 'Destination Tag'
            },
          ],
          buttons: [
            { text: 'Cancel', handler: data => {this.logger.debug("### Deposit Canceled"); }},
            { text: 'Save', handler: data => { 
              this.brmService.finishRegistration(this.navCtrl, this.currentOperator.AccountID, this.userOperatorAccountID, data.destinationTag);
             }}
          ]
        });
      prompt.present();
    }
  }

  transferFunds(){
    let modal = this.modalCtrl.create(TransferFundsPage, { userOperatorAccountID: this.userOperatorAccountID, defaultAccountID: this.brmService.currentUser.AccountID, displayName: this.currentOperator.DisplayName});
    modal.present();
  }

  visitOperator(){
    // open deeplink
    // let link = operator.Deeplink.replace(":action", "register");
    // open weblink
    this.logger.debug("### visitOperator: " + this.currentOperator.Weblink);
    let link = this.currentOperator.Weblink.replace(":action", "user");
    link = link.replace(":accountid", this.userOperatorAccountID);
    this.logger.debug("### BRM Open Link: " + link);
    this.iab.create(link, "_system");
  }

  openDepositePage() {
    let modal = this.modalCtrl.create(DepositPage, { 
      userOperatorAccountID: this.userOperatorAccountID,
      operatorAccountID: this.currentOperator.AccountID,
      destinationTag: this.registration.DestinationTag
    });
    modal.present();
  }

  depositFunds(){
    if(!this.currentUser.UserGamingLimits || !this.currentUser.UserGamingLimits.SelfExclusion && !this.currentUser.UserGamingLimits.SelfExclusionPermanent) {
      this.openDepositePage();
    } else {
      if(this.currentUser.UserGamingLimits.SelfExclusion) {
        let timestamp = CSCUtil.casinocoinTimeToISO8601(this.currentUser.UserGamingLimits.SelfExclusionTimestamp);
        let compareDate = new Date(timestamp);
        compareDate.setMinutes(compareDate.getMinutes() + this.currentUser.UserGamingLimits.SelfExclusionDurationMinutes);
        let difference = compareDate.getTime() - new Date().getTime();  
        if (difference <= 0) {
          this.currentUser.UserGamingLimits.SelfExclusion = false;
          this.currentUser.UserGamingLimits.SelfExclusionTemporaryCheck = false;
          this.brmService.updateUser(this.currentUser).subscribe( result => {
            this.logger.debug("### Updated User ###");
          });
          this.openDepositePage();
        } else {
          let alert = this.alertCtrl.create({
            title: "You will no longer be able to send coins as you have opted for self exclusion.",
            buttons: ['Ok']
          });
          alert.present();
        }
      } else if(this.currentUser.UserGamingLimits.SelfExclusionPermanent) {
        let alert = this.alertCtrl.create({
          title: "You will no longer be able to send coins as you have opted for self exclusion.",
          buttons: ['Ok']
        });
        alert.present();        
      }
    }
    // let prompt = this.alertCtrl.create({
    //   title: 'Deposit to Operator',
    //     message: "Enter the amount you want to deposit",
    //     inputs: [
    //       {
    //         name: 'amount',
    //         placeholder: 'Amount'
    //       },
    //     ],
    //     buttons: [
    //       { text: 'Cancel', handler: data => {this.logger.debug("### Deposit Canceled"); }},
    //       { text: 'Deposit', handler: data => { 
    //         this.brmService.signAndSubmit(this.userOperatorAccountID, this.currentOperator.AccountID, CSCUtil.cscToDrops(data.amount), this.registration.DestinationTag);
    //        }}
    //     ]
    //   });
    // prompt.present();
  }

  // resetOperator(){
  //   this.logger.debug("### Delete Operator Registration: " + JSON.stringify(this.currentOperator));
  //   let regIndex = this.brmService.currentUser.Registrations.findIndex(x => x.OperatorAccountID === this.currentOperator.AccountID);
  //   this.brmService.currentUser.Registrations.splice(regIndex, 1);
  //   this.walletService.removeAccount(this.userOperatorAccountID);
  //   delete this.userOperatorAccountID;
  //   this.dismiss();
  // }

  resetOperator(){
    let alert = this.alertCtrl.create({
      title: "Are you sure?",
      message: "Do you want to Delete registration with '" + this.currentOperator.DisplayName + "' operator",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.logger.debug("### Delete Operator Registration: " + JSON.stringify(this.currentOperator));
            let regIndex = this.brmService.currentUser.Registrations.findIndex(x => x.OperatorAccountID === this.currentOperator.AccountID);
            let sourceAccount = this.brmService.currentUser.Registrations[regIndex].UserAccountIDForOperator;
            this.brmService.currentUser.Registrations[regIndex].RegistrationDeleted = true;
            let accountBalance = new Big(this.walletService.getAccountBalance(sourceAccount));
            accountBalance = accountBalance.minus(this.accountReserveDrops);
            accountBalance = accountBalance.minus(this.brmService.feesDrops);
            if(accountBalance > 0){
              this.logger.debug("### ResetOperator - balance: " + accountBalance + " reserve: " + this.accountReserveDrops);
              this.brmService.signAndSubmit(sourceAccount ,this.currentUser.AccountID, accountBalance.toString());
            }
            this.brmService.saveUser(this.brmService.currentUser).subscribe(result => {
              this.logger.debug("### User Updated " + JSON.stringify(result));
              this.dismiss();            
            }, error => {
              this.viewCtrl.dismiss({ status: 'error' });
            });
          }
        },
        { text: 'Cancel' }
      ]
    });
    alert.present();
  }

}
