import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OperatorDetailsPage } from './operator-details';

@NgModule({
  declarations: [ ],
  imports: [
    IonicPageModule.forChild(OperatorDetailsPage),
  ],
})
export class OperatorDetailsPageModule {}
