import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LogService } from '../../providers/log.service';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { WalletService } from '../../providers/wallet.service';
import { BRMService } from '../../providers/brm.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { CSCUtil } from '../../domain/csc-util';
import { CSCAmountPipe } from '../../app/app-pipes.module';
import { CSCURI, PrepareTxPayment } from '../../domain/csc-types';
import { AppConstants } from '../../domain/app-constants';
import { PincodeController } from  'ionic2-pincode-input';
import Big from 'big.js';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { LocalStorageService } from 'ngx-store';
import { CSCCrypto } from '../../domain/csc-crypto';
import { User } from '../../domain/brm-api';
import { CustomPinComponent } from '../../components/custom-pin/custom-pin';

@IonicPage()
@Component({
  selector: 'page-send',
  templateUrl: 'send.html',
})
export class SendPage {

  connectedText: string = "Disconnected";
  connectedColor: string = "disconnected";

  pinCode: any;
  fgSendCoins: FormGroup;
  totalSend:string = "0.00";
  reserve: number = 0;
  sendCoinsEnabled:boolean = false;
  defaultAccountBalance: string;
  selfExclusionDays: number = 0;
  selfExclusionHours: number = 0;
  selfExclusionMinutes: number = 0;
  selfExclusionSeconds: number = 0;
  selfExclusionTimer: any;

  currentUser:User;

  displayCustomPin: boolean = false;
  enteredPinCode: string = "";

  constructor( public navCtrl: NavController, 
               public navParams: NavParams,
               private formBuilder: FormBuilder,
               public logger: LogService,
               private casinocoinService: CasinocoinService,
               public walletService: WalletService,
               public brmService: BRMService,
               private barcodeScanner: BarcodeScanner,
               private cscAmountPipe: CSCAmountPipe,
               public pincodeCtrl: PincodeController,
               private faio: FingerprintAIO,
               public localStorageService: LocalStorageService,
               private alertCtrl: AlertController,
               public modalCtrl: ModalController) {
    this.currentUser =  this.brmService.currentUser;
    // create form groups
    this.createFormGroup();
    // subscribe to server state
    this.casinocoinService.serverStateSubject.subscribe(server => {
      this.logger.debug("### SEND Server State: " + JSON.stringify(server));
      if(server.complete_ledgers.length > 0){
        this.reserve = server.validated_ledger.reserve_base;
        this.fgSendCoins.controls['fees'].setValue(CSCUtil.dropsToCsc(server.validated_ledger.base_fee.toString()));
        this.defaultAccountBalance = CSCUtil.dropsToCsc(
          new Big( this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) ).minus(new Big(this.reserve))
        );
        this.calculateTotal();
      }
    });
    // subscribe to CasinoCoin connections
    this.casinocoinService.casinocoinConnectedSubject.subscribe( connected => {
      if(connected){
        // set connected header
        this.connectedText = "Connected";
        this.connectedColor = "connected";
        // subscribe to account updates
        this.casinocoinService.accountSubject.subscribe( account => {
          this.logger.debug("### OPERATORS Account Update: " + JSON.stringify(account));
          this.doBalanceUpdate();
      });
      } else {
        // set connected header
        this.connectedText = "Disconnected";
        this.connectedColor = "disconnected";
      }
    });
    // to auto fill Recipient from contact page
    if(this.walletService.contactAccountID.length > 0){
      this.fgSendCoins.controls['recipient'].setValue(this.walletService.contactAccountID);
      this.walletService.contactAccountID = "";
    }
  }

  createFormGroup() {    
    this.fgSendCoins = this.formBuilder.group({
      recipient: ['', Validators.required],
      description: [''],
      destinationTag: [''],
      amount: ['',  Validators.required],
      fees: [{value: "", disabled: true }],
      totalSendFormatted: [{ value: "", disabled: true }],
    });
    this.fgSendCoins.controls['amount'].valueChanges.subscribe( value => {
      this.logger.debug("Send Coins Form Changed: " + JSON.stringify(value));     
      if(value != null && value.length > 0){
        this.calculateTotal();
      }
    });
  }

  doBalanceUpdate(){
    let balance = new Big(this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) ? this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) : "0");
    this.logger.debug("### Wallet Balance: " + balance);
    this.casinocoinService.serverStateSubject.subscribe( server =>{
      if(server.complete_ledgers.length > 0){
        let reserve = new Big(server.validated_ledger.reserve_base);
        this.logger.debug("### Account Reserve: " + reserve);
        this.defaultAccountBalance = CSCUtil.dropsToCsc(balance.minus(reserve));
        this.logger.debug("### SEND Wallet Balance: " + this.defaultAccountBalance);
      }
    });
  }

  scanQRCode(){
    // Scan CasinoCoin QRCode
    this.barcodeScanner.scan().then((barcodeData) => {
      this.logger.debug('### SEND - QR Scanned: ' + JSON.stringify(barcodeData));
      let cscUri: CSCURI = CSCUtil.decodeCSCQRCodeURI(barcodeData.text);
      this.fgSendCoins.controls['recipient'].setValue(cscUri.address);
      if(cscUri.amount !== undefined){
        this.fgSendCoins.controls['amount'].setValue(cscUri.amount);
      } else {
        this.fgSendCoins.controls['amount'].setValue('');
      }
      if(cscUri.label !== undefined){
        this.fgSendCoins.controls['description'].setValue(cscUri.label);
      } else {
        this.fgSendCoins.controls['description'].setValue('');
      }
      if(cscUri.destinationTag !== undefined){
        this.fgSendCoins.controls['destinationTag'].setValue(cscUri.destinationTag);
      } else {
        this.fgSendCoins.controls['destinationTag'].setValue('');
      }
    }, (err) => {
      // An error occurred
      this.logger.error('### SEND QR Error: ' + JSON.stringify(err));
    });
  }

  calculateTotal(): boolean{
    let amount = this.fgSendCoins.get('amount').value;
    let fees = this.fgSendCoins.get('fees').value;
    let amountChanged = false;
    this.logger.debug("### SendCoins - calculateTotal - amount: " + amount + " fees: " + fees);
    if(amount != null && amount.length > 0){
      if(new Big(amount) > 0 && new Big(fees) > 0){
        let amountToSend = new Big(CSCUtil.cscToDrops(amount));
        let maxToSend = new Big(this.walletService.getAccountBalance(this.brmService.currentUser.AccountID))
                          .minus(new Big(CSCUtil.cscToDrops(fees)));
        maxToSend = maxToSend.minus(new Big(this.reserve));
        this.logger.debug("amountToSend: " + amountToSend + " maxToSend: " + maxToSend + " Reserve: " + CSCUtil.dropsToCsc(this.reserve.toString()));
        if(amountToSend.gt(maxToSend)){
          // we need to reduce the amount so we do not pass maxToSend
          if(maxToSend < 0){
            amountToSend = new Big(0);
          } else {
            amountToSend = maxToSend;
          }
          amount = CSCUtil.dropsToCsc(amountToSend.toString());
          this.logger.debug("### Set amount to limited: " + amount);
          amountChanged = true;
        }
        // set total to send
        this.totalSend = CSCUtil.dropsToCsc(amountToSend.plus(new Big(CSCUtil.cscToDrops(fees))));
      } else {
        this.totalSend = "0.00";
      }
    } else {
      this.totalSend = "0.00"
    }
    // format total to send
    this.fgSendCoins.controls['totalSendFormatted'].setValue(this.cscAmountPipe.transform(CSCUtil.cscToDrops(this.totalSend), false, true));
    if(amountChanged){
      this.fgSendCoins.controls['amount'].setValue(amount);
      return false;
    } else {
      return true;
    }
  }

  // createPINPad(){
  //   // define pinpad
  //   this.pinCode =  this.pincodeCtrl.create({
  //     title:'BRM PIN Code',
  //     hideForgotPassword: true
  //   });
  //   this.pinCode.onDidDismiss( (code,status) => {
  //     this.logger.debug("### Pinpad status: " + status);
  //     if(status === 'done'){
  //       let decryptedPIN = "";
  //       try {
  //         let cscCrypto = new CSCCrypto(this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID), this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
  //         let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
  //         decryptedPIN = cscCrypto.decrypt(storedPIN);
  //       } catch (e) {
  //         this.logger.debug("### Decrypting PIN Went wrong ###");
  //         decryptedPIN = code;
  //       }
  //       if(code == decryptedPIN){
  //         this.logger.debug("### PIN: " + JSON.stringify(code));
  //         this.signAndSubmit(code);
  //         // this.validateAccountAndSubmit(code);
  //       } else {
  //         let alert = this.alertCtrl.create({
  //           title: 'Invalid PIN',
  //           subTitle: 'You entered an invalid PIN. Please retry!',
  //           buttons: ['Dismiss']
  //         });
  //         alert.present();
  //       }
  //     }
  //   });
  //   this.pinCode.present();
  // }

  // handlePinInput(pin: string) {
  //   this.enteredPinCode += pin;
  //   if (this.enteredPinCode.length === 6) {
  //     this.displayCustomPin = false;
  //     setTimeout(() => {
  //       this.validatePincode();        
  //     }, 100);
  //   }
  // }

  // backspacePin() {
  //   this.enteredPinCode = this.enteredPinCode.substring(0, this.enteredPinCode.length - 1);
  // }

  // cancelPin() {
  //   this.enteredPinCode = "";
  //   this.displayCustomPin = false;
  // }

  showCustomPin() {
    let modal = this.modalCtrl.create(CustomPinComponent, { pageTitle: "Enter PIN code" });
    modal.present();
    modal.onDidDismiss(data => {
      if(data) {
        this.enteredPinCode = data;
        this.validatePincode();
      }
    });
    // this.displayCustomPin = true;
  }

  validatePincode() {
    if(this.enteredPinCode.length === 6){
      let decryptedPIN = "";
      try {
        let cscCrypto = new CSCCrypto(this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID), this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
        let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
        decryptedPIN = cscCrypto.decrypt(storedPIN);
      } catch (e) {
        this.logger.debug("### Decrypting PIN Went wrong ###");
        decryptedPIN = this.enteredPinCode;
      }
      if(this.enteredPinCode === decryptedPIN) {
        this.logger.debug("### Correct PIN ###");
        // this.cancelPin();
        this.signAndSubmit(decryptedPIN);
      } else {
        let alert = this.alertCtrl.create({
          title: 'Invalid PIN',
          subTitle: 'You entered an invalid PIN. Please retry!',
          buttons: ['Dismiss']
        });
        alert.onDidDismiss(() => {
          this.enteredPinCode = "";
          this.showCustomPin();
        });
        alert.present();
      }
    }
  }

  showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: message,
      buttons: ['Ok']
    });
    alert.present();
  }

  isValidToSend(): boolean {
    if (!CSCUtil.validateAccountID(this.fgSendCoins.get('recipient').value)) {
      this.showAlert("Invalid Account ID.");
      return false;
    }
    if (parseFloat(this.fgSendCoins.get('amount').value) <= 0.0) {
      this.showAlert("Amount should be more than zero.");
      return false;
    }
    if (this.fgSendCoins.get('recipient').value === this.brmService.currentUser.AccountID) {
      this.showAlert("Main account and recipient can't be same.");
      return false;
    }
    if (!this.calculateTotal()) {
      this.showAlert("Cannot send CSC entered. Reverting to possible sending CSC.");
      return false;
    } else {
      return true;
    }
  }

  sendCoins(){
    if (this.isValidToSend()) {
      if(this.localStorageService.get(AppConstants.TOUCH_ID_ENABLE)) {
        this.faio.show({
          clientId: 'CasinoCoin BRM',
          clientSecret: 'password'
        })
        .then((result: any) => {
          this.logger.debug("### FingerPrint: " + JSON.stringify(result));
          let decryptedPIN = "";
          let cscCrypto = new CSCCrypto(this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID), this.localStorageService.get(AppConstants.KEY_BRM_USER).Emailaddress);
          let storedPIN = this.localStorageService.get(AppConstants.KEY_BRM_PIN);
          decryptedPIN = cscCrypto.decrypt(storedPIN);
          this.signAndSubmit(decryptedPIN);
          // this.validateAccountAndSubmit(decryptedPIN);
        })
        .catch((error: any) => {
          // create PIN pad
          this.logger.debug("### SEND COINS Fingerprint Error: " +JSON.stringify(error));
          // this.createPINPad();
          this.showCustomPin();
        });    
      } else {
        // create PIN pad
        // this.createPINPad();
        this.showCustomPin();
      }
    }
  }

  signAndSubmit(password){
    let amount = this.fgSendCoins.get('amount').value;
    if (amount.split('.')[1]) {
      // to remove 999,999.99998001 csc submitted error, if user enter more than 8 decimals with zeros
      this.fgSendCoins.controls['amount'].setValue(this.cscAmountPipe.transform(CSCUtil.cscToDrops(new Big(amount)), false, true));
    }
    try {
      let preparePayment: PrepareTxPayment = 
        { source: this.brmService.currentUser.AccountID, 
          destination: this.fgSendCoins.get('recipient').value, 
          amountDrops: CSCUtil.cscToDrops(this.fgSendCoins.get('amount').value),
          feeDrops: CSCUtil.cscToDrops(this.fgSendCoins.get('fees').value),
          description: this.fgSendCoins.get('description').value
        };
      if(this.fgSendCoins.get('destinationTag').value.length > 0){
        preparePayment.destinationTag = this.fgSendCoins.get('destinationTag').value;
      }
      let txObject = this.casinocoinService.createPaymentTx(preparePayment);
      this.logger.debug("### Sign: " + JSON.stringify(txObject));

      let txBlob:string = this.casinocoinService.signTx(txObject, password);
      if(txBlob == AppConstants.KEY_ERRORED){
        // probably a wrong password!
        this.logger.error("Signing failed - Verify Password");
        // this.messageService.add({severity:'error', summary:'Transaction Signing', detail:'There was an error signing the transactions. Verify your password.'});
      } else {
        this.casinocoinService.submitTx(txBlob);        
        // reset form and dialog fields
        // this.fgSendCoins.reset();
        this.createFormGroup();
      }      
    } catch (error) {
      this.logger.error("Send failed - Error: " + JSON.stringify(error));
      let alert = this.alertCtrl.create({
        title: "Unknown error occurred.",
        subTitle: "Cannot send CSC to recipient at moment.",
        buttons: ['Ok']
      });
      alert.present();
    }
  }

  // validateAccountAndSubmit(password){    
  //   let operators = this.brmService.getAllOperators();
  //   operators.forEach(element => {
  //     if(element.AccountID === this.fgSendCoins.get('recipient').value) {
  //       let alert = this.alertCtrl.create({
  //         title: "Cannot send CSC to Operator directly.",
  //         buttons: ['Ok']
  //       });
  //       alert.present();
  //       return;
  //     }
  //   });
  //   let recipient = this.fgSendCoins.get('recipient').value;
  //   let preparePayment: PrepareTxPayment = 
  //     { source: this.brmService.currentUser.AccountID, 
  //       destination: this.fgSendCoins.get('recipient').value, 
  //       amountDrops: CSCUtil.cscToDrops(this.fgSendCoins.get('amount').value),
  //       feeDrops: CSCUtil.cscToDrops(this.fgSendCoins.get('fees').value),
  //       description: this.fgSendCoins.get('description').value
  //     };
  //   if(this.fgSendCoins.get('destinationTag').value.length > 0){
  //     preparePayment.destinationTag = this.fgSendCoins.get('destinationTag').value;
  //   }
  //   this.app.getRootNav().setRoot(HomePage, {id:1});
  //   this.casinocoinService.validateAndSend(recipient, password, preparePayment);
  // }

}
