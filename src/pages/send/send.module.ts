import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendPage } from './send';

@NgModule({
  declarations: [],
  imports: [
    IonicPageModule.forChild(SendPage),
  ],
})
export class SendPageModule {}
