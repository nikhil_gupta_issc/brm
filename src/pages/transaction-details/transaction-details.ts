import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { LogService } from '../../providers/log.service';
import { Clipboard } from "@ionic-native/clipboard";

@IonicPage()
@Component({
  selector: 'page-transaction-details',
  templateUrl: 'transaction-details.html',
})
export class TransactionDetailsPage {

  source: string;
  destination: string;
  amount: string;
  hash: string;
  legderIndex: number;

  constructor(public navCtrl: NavController,
    public logger: LogService,
    public navParams: NavParams, public clipborad: Clipboard, public toast: ToastController) {

    this.source = this.navParams.get("source");
    this.amount = this.navParams.get("amount");
    this.destination = this.navParams.get("destination");
    this.hash = this.navParams.get("hash");
    this.legderIndex = this.navParams.get("legderIndex");

    this.logger.debug("### Source: " + this.source +
      " Destination: " + this.destination +
      " Amount: " + this.amount +
      " Hash: " + this.hash +
      " Ledger Index: " + this.legderIndex);
  }
  copySourceID() {
    this.clipborad.copy(this.source);
    let toaster = this.toast.create({
      message: 'Copied!',
      duration: 3000,
      position: 'top'
    });
    toaster.present();
  }

  copyDestinationID() {
    this.clipborad.copy(this.destination);
    let toaster1 = this.toast.create({
      message: 'Copied!',
      duration: 3000,
      position: 'top'
    });
    toaster1.present();
  }

  copyHash() {
    this.clipborad.copy(this.hash);
    let toaster3 = this.toast.create({
      message: 'Copied!',
      duration: 3000,
      position: 'top'
    });
    toaster3.present();
  }

  copyLedger() {
    this.clipborad.copy(this.legderIndex.toString());
    let toaster4 = this.toast.create({
      message: 'Copied!',
      duration: 3000,
      position: 'top'
    });
    toaster4.present();
  }
}
