import { Component, Inject, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Nav } from 'ionic-angular';
import { LocalStorageService } from 'ngx-store';
import { AppConstants } from '../../domain/app-constants';
import { BASE_PATH, VerificationStatus } from '../../domain/brm-api';
import { LogService } from '../../providers/log.service';
import { JumioService } from '../../providers/jumio.service';
import { BRMService } from '../../providers/brm.service';
import { DatePipe } from '@angular/common';
import { DatePicker } from '@ionic-native/date-picker';
import { LoginPage } from '../login/login';

declare var Jumio;

@IonicPage()
@Component({
  selector: 'page-kyc',
  templateUrl: 'kyc.html',
})
export class KycPage {
  @ViewChild('birthDate') birthDate;

  showStatus: boolean = false;
  showUpload: boolean = false;
  showPending: boolean = false;
  showDetails: boolean = false;
  showInputs: boolean = false;
  firstNameInputShow: boolean = false;
  lastNameInputShow: boolean = false;
  fullNameInputShow: boolean = false;
  dobInputShow: boolean = false;
  userJumioData: any;
  message: string = "";
  type: string = "";
  firstName: string = "";
  lastName: string = "";
  dob: string = "";
  number: string = "";
  status: string = "";
  isCordova: boolean = false;
  dobDisplay: string = "";

  // jumio api token and secret (MOVE out of source code !!!)
  jumioToken: string = "654da134-d533-4d86-aabe-749568459529";
  jumioSecret: string = "OA9NKY8hW9R0HeX4yaZhrBSh3WINX5TY";
  jumioCallbackUrl = "";

  constructor(public navCtrl: NavController,
              public nav: Nav,
              public navParams: NavParams,
              public localStorageService: LocalStorageService,
              public logger: LogService,
              public brmService: BRMService,
              public jumioService: JumioService,
              public datepipe: DatePipe,
              public platform: Platform,
              private datePicker: DatePicker,
              @Inject(BASE_PATH) basePath: string) {
    this.jumioCallbackUrl = basePath + '/jumio/callback';
    if (this.brmService.currentUser.KYCStatus.Status !== VerificationStatus.APPROVEDVERIFIED) {
      if (this.brmService.currentUser.KYCStatus.ScanReference) {
        if (this.brmService.currentUser.KYCStatus.Status === VerificationStatus.NOTVERIFIED) {
          this.logger.debug("### KycPage - KYC Status: " + this.brmService.currentUser.KYCStatus.Status + " call Jumio Api and save User ###");
          this.getJumioUserResultAndSave(this.brmService.currentUser.KYCStatus.ScanReference);
        } else {
          this.logger.debug("### KycPage - KYC Status: " + this.brmService.currentUser.KYCStatus.Status + " show status to User & let user upload doc ###");
          this.showStatus = true;
          this.showUpload = true;
        }
      } else {
        this.logger.debug("### KycPage - No ScanReference value show status & let user upload doc ###");
        this.showStatus = true;
        this.showUpload = true;
      }
    } else {
      this.logger.debug("### KycPage - Jumio Status is " + this.brmService.currentUser.KYCStatus.Status + " Validate with user details ###");
      this.userJumioData = this.localStorageService.get(AppConstants.JUMIO_RESULT);
      if (this.userJumioData) {
        if (this.userJumioData.scanReference === this.brmService.currentUser.KYCStatus.ScanReference) {
          if (this.jumioService.isUserDetailsValid()) {
            this.logger.debug("### KycPage - User details validated with Jumio Result. Show Details ###");
            this.showUserKycDetails();
          } else {
            this.logger.debug("### KycPage - User details not validating with Jumio Result ###");
            this.checkDifferenceAndShowInputs();
          }
        } else {
          this.logger.debug("### KycPage - User updated scanreference. Get latest details ###");
          this.getJumioUserResultAndSave(this.brmService.currentUser.KYCStatus.ScanReference);
        }
      } else {
        this.logger.debug("### KycPage - Jumio Data not present in Localstorage. Call api and update ###");
        this.getJumioUserResultAndSave(this.brmService.currentUser.KYCStatus.ScanReference);
      }
    }
    if(this.platform.is('cordova')) {
      // for Date of birth date picker
      this.isCordova = true;
    }
  }

  getJumioUserResultAndSave(scanReference: string) {
    // this.jumioService.kycVerify(this.jumioToken, this.jumioSecret, this.brmService.currentUser.KYCStatus.ScanReference).subscribe(jumioData => {
    this.brmService.getScanResult(scanReference).subscribe(jumioData => {
      this.logger.debug("### Jumio Response: " + JSON.stringify(jumioData));
      this.localStorageService.set(AppConstants.JUMIO_RESULT, jumioData);
      if (jumioData.document.status === VerificationStatus.APPROVEDVERIFIED) {
        this.userJumioData = jumioData;
        this.brmService.currentUser.KYCStatus.Status = jumioData.document.status;
        this.brmService.currentUser.KYCStatus.ScanSubmittedDate = jumioData.transaction.date;
        // this.brmService.currentUser.KYCStatus.VerificationReceivedDate
        this.brmService.currentUser.KYCStatus.JumioFirstName = jumioData.document.firstName;
        this.brmService.currentUser.KYCStatus.JumioLastName = jumioData.document.lastName;
        this.brmService.currentUser.KYCStatus.JumioDOB = jumioData.document.dob;
        this.brmService.saveUser(this.brmService.currentUser).subscribe(result => {
          this.logger.debug("### Save User with KYC Update " + JSON.stringify(result));
        });
        if (this.jumioService.isUserDetailsValid()) {
          this.logger.debug("### KycPage - User details validated with Jumio Result ###");
          this.showUserKycDetails();
        } else {
          this.logger.debug("### KycPage - User details not validating with Jumio Result. Show Inputs  ###");
          this.checkDifferenceAndShowInputs();
        }
      } else {
        this.logger.debug("### KycPage - KYC Status: " + this.brmService.currentUser.KYCStatus.Status + " show status to User & let user upload doc ###");
        this.brmService.currentUser.KYCStatus.Status = jumioData.document.status;
        this.showStatus = true;
        this.showUpload = true;
      }
    }, error => {
      this.logger.debug("### Jumio Error: " + JSON.stringify(error));
      this.showPending = true;
      // this.showStatus = true;
      // if (this.brmService.currentUser.KYCStatus.Status !== VerificationStatus.APPROVEDVERIFIED) {
      //   this.showUpload = true;
      // }
    });
  }

  selectCordovaDateOfBirth() {
    this.datePicker.show({
      mode: 'date',
      date: new Date(this.brmService.currentUser.DateOfBirth),
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        this.logger.debug(" datePicker Got date: " + date);
        this.dobDisplay = this.datepipe.transform(date, 'dd-MMM-yyyy');
        this.brmService.currentUser.DateOfBirth = this.datepipe.transform(date, 'yyyy-MM-dd');
        this.birthDate.setBlur();
      },
      err => {
        this.birthDate.setBlur();
        this.logger.debug("Error occurred while getting date: " + err)
      }
    );
  }

  showUserKycDetails() {
    if (this.userJumioData.document.status === VerificationStatus.APPROVEDVERIFIED) {
      this.displayDetails();
    } else {
      // this.jumioService.kycVerify(this.jumioToken, this.jumioSecret, this.brmService.currentUser.KYCStatus.ScanReference).subscribe(jumioData => {
      this.brmService.getScanResult(this.brmService.currentUser.KYCStatus.ScanReference).subscribe(jumioData => {
        this.logger.debug("### Jumio Response: " + JSON.stringify(jumioData));
        this.localStorageService.set(AppConstants.JUMIO_RESULT, jumioData);
        this.userJumioData = jumioData;
        this.displayDetails();
      });
    }
  }
  
  displayDetails() {    
    this.showDetails = true;
    this.showStatus = true;
    this.type = this.userJumioData.document.type;
    this.firstName = this.userJumioData.document.firstName == "N/A" ? "" : this.userJumioData.document.firstName;
    this.lastName = this.userJumioData.document.lastName == "N/A" ? "" : this.userJumioData.document.lastName;
    this.dob = this.userJumioData.document.dob;
    this.number = this.userJumioData.document.number;
    this.status = this.userJumioData.document.status;
  }

  checkDifferenceAndShowInputs() {
    this.showInputs = true;
    this.message = "Your";
    if(this.isCordova){
      if(this.dobDisplay){
        this.brmService.currentUser.DateOfBirth = this.dobDisplay;
      }
    }
    // let firstMiddleName = this.brmService.currentUser.Firstname.toLowerCase() + " " + this.brmService.currentUser.Middlename.toLowerCase();
    // if (this.brmService.currentUser.KYCStatus.JumioFirstName.toLowerCase() !== firstMiddleName.trim()) {
    //   this.firstNameInputShow = true;
    //   this.message = this.message + " First Name";
    //   this.logger.debug("### KycPage - First Name doesn't Match ###");
    // } else {
    //   this.firstNameInputShow = false;
    // }
    // if (this.brmService.currentUser.KYCStatus.JumioLastName.toLowerCase() !== this.brmService.currentUser.Lastname.toLowerCase().trim()) {
    //   this.lastNameInputShow = true;
    //   this.message = this.message + " Last Name";
    //   this.logger.debug("### KycPage - Last Name doesn't Match ###");
    // } else {
    //   this.lastNameInputShow = false;
    // }

    // let jumioFullName = this.brmService.currentUser.KYCStatus.JumioFirstName + " " + this.brmService.currentUser.KYCStatus.JumioLastName;
    let jumioFullName = "";
    if (this.brmService.currentUser.KYCStatus.JumioFirstName != "N/A")
      jumioFullName = this.brmService.currentUser.KYCStatus.JumioFirstName + " ";
    if (this.brmService.currentUser.KYCStatus.JumioLastName != "N/A")
      jumioFullName = jumioFullName + this.brmService.currentUser.KYCStatus.JumioLastName;
    
    if (jumioFullName.toLowerCase().trim() !== this.brmService.currentUser.Fullname.toLowerCase().trim()) {
      this.fullNameInputShow = true;
      this.message = this.message + " Full Name";
      this.logger.debug("### KycPage - Full Name doesn't Match ###");
    } else {
      this.fullNameInputShow = false;
    }
    if (this.brmService.currentUser.KYCStatus.JumioDOB !== this.datepipe.transform(this.brmService.currentUser.DateOfBirth, 'yyyy-MM-dd')) {
      this.dobInputShow = true;
      this.dobDisplay = this.datepipe.transform(this.brmService.currentUser.DateOfBirth, 'dd-MMM-yyyy');
      // if (this.firstNameInputShow && this.lastNameInputShow) {
      //   this.message = this.message.substr(0, 15) + ", " + this.message.substr(15) + " and";
      // } else {
      //   if (this.firstNameInputShow) {
      //     this.message = this.message.substr(0, 15) + " and";
      //   }
      //   if (this.lastNameInputShow) {
      //     this.message = this.message.substr(0, 15) + " and";
      //   }
      // }

      if (this.fullNameInputShow) {
        this.message = this.message.substr(0, 15) + " and";
      }
      this.message = this.message + " Date of Birth";
      this.logger.debug("### KycPage - Date of Birth doesn't Match ###");
    } else {
      // if (this.firstNameInputShow && this.lastNameInputShow) {
      //   this.message = this.message.substr(0, 15) + " and " + this.message.substr(15);
      // }
      this.dobInputShow = false;
    }
    this.message = this.message + " doesn't match with the document provided.";
  }

  validateAndUpdateUser() {
    if (this.jumioService.isUserDetailsValid()) {
      this.showInputs = false;
      this.brmService.saveUser(this.brmService.currentUser).subscribe(result => {
        this.logger.debug("### Save User with KYC Update " + JSON.stringify(result));
      }, error => {
        this.nav.setRoot(LoginPage);
        this.brmService.logOut().present();
      });
      this.showUserKycDetails();
    } else {
      this.checkDifferenceAndShowInputs();
    }
  }

  startNetverify() {
    // Netverify => requireVerification= true
    this.logger.debug("### Init Netverify ###");
    Jumio.initNetverify(this.jumioToken, this.jumioSecret, "EU", {
      requireVerification: true,
      requireFaceMatch: true,
      customerId: this.brmService.currentUser.AccountID,
      preselectedCountry: this.brmService.currentUser.CountryISOCode,
      cameraPosition: "BACK",
      documentTypes: ["DRIVER_LICENSE", "PASSPORT", "IDENTITY_CARD"],
      callbackUrl: this.jumioCallbackUrl
    });
    this.logger.debug("### Start Netverify");
    this.brmService.currentUser.KYCStatus.Status = VerificationStatus.NOTVERIFIED;
    this.brmService.currentUser.KYCStatus.ScanSubmittedDate = new Date();
    Jumio.startNetverify((documentData, error) => {
      if (error) {
        this.logger.debug("### Netverify Error: " + JSON.stringify(error));
        this.showUpload = true;
      } else {
        this.logger.debug("### Netverify DocumentData: " + JSON.stringify(documentData));
        this.showUpload = false;
        this.showStatus = false;
        this.showPending = true;
        this.localStorageService.remove(AppConstants.JUMIO_RESULT);
        this.brmService.currentUser.KYCStatus.ScanReference = documentData.scanReference;
        this.brmService.saveUser(this.brmService.currentUser).subscribe(result => {
          this.logger.debug("### Save User Result: " + JSON.stringify(result));
        }, error => {
          this.nav.setRoot(LoginPage);
          this.brmService.logOut().present();
        });
      }
    });
  }
}
