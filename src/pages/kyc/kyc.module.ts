import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KycPage } from './kyc';

@NgModule({
  declarations: [ ],
  imports: [
    IonicPageModule.forChild(KycPage),
  ],
})
export class KycPageModule {}
