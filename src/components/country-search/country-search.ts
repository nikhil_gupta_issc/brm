import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { CountryPickerService } from 'ngx-country-picker';
import { ICountry } from 'ngx-country-picker/country.interface';
import { ViewController, Searchbar, Platform, NavParams } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { LogService } from '../../providers/log.service';
import { BRMService } from '../../providers/brm.service';

@Component({
  selector: 'country-search',
  templateUrl: 'country-search.html'
})
export class CountrySearchComponent implements OnInit, AfterViewInit {

  @ViewChild('searchbar') searchbar:Searchbar;

  searchQuery: string = '';
  countries: ICountry[] = [];
  modalType: string = "country";

  constructor( public platform: Platform,
               public navParams: NavParams,
               public keyboard: Keyboard,
               private logger: LogService,
               protected countryPicker: CountryPickerService,
               public viewCtrl: ViewController,
               private brmService: BRMService ) {
    this.logger.debug('### CountrySearchComponent ###');
    
  }

  ngOnInit(){
    this.modalType = this.navParams.get('type');
    this.logger.debug('### CountrySearchComponent INIT: Type - ' + this.modalType);
    if (this.brmService.countries) {
      this.countries = this.brmService.countries;
      if (this.modalType === 'nationality') {
        this.sortNationalityList();
      } else {
        this.sortCountryList();
      }
    } else {
      this.countryPicker.getCountries().subscribe((countries: ICountry[]) => {
        this.countries = countries;
        if (this.modalType === 'nationality') {
          this.sortNationalityList();
        } else {
          this.sortCountryList();
        }
      });
    }
  }

  ionViewDidLoad() {
      // setTimeout(() => {
      //   this.logger.debug("### CountrySearchComponent - setFocus()");
      //   this.searchbar.setFocus();
      // }, 500);
      // this.keyboard.onKeyboardShow().subscribe((data)=>{
      //   this.searchbar.setFocus();
      // })
  }

  ngAfterViewInit(){
    // this.searchbar.setFocus();
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    if (this.brmService.countries) {
      this.countries = this.brmService.countries;
      this.sortCountryList();
      // set val to the value of the searchbar
      let val = ev.target.value;
      if (this.modalType === 'nationality') {
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.countries = this.countries.filter((item) => {
            return (item.demonym.toLowerCase().startsWith(val.toLowerCase()));
          });
        }
      } else {
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.countries = this.countries.filter((item) => {
            return (item.name.common.toLowerCase().startsWith(val.toLowerCase()));
          });
        }
      }
    } else {
      this.countryPicker.getCountries().subscribe((countries: ICountry[]) => {
        this.countries = countries;
        this.sortCountryList();
        // set val to the value of the searchbar
        let val = ev.target.value;
        if (this.modalType === 'nationality') {
          // if the value is an empty string don't filter the items
          if (val && val.trim() != '') {
            this.countries = this.countries.filter((item) => {
              return (item.demonym.toLowerCase().startsWith(val.toLowerCase()));
            });
          }
        } else {
          // if the value is an empty string don't filter the items
          if (val && val.trim() != '') {
            this.countries = this.countries.filter((item) => {
              return (item.name.common.toLowerCase().startsWith(val.toLowerCase()));
            });
          }
        }
      });
    }
  }

  countrySelected(item){
    this.viewCtrl.dismiss(item);
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  sortCountryList(){
    this.countries = this.countries.sort((data1: ICountry, data2: ICountry) => {
      if(data1.name.common == null){
          return 1;
      } else if(data2.name.common == null){
          return -1;
      } else if(data1.name.common < data2.name.common){
          return -1;
      } else if(data1.name.common > data2.name.common){
          return 1;
      } else {
          return 0;
      }
    });
  }

  sortNationalityList() {
    this.countries = this.countries.sort((data1: ICountry, data2: ICountry) => {
      if(data1.demonym == null){
          return 1;
      } else if(data2.demonym == null){
          return -1;
      } else if(data1.demonym < data2.demonym){
          return -1;
      } else if(data1.demonym > data2.demonym){
          return 1;
      } else {
          return 0;
      }
    });
  }
}
