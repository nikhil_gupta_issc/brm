import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { LogService } from '../../providers/log.service';

@Component({
  selector: 'terms-conditions',
  templateUrl: 'terms-conditions.html'
})
export class TermsConditionsComponent {

  agreeCheckbox: boolean = false;
  continue: boolean = false;

  constructor(public viewCtrl : ViewController,
              private logger: LogService) {
    this.logger.debug('### TermsConditionsComponent ###');
  }

  changeAgree() {
    if(this.agreeCheckbox){
      this.continue = true;
    } else {
      this.continue = false;
    }
  }

  continueToSignUp() {
    this.viewCtrl.dismiss(true);
  }

  closeModal() {
    this.viewCtrl.dismiss(false);
  }

}
