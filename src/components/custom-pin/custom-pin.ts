import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'custom-pin',
  templateUrl: 'custom-pin.html'
})
export class CustomPinComponent {
  pageTitle: String = "Enter Pin";

  pin:string= "";

  constructor(public viewCtrl: ViewController,
              public navParams: NavParams) {
    this.pageTitle = this.navParams.get("pageTitle");
  }

  emitEvent() {
    if (this.pin.length === 6) {
      this.viewCtrl.dismiss(this.pin);
    }
  }

  handleInput(pin: string) {
    if (pin === "clear") {
      this.pin = "";
      this.viewCtrl.dismiss();
    }
    this.pin += pin;
    if (this.pin.length === 6) {
      this.viewCtrl.dismiss(this.pin);
    }
  }

  clear() {
    this.pin = this.pin.substring(0, this.pin.length - 1);
  }

  cancel() {
    this.viewCtrl.dismiss();    
  }

}
