import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { LocalStorageService } from 'ngx-store';
import { AppConstants } from '../../domain/app-constants';
import { User } from '../../domain/brm-api';

@Component({
  selector: 'responsible-gaming-popover',
  templateUrl: 'responsible-gaming-popover.html'
})
export class ResponsibleGamingPopoverComponent {

  operators: Array<any>;
  showCheckbox: boolean;
  user: User;
  allCasinos: boolean = false;

  constructor(public localStorageService: LocalStorageService,
              public viewCtrl: ViewController,
              public navParams: NavParams) {
    this.operators = this.localStorageService.get(AppConstants.KEY_BRM_OPERATORS);
    this.user = this.localStorageService.get(AppConstants.KEY_BRM_USER);
    this.showCheckbox = this.navParams.get('showCheckbox');
    this.operators.forEach(operator => {
      operator.Selected = false;
    });
  }

  onSingleClick(OperatorID, DisplayName) {
    this.viewCtrl.dismiss({OperatorID, DisplayName});
  }

  onAllCasinoClick() {
    this.viewCtrl.dismiss([]);
  }

  onMultiSelect() {
    let selectedOperator = [];
    if(this.allCasinos) {
      this.viewCtrl.dismiss(selectedOperator);
    } else {
      this.operators.forEach(operator => {
        if(operator.Selected) {
          selectedOperator.push({OperatorID: operator.OperatorID, DisplayName: operator.DisplayName});
        }
      });
      this.viewCtrl.dismiss(selectedOperator);
    }
  }

  haveLimits(OperatorID): boolean {
    if(this.user.UserGamingLimits.OperatorLimits.find(x => (x.OperatorID === OperatorID && (x.Enabled24H || x.Enabled7D || x.Enabled30D)))) {
      return true;
    } else {
      return false;
    }
  }

}
