import { Component } from '@angular/core';
import { ViewController, ActionSheetController, ToastController } from 'ionic-angular';
import { BRMService } from '../../providers/brm.service';
import { CSCUtil } from '../../domain/csc-util';
import Big from 'big.js';
import { LogService } from '../../providers/log.service';
import { WalletService } from '../../providers/wallet.service';
import { CasinocoinService } from '../../providers/casinocoin.service';
import { LocalStorageService } from "ngx-store";
import { AppConstants } from '../../domain/app-constants';
import { Clipboard } from '@ionic-native/clipboard';

@Component({
  selector: 'account-details',
  templateUrl: 'account-details.html'
})
export class AccountDetailsComponent {

  cscReceiveURI: string = "";
  walletBalance: string;
  accountID: string = "";

  constructor(public viewCtrl : ViewController,
              public brmService: BRMService,
              private walletService: WalletService,
              private logger: LogService,
              public casinocoinService: CasinocoinService,
              private localStorageService: LocalStorageService,
              public actionSheetCtrl: ActionSheetController,
              public clipboard: Clipboard,
              public toastCtrl: ToastController) {
    this.cscReceiveURI = CSCUtil.generateCSCQRCodeURI({ address: this.brmService.currentUser.AccountID });
    // let balance = new Big(this.walletService.getWalletBalance() ? this.walletService.getWalletBalance() : "0");   
    let balance = new Big(this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) ? this.walletService.getAccountBalance(this.brmService.currentUser.AccountID) : "0"); 
    this.logger.debug("### Wallet Balance: " + balance);
    this.casinocoinService.serverStateSubject.subscribe( server =>{
      if(server.complete_ledgers.length > 0){
        let reserve = new Big(server.validated_ledger.reserve_base);
        this.logger.debug("### Account Reserve: " + reserve);
        this.walletBalance = CSCUtil.dropsToCsc(balance.minus(reserve));
        this.logger.debug("### ACCOUNT DETAILS Wallet Balance: " + this.walletBalance);
      }
    });
    this.accountID = this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID);
  }

  public closeModal(){
    this.viewCtrl.dismiss();
  }

  showActionSheet() {
    this.logger.debug('### Receive - Press QR code showing Action Sheet ###');
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Copy Account Address',
          handler: () => {
            console.log('Copy clicked');
            this.clipboard.copy(this.accountID);
            let toast = this.toastCtrl.create({
              message: 'Copied!',
              duration: 3000,
              position: 'top'
            });         
            toast.present();
          }
        },
        { text: 'Cancel' }
      ]
    }); 
    actionSheet.present();
  }

}
