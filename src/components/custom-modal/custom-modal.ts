import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { BRMService } from '../../providers/brm.service';
import { Observable } from 'rxjs';
import { LogService } from '../../providers/log.service';

@Component({
  selector: 'custom-modal',
  templateUrl: 'custom-modal.html'
})
export class CustomModalComponent {
  pinConfirmation: boolean = false;
  pinConfirmationSuccess: boolean = false;
  accountBackup: boolean = false;
  accountBackupSuccess: boolean = false;
  accountCreation: boolean = false;
  accountCreationSuccess: boolean = false;
  creationSuccess: boolean = false;
  type;

  constructor(public viewCtrl: ViewController,
    public navParams: NavParams,
    public brmService: BRMService,
    public logger: LogService) {

    this.type = this.navParams.get('Type');
    let success = this.navParams.get('Success');    
    this.logger.debug('### CustomModalComponent Opened - Type: ' + this.type + ' success: ' + success);

    if (this.type === 'PinConformation') {
      this.pinConfirmation = true;
      if (success) {
        this.pinConfirmationSuccess = true;
      } else {
        this.pinConfirmationSuccess = false;
      }
      setTimeout(() => {
        this.viewCtrl.dismiss();
      }, 2000);
    } else if (this.type === 'AccountCreation') {
      this.accountCreation = true;
      if (success) {
        this.accountCreationSuccess = true;
      } else {
        this.accountCreationSuccess = false;
      }
    } else if (this.type === 'AccountBackUp') {
      this.accountBackup = true;
      if (success) {
        this.accountBackupSuccess = true;
      } else {
        this.accountBackupSuccess = false;
      }
    } else if (this.type === 'CreationSuccess') {      
      this.creationSuccess = true;
    }

    if (this.type === 'AccountCreation') {
      let interval = Observable.interval(1000)
        .subscribe((val) => {
          if (this.brmService.accountCreated && this.brmService.cscConnected && this.brmService.accountActivated && this.brmService.kycVerified) {
            this.accountCreation = false;
            this.creationSuccess = true;
            interval.unsubscribe();
            // setTimeout(() => {
            //   this.viewCtrl.dismiss();
            // }, 2000);
          } else {
            this.accountCreation = true;
          }
        });
    }
  }

  nextSlide() {
    this.viewCtrl.dismiss();
  }

}
