import { NgModule } from '@angular/core';
import {IonicModule} from 'ionic-angular';

import { CountrySearchComponent } from './country-search/country-search';
import { LoginComponent } from './login/login';
import { ContactPopoverComponent } from './contact-popover/contact-popover';
import { AccountDetailsComponent } from './account-details/account-details';
import { QRCodeModule } from 'angular2-qrcode';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions';
import { CustomModalComponent } from './custom-modal/custom-modal';
import { LockScreenComponent } from "./lock-screen-component";
import { ResponsibleGamingPopoverComponent } from './responsible-gaming-popover/responsible-gaming-popover';
import { CustomPinComponent } from './custom-pin/custom-pin';

@NgModule({
	declarations: [CountrySearchComponent, LoginComponent,
    ContactPopoverComponent,
    AccountDetailsComponent,
    TermsConditionsComponent,
    LockScreenComponent,
    CustomModalComponent,
    ResponsibleGamingPopoverComponent,
    CustomPinComponent],
	imports: [IonicModule, QRCodeModule],
	exports: [CountrySearchComponent, LoginComponent,
    ContactPopoverComponent,
    AccountDetailsComponent,
    TermsConditionsComponent,
    CustomModalComponent,
    ResponsibleGamingPopoverComponent,
    CustomPinComponent],
    entryComponents: [ CountrySearchComponent, ContactPopoverComponent,
    AccountDetailsComponent, TermsConditionsComponent, CustomModalComponent,
    LockScreenComponent, ResponsibleGamingPopoverComponent, CustomPinComponent ]
})
export class ComponentsModule {}
