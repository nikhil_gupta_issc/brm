import { Component } from '@angular/core';
import { NavController, ViewController, ToastController, App } from 'ionic-angular';
import { WalletService } from '../../providers/wallet.service';
import { Clipboard } from '@ionic-native/clipboard';
import { HomePage } from '../../pages/home/home';

@Component({
  selector: 'contact-popover',
  templateUrl: 'contact-popover.html'
})
export class ContactPopoverComponent {

  contactAccountID: string = "";

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              private walletService: WalletService,
              private toastCtrl: ToastController,
              private clipboard: Clipboard,
              private app: App) {
    this.contactAccountID = this.walletService.contactAccountID;
  }

  redirectToSendPage() {    
    // this.navCtrl.push(SendPage);
    let nav = this.app.getRootNav(); 
    nav.setRoot(HomePage, {id:1});
    // this.navCtrl.setRoot(HomePage,{id:1});
    // this.navCtrl.parent.select(1);
    this.viewCtrl.dismiss();
  }

  copyAccountID(){
    this.clipboard.copy(this.contactAccountID);
    this.walletService.contactAccountID = "";
    let toast = this.toastCtrl.create({
      message: 'Copied!',
      duration: 3000,
      position: 'top'
    });         
    toast.present();
    this.viewCtrl.dismiss();
  }

  deleteContact() {
    this.walletService.removeAddress(this.contactAccountID);
    this.walletService.contactAccountID = "";
    let toast = this.toastCtrl.create({
      message: 'Contact Deleted Successfully.',
      duration: 3000,
      position: 'top'
    });         
    toast.present();
    this.viewCtrl.dismiss();
  } 

}
