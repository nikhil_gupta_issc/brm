import { Injectable, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { LocalStorageService, SessionStorageService } from "ngx-store";
import { LogService } from './log.service';
import { DefaultService as BRMRemoteService, Configuration, User, Operator, Registration, LoginRequest } from '../domain/brm-api';
import { AppConstants } from '../domain/app-constants';
import { WalletService } from './wallet.service';
import { CasinocoinService } from './casinocoin.service';
import { LokiAccount, LokiKey } from '../domain/lokijs';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { PrepareTxPayment } from '../domain/csc-types';
import Big from 'big.js';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import CasinocoinAPI from 'casinocoin-libjs';
import { WebsocketService } from './websocket.service';
import { CountryPickerService, ICountry } from 'ngx-country-picker';
import { CSCUtil } from '../domain/csc-util';
import { CSCCrypto, CSCKeyPair } from '../domain/csc-crypto';
import { AlertController, Platform, ToastController } from 'ionic-angular';
import { Clipboard } from '@ionic-native/clipboard';

export interface ErrorMessage {
    device: string;
    model: string;
    timestamp: string;
    error: any;
}

// export function apiConfigFactory (): Configuration {
//     const params: ConfigurationParameters = {
//         apiKeys: {}
//     }
//     return new Configuration(params);
// }

@Injectable()
export class BRMService implements OnInit {

    walletOpen: boolean = false;
    currentOperators: Operator[] = [];
    currentUser: User;
    registrationInitiated: string;
    accountReserveDrops: Big;
    feesDrops: Big;
    updateUserSubject = new BehaviorSubject<boolean>(false);
    // loginSubject = new BehaviorSubject<boolean>(false);
    loginSubject = new BehaviorSubject<string>(AppConstants.KEY_INIT);
    userAccountCheck = new BehaviorSubject<boolean>(false);
    sessionID:string;
    countries: ICountry[] = [];
    brmDefaultMode: boolean = true;

    // final status for SignUp
    accountCreated: boolean = false;
    accountCreatedFailed:boolean = false;
    cscConnected: boolean = false;
    cscConnectedFailed: boolean = false;
    accountActivated: boolean = false;
    accountActivatedFailed: boolean = false;
    kycVerified: boolean = false;
    kycVerifiedFailed: boolean = false;

    constructor(private logger: LogService, 
                private localStorageService: LocalStorageService,
                private sessionStorageService: SessionStorageService,
                private brmRemote: BRMRemoteService,
                private brmConfiguration: Configuration,
                private walletService: WalletService,
                private casinocoinService: CasinocoinService,
                private iab: InAppBrowser,
                private webSocketService: WebsocketService,
                private countryPicker: CountryPickerService,
                private alertCtrl: AlertController, 
                private clipboard: Clipboard,
                private toastCtrl: ToastController,
                private platform: Platform ) {
        logger.debug("### INIT  BRMService ###");
        // set api endpoint configuration
        // this.brmConfiguration = apiConfigFactory();
        this.brmRemote.configuration = this.brmConfiguration;
        // subscribe to open wallet subject
        this.walletService.openWalletSubject.subscribe( result => {
            if(result == AppConstants.KEY_LOADED){
              this.logger.debug("### BRMService Wallet Open ###");
              this.walletOpen = true;
            }
          });
        // get account reserve and fee
        this.casinocoinService.serverStateSubject.subscribe(state => {
            if(state.complete_ledgers.length > 0){
                this.accountReserveDrops = new Big(state.validated_ledger.reserve_base);
                this.feesDrops = new Big(state.validated_ledger.base_fee);
            }
        });
        // get current user profile when logged in
        this.loginSubject.subscribe(loginResult => {
            if(loginResult == AppConstants.KEY_FINISHED){
                this.logger.debug("### Logged In -> updateCurrentUser()");
                // get the current operators
                this.updateCurrentOperators();
                // update current user
                this.updateCurrentUser();
            }
        });
        // subscribe user update through websocket
        this.webSocketService.userUpdate.subscribe(user => {
            this.logger.debug("### BRMService Updating currentUser ###");
            if(user){
                this.currentUser = user;
            }
        });
        // load countries
        if(!this.localStorageService.get(AppConstants.KEY_SETUP_COMPLETED)) {
            this.countryPicker.getCountries().subscribe((countries: ICountry[]) => {
                this.countries = countries;
            });
        }
    }

    ngOnInit() {
        this.logger.debug("### BRMService onInit ###");
    }

    logOut() {
        this.walletService.openWalletSubject.subscribe(result => {
            if (result == AppConstants.KEY_LOADED) {
                this.localStorageService.set(AppConstants.LOGGED_IN, false);
                this.walletService.closeWallet();
                this.casinocoinService.disconnect();
                this.walletService.openWalletSubject = new BehaviorSubject<string>(AppConstants.KEY_INIT);
                this.loginSubject = new BehaviorSubject<string>(AppConstants.KEY_INIT);
            }
        });
        this.logger.debug("### JWT token session expired. Logged out ###");
        return this.alertCtrl.create({ title: 'Session has expired', subTitle: 'Please login again to continue.', buttons: [{ text: 'Ok' }] });
    }

    loginUser(keyPair: LokiKey): Observable<any> {
        // get a new session
        this.logger.debug("### BRMService - loginUser: " + JSON.stringify(keyPair));
        if(keyPair.encrypted){
            this.loginSubject.next(AppConstants.KEY_ERRORED);
            return this.loginSubject.asObservable();
        } else {
            this.brmRemote.sessionAccountIDUsertypeUserTypeStartGet(keyPair.accountID, "user").subscribe( (session) => {
                this.logger.debug("### BRMService loginUser - session: " + JSON.stringify(session));
                // get the account key object
                // let accountKey: LokiKey = this.walletService.getKey(accountID);
                // let accountSecret:string = this.walletService.getDecryptSecret(this.walletService.walletPIN, accountKey);
                // sign SessionID
                const cscAPI = new CasinocoinAPI.CasinocoinAPI();
                let signature = cscAPI.signMessage(session['SessionID'], keyPair.secret);
                this.logger.debug("### BRMService loginUser - signature: " + JSON.stringify(signature));
                // login to obtain JWT token
                let loginRequest: LoginRequest = {
                    PublicKey: signature.public_key,
                    SessionID: session['SessionID'],
                    Signature: signature.signature
                };
                this.logger.debug("### BRMService loginUser - loginRequest: " + JSON.stringify(loginRequest));
                this.brmRemote.userLoginPost(loginRequest).subscribe(loginResult => {
                    this.logger.debug("### BRMService loginUser - loginResult: " + JSON.stringify(loginResult));
                    if(loginResult['Success']){
                        // save JWT on brm service config
                        this.logger.debug("### BRM Configuration: " + JSON.stringify(this.brmConfiguration));
                        this.brmConfiguration.apiKeys['X-API-JWT'] = loginResult['Token'];
                        // save JWT in session storage
                        this.sessionStorageService.set(AppConstants.KEY_JWT, loginResult['Token']);
                        // return login success
                        this.loginSubject.next(AppConstants.KEY_FINISHED);
                    } else {
                        this.loginSubject.next(AppConstants.KEY_ERRORED);
                    }
                })
            }, error => {
                this.loginSubject.next(AppConstants.KEY_ERRORED);
                let alert = this.alertCtrl.create({
                    title: 'Internal Error',
                    subTitle: 'Server is not responding currently. Please try again later.',
                    buttons: ['Dismiss']
                });
                alert.present();
                if (this.walletService.isWalletOpen) {
                    this.walletService.encryptSecretKey(this.walletService.walletPIN);
                    this.walletService.closeWallet();
                } else {
                    this.walletService.openWalletSubject = new BehaviorSubject<string>(AppConstants.KEY_INIT);
                    this.loginSubject = new BehaviorSubject<string>(AppConstants.KEY_INIT);
                }
            });
            return this.loginSubject.asObservable();
        }
    }

    saveUser(user: User){
        user.LastUpdateDate = new Date();
        // save user for session
        this.currentUser = user;
        // first save local
        this.localStorageService.set(AppConstants.KEY_BRM_USER, user);
        // save remote
        return this.brmRemote.userAccountIDPost(user.AccountID, user);
    }

    updateUser(user: User){
        user.LastUpdateDate = new Date();
        // save user for session
        this.currentUser = user;
        // first save local
        this.localStorageService.set(AppConstants.KEY_BRM_USER, user);
        // save remote
        return this.brmRemote.userAccountIDPut(user.AccountID, user);
    }

    updateCurrentUser(): Observable<boolean> {
        // first get local
        this.currentUser = this.localStorageService.get(AppConstants.KEY_BRM_USER);
        let currentUserAccountID = this.localStorageService.get(AppConstants.KEY_DEFAULT_ACCOUNT_ID);
        if(this.currentUser == null){
            if(currentUserAccountID != null){
                // get remote and check if newer
                this.brmRemote.userAccountIDGet(currentUserAccountID).subscribe( result => {
                    this.currentUser = result;
                    this.currentUser.UserGamingLimits = result.UserGamingLimits[0];
                    this.currentUser.LinkedAccount = result.LinkedAccount[0];
                    this.currentUser.KYCStatus = result.KYCStatus[0];    
                    // save local
                    this.localStorageService.set(AppConstants.KEY_BRM_USER, this.currentUser);
                    this.updateUserSubject.next(true);
                });
            } else {
                this.updateUserSubject.next(true);
            }
        } else {
            // get remote and check if newer
            this.brmRemote.userAccountIDGet(this.currentUser.AccountID).subscribe( result => {
                this.logger.debug("### Get User: " + JSON.stringify(result));
                // if(result.LastUpdateDate > this.currentUser.LastUpdateDate){
                    // this.logger.debug("### User got updated remote so update local");
                    this.currentUser = result;
                    this.currentUser.UserGamingLimits = result.UserGamingLimits[0];
                    this.currentUser.LinkedAccount = result.LinkedAccount[0];
                    this.currentUser.KYCStatus = result.KYCStatus[0]; 
                    // save local
                    this.localStorageService.set(AppConstants.KEY_BRM_USER, this.currentUser);
                    this.updateUserSubject.next(true);
                // } else {
                //     // user not updated
                //     this.updateUserSubject.next(true);
                // }
            });
        }
        // define and return subject
        return this.updateUserSubject.asObservable();
    }

    getUser(): User{
        return this.currentUser;
    }

    isGamingLimitTimeCompleted(timestamp: number, duration: string): boolean {
        if (duration == "24H") {
            if ((CSCUtil.casinocoinToUnixTimestamp(timestamp) + (86400 * 1000)) < Date.now()) { // 24H = 86,400 seconds
                return true;
            }
        } else if (duration == "7D") {
            if ((CSCUtil.casinocoinToUnixTimestamp(timestamp) + (604800 * 1000)) < Date.now()) { // 7D = 604,800 seconds
                return true;
            }
        } else if (duration == "30D") {
            if ((CSCUtil.casinocoinToUnixTimestamp(timestamp) + (2592000 * 1000)) < Date.now()) { // 30D = 2,592,000 seconds
                return true;
            }
        }
        return false;
    }

    userWithUpdatedLimitsTime(): User {
        let changeMade = false;
        if (this.currentUser.UserGamingLimits) {
            if (this.currentUser.UserGamingLimits.All24HEnabled && this.isGamingLimitTimeCompleted(this.currentUser.UserGamingLimits.All24Timestamp, "24H")) {
                this.logger.debug("### BRMService - GamingLimit time is done for All 24H");
                this.currentUser.UserGamingLimits.All24H = 0;
                this.currentUser.UserGamingLimits.All24HEnabled = false;
                this.currentUser.UserGamingLimits.All24Timestamp = 0;
                changeMade = true;
            }
            if (this.currentUser.UserGamingLimits.All7DEnabled && this.isGamingLimitTimeCompleted(this.currentUser.UserGamingLimits.All7DTimestamp, "7D")) {
                this.logger.debug("### BRMService - GamingLimit time is done for All 7D");
                this.currentUser.UserGamingLimits.All7D = 0;
                this.currentUser.UserGamingLimits.All7DEnabled = false;
                this.currentUser.UserGamingLimits.All7DTimestamp = 0;
                changeMade = true;
            }
            if (this.currentUser.UserGamingLimits.All30DEnabled && this.isGamingLimitTimeCompleted(this.currentUser.UserGamingLimits.All30DTimestamp, "30D")) {
                this.logger.debug("### BRMService - GamingLimit time is done for All 30D");
                this.currentUser.UserGamingLimits.All30D = 0;
                this.currentUser.UserGamingLimits.All30DEnabled = false;
                this.currentUser.UserGamingLimits.All30DTimestamp = 0;
                changeMade = true;
            }
            if (this.currentUser.UserGamingLimits.OperatorLimits) {
                this.currentUser.UserGamingLimits.OperatorLimits.forEach(limit => {
                    if (limit.Enabled24H && this.isGamingLimitTimeCompleted(limit.TimestampSet24H, "24H")) {
                        this.logger.debug("### BRMService - GamingLimit time is done for 24H for OperatorID: " + limit.OperatorID + " OperatorName: " + limit.OperatorName);
                        limit.Value24H = 0;
                        limit.Enabled24H = false;
                        limit.TimestampSet24H = 0;
                        changeMade = true;
                    }
                    if (limit.Enabled7D && this.isGamingLimitTimeCompleted(limit.TimestampSet7D, "7D")) {
                        this.logger.debug("### BRMService - GamingLimit time is done for 7D for OperatorID: " + limit.OperatorID + " OperatorName: " + limit.OperatorName);
                        limit.Value7D = 0;
                        limit.Enabled7D = false;
                        limit.TimestampSet7D = 0;
                        changeMade = true;
                    }
                    if (limit.Enabled30D && this.isGamingLimitTimeCompleted(limit.TimestampSet30D, "30D")) {
                        this.logger.debug("### BRMService - GamingLimit time is done for 30D for OperatorID: " + limit.OperatorID + " OperatorName: " + limit.OperatorName);
                        limit.Value30D = 0;
                        limit.Enabled30D = false;
                        limit.TimestampSet30D = 0;
                        changeMade = true;
                    }
                });
            }
        }
        if (changeMade) {
            this.saveUser(this.currentUser).subscribe( result => {
                this.logger.debug("### User Updated after gaming limit time changed: " + JSON.stringify(result));
            });
        }
        return this.currentUser;
    }

    updateCurrentOperators(){
        this.brmRemote.getAllOperators().subscribe( response => {
            let operatorArray: Array<Operator> = response;
            this.currentOperators = [];
            operatorArray.forEach(operator => {
                this.currentOperators.push(operator);
            });
            // store local
            this.localStorageService.set(AppConstants.KEY_BRM_OPERATORS, this.currentOperators);
        });
        /*this.currentOperators = this.localStorageService.get(AppConstants.KEY_BRM_OPERATORS);
        if(this.currentOperators == null){
            // no stored operators -> get them online
            this.brmRemote.getAllOperators().subscribe( response => {
                let operatorArray: Array<Operator> = response;
                this.currentOperators = [];
                operatorArray.forEach(operator => {
                    this.currentOperators.push(operator);
                });
                // store local
                this.localStorageService.set(AppConstants.KEY_BRM_OPERATORS, this.currentOperators);
            });
        } else {
            // get remote and check if newer
            this.brmRemote.getAllOperators().subscribe( response => {
                response.forEach(operator => {
                    // find local
                    let operatorIndex = this.currentOperators.findIndex(x => x.AccountID === operator.AccountID);
                    if(operatorIndex == undefined || operatorIndex < 0){
                        // non existing so add
                        this.currentOperators.push(operator);
                    } else if(operator.LastUpdateDate > this.currentOperators[operatorIndex].LastUpdateDate){
                        // update local operator
                        this.currentOperators[operatorIndex] = operator;
                    }
                });
                // store local
                this.localStorageService.set(AppConstants.KEY_BRM_OPERATORS, this.currentOperators);
            });
        }*/
    }

    activateAccount(user: User){
        return this.brmRemote.userAccountIDActivateGet(user.AccountID);
    }

    getKycInfoByAccountID(accountID: string){
        return this.brmRemote.userAccountIDKycstatusGet(accountID);
    }

    saveRegistration(accountID: string, reg: Registration){
        return this.brmRemote.userAccountIDInitiateRegistrationPost(accountID, reg);
    }

    getAllOperatorsApi() {
        return this.brmRemote.getAllOperators();
    }

    getScanResult(scanReference){
        return this.brmRemote.jumioScanReferenceScanDataGet(scanReference);
    }

    getAccountBalance(accountID){
        return this.brmRemote.userAccountIDBalanceGet(accountID);
    }

    getUserByAccountID(accountID){
        return this.brmRemote.userAccountIDGet(accountID);
    }

    getAllOperators(): Array<Operator>{
        return this.currentOperators;
    }

    getOperatorByID(operatorID){
        return this.currentOperators.find(x => x.OperatorID === operatorID);
    }

    getOperatorByAccountID(accountID){
        return this.currentOperators.find(x => x.AccountID === accountID);
    }

    isRegisteredToOperator(operatorAccountID): boolean{
        if(this.currentUser.Registrations.length > 0){
            let regIndex = this.currentUser.Registrations.findIndex(x => x.OperatorAccountID === operatorAccountID);
            if(regIndex >= 0){
                if(this.currentUser.Registrations[regIndex].RegistrationCompleted){
                    if(this.currentUser.Registrations[regIndex].RegistrationDeleted){
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    getUserOperatorRegistration(operatorAccountID): Registration{
        return this.currentUser.Registrations.find(x => x.OperatorAccountID === operatorAccountID);
    }

    generateOperatorAccount():LokiKey {
        // create new user account for operator
        this.logger.debug("### BRM Create New Operator Account");
        // get current max account sequence and add 1
        let newAccountSequence = this.walletService.getAccountsMaxSequence() + 1;
        this.logger.debug("### BRM New Account Sequence: " + newAccountSequence);
        // generate new key pair for sequence
        let cscCrypto:CSCCrypto = new CSCCrypto("");
        cscCrypto.setPasswordKey(this.localStorageService.get(AppConstants.KEY_WALLET_MNEMONIC_HASH));
        cscCrypto.setPasswordSalt(this.getUser().Emailaddress);
        let cscKeyPair:CSCKeyPair = cscCrypto.generateKeyPair(newAccountSequence);
        let newKeyPair:LokiKey =
        { 
            privateKey: cscKeyPair.privateKey, 
            publicKey: cscKeyPair.publicKey, 
            accountID: cscKeyPair.accountID, 
            secret: cscKeyPair.seed, 
            encrypted: false
        }
        return newKeyPair;
    }

    activateOperatorAccount(newKeyPair: LokiKey, operatorAccountID):string {
        let newAccountSequence = this.walletService.getAccountsMaxSequence() + 1;
        if (newKeyPair.accountID.length > 0){
            // add keypair to database
            this.walletService.addKey(newKeyPair);
            // create new account
            let walletAccount: LokiAccount = {
                accountID: newKeyPair.accountID, 
                accountSequence: newAccountSequence,
                balance: "0", 
                lastSequence: 0, 
                label: operatorAccountID,
                activated: false,
                ownerCount: 0,
                lastTxID: "",
                lastTxLedger: 0
            };
            this.walletService.addAccount(walletAccount);
            // subcribe to new account transactions
            this.casinocoinService.subscribeAccounts.push(newKeyPair.accountID);
            this.casinocoinService.subscribeToAccountsStream(this.casinocoinService.subscribeAccounts);
            // encrypt keys
            this.logger.debug("### BRM - Encrypt Wallet Keys");
            this.walletService.encryptAllKeys(this.walletService.walletPIN).subscribe( result => {
                if(result == AppConstants.KEY_FINISHED){
                    this.logger.debug("### Account Created: " + walletAccount.accountID);
                    // send reserve coins from main account to operator account
                    this.signAndSubmit(this.currentUser.AccountID, walletAccount.accountID, this.accountReserveDrops.toString());
                }
            });
            return walletAccount.accountID;
        } else {
            return null;
        }
    }

    initializeOperatorRegistration(operatorAccountID:string, userAccountID:string): Observable<any>{
        // check if registration does not exist yet
        let regIndex = this.currentUser.Registrations.findIndex(x => x.OperatorAccountID === operatorAccountID);
        if(regIndex < 0){
            // find operator
            let operator = this.currentOperators.find(x => x.AccountID === operatorAccountID);
            let reg: Registration = {
                OperatorID: operator.OperatorID,
                RegistrationInitiatedDate: new Date(),
                RegistrationCompleted: false,
                RegistrationDeleted: false,
                OperatorAccountID: operator.AccountID,
                UserAccountIDForOperator: userAccountID,
                AccountSequence: this.walletService.getAccountsMaxSequence()
            }
            this.currentUser.Registrations.push(reg);
            this.currentUser.LastUpdateDate = new Date();
            this.localStorageService.set(AppConstants.KEY_BRM_USER, this.currentUser);
            return this.saveRegistration(this.currentUser.AccountID, reg);
            // this.saveUser(this.currentUser);
        }
    }

    // Deeplink: 
    registerToOperator(operatorAccountID:string, userAccountID:string){
        // check if not already registered
        if(! this.isRegisteredToOperator(operatorAccountID)){
            // get user registration for operator
            let regIndex = this.currentUser.Registrations.findIndex(x => x.OperatorAccountID === operatorAccountID);
            this.logger.debug("### BRM Current Registration Index: " + JSON.stringify(regIndex));
            if(regIndex >= 0){
                // find operator
                let operator = this.currentOperators.find(x => x.AccountID === operatorAccountID);
                if(operator !== undefined){
                    // open deeplink
                    // let link = operator.Deeplink.replace(":action", "register");
                    // open weblink
                    let link = operator.Weblink.replace(":action", "signup");
                    link = link.replace(":accountid", userAccountID);
                    link = link.replace(":token", this.brmConfiguration.apiKeys["X-API-JWT"]);
                    this.logger.debug("### BRM Open Link: " + link);
                    // this.iab.create(link, "_system");
                    if (this.platform.is('cordova')) {
                        let browser = this.iab.create(link, "_system");
                        browser.on('loaderror').subscribe(error => {
                            this.logger.error("### visitOperator Error opening browser: " + JSON.stringify(error));
                            this.alertCtrl.create({ 
                                title: 'Error redirecting to Browser', 
                                subTitle: 'Please copy the link and open in browser.',
                                message: link, 
                                buttons: [{ text: 'Copy', handler: () => {
                                    this.clipboard.copy(link);
                                    let toast = this.toastCtrl.create({
                                      message: 'Copied!',
                                      duration: 3000,
                                      position: 'top'
                                    });         
                                    toast.present();
                                } }] 
                            }).present();
                        });
                    } else {
                        this.iab.create(link, "_system");
                    }
                }
            }
        }

    }

    depositToOperator(operatorAccountID){
        this.logger.debug("### BRM Deposit to: " + operatorAccountID);
        this.currentOperators.forEach( operator => {
            if(operator.AccountID == operatorAccountID){
                // open deeplink
                operator.Deeplink.replace(":action", "deposit");
                this.logger.debug("### BRM Open Deeplink: " + operator.Deeplink);
            }
        });
    }

    finishRegistration(nav: NavController, operatorAccountID: string, accountID: string, destinationTag: number){
        this.logger.debug("### Finish Registration - UserAccountIDForOperator: " + accountID);
        // update the user profile from brm db
        this.updateCurrentUser().subscribe(finished => {
            this.logger.debug("### BRM Registraion update user finished: " + finished);
            if(finished){
                let regIndex = this.currentUser.Registrations.findIndex(x => x.UserAccountIDForOperator === accountID);
                let user = this.currentUser;
                this.logger.debug("### BRM Update User Registration Index: " + regIndex);
                if(regIndex >= 0){
                    user.Registrations[regIndex].DestinationTag = destinationTag;
                    user.Registrations[regIndex].RegistrationCompleted = true;
                    user.Registrations[regIndex].RegistrationCompletedDate = new Date();
                    user.Registrations[regIndex].RegistrationDeleted = false;
                    this.saveUser(user).subscribe(result => {
                        if(nav !== null){
                            nav.popToRoot();
                        }
                    });
                } else {
                    if(nav !== null){
                        nav.popToRoot();
                    }
                }
            }
        });
    }

    sendErrorMessage(error: ErrorMessage){
        this.logger.debug(JSON.stringify(error));
    }

    signAndSubmit(sourceAccount: string, destinationAccount: string, amountDrops: string, destinationTag?: number){
        this.logger.debug("### signAndSubmit - amount: " + amountDrops + " type: " + typeof amountDrops);
        let preparePayment: PrepareTxPayment = 
          { source: sourceAccount, 
            destination: destinationAccount, 
            amountDrops: amountDrops,
            feeDrops: this.feesDrops.toString(),
            description: "Operator Account Activation"
          };
        if(destinationTag){
            preparePayment.destinationTag = destinationTag;
        } else {
            this.logger.debug("No DestinationTag Added");
        }
        let txObject = this.casinocoinService.createPaymentTx(preparePayment);
        this.logger.debug("### Sign: " + JSON.stringify(txObject));
        let txBlob:string = this.casinocoinService.signTx(txObject, this.walletService.walletPIN);
        if(txBlob == AppConstants.KEY_ERRORED){
          // probably a wrong password!
          this.logger.error("Signing failed - Verify Password");
          // this.messageService.add({severity:'error', summary:'Transaction Signing', detail:'There was an error signing the transactions. Verify your password.'});
        } else {
          this.casinocoinService.submitTx(txBlob);
        }
    }

    checkUserAccount(accountID: string){
        this.userAccountCheck.next(true);
        this.brmRemote.userAccountIDGet(accountID).subscribe( result => {
            console.log("Login Result", result);
            this.userAccountCheck.next(true);
        },
        err =>{
            console.log("I am in error", err.status);
            if(err.status == 400){
                this.userAccountCheck.next(false);
            }
            else{
                this.userAccountCheck.next(true);
            }
        });

        // define and return subject
        return this.userAccountCheck.asObservable();
    }
    
    formatBalance(coins) {
        return coins.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

}