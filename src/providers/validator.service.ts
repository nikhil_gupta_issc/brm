import * as postalcode from 'postcode-validator';

export class ValidatorService {
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            'required': 'Required',
            'invalidCreditCard': 'Is invalid credit card number',
            'invalidEmailAddress': 'Invalid email address',
            'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
            'invalidAge': 'Invalid age. You must be 18 years or older to use this application.',
            'invalidInput': 'You have entered invalid input.',
            'minlength': `Minimum length ${validatorValue.requiredLength}`
        };
        return config[validatorName];
    }

    static creditCardValidator(control) {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
            return null;
        } else {
            return { 'invalidCreditCard': true };
        }
    }

    static emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
    }

    static nameValidator(control) {
        if (control.value.match(/^[a-zA-Z,.'`-\s]*$/)) {
            return null;
        } else {
            return { 'invalidInput': true };
        }
    }


    static postalcodeValidator(control) {
        if (control._parent) {
            let postelCode = control.value.toUpperCase();
            let countryCode = control._parent.value.cca2Code;
            if (countryCode == 'GB') {
                countryCode = 'UK';
            }
            if (postelCode.length > 0 && countryCode.length > 0) {
                if (postalcode.validate(postelCode, countryCode)) {
                    return null;
                }
            }
        }
        return { 'invalidInput': true };
    }

    static passwordValidator(control) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
            return null;
        } else {
            return { 'invalidPassword': true };
        }
    }

    static ageValidator(control) {
        console.log("### Age Validator Value: " + control.value);
        let today = new Date();
        let birthDate = new Date(control.value);
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        console.log("### Age: " + age);
        if(age >= 18){
            return null;
        } else {
            return { 'invalidAge': true };
        }
    }
}