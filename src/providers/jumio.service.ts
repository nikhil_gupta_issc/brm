import { Injectable } from '@angular/core';
import { LogService } from './log.service';
import { BRMService } from './brm.service';
import { DatePipe } from '@angular/common';
import { Platform } from 'ionic-angular';

@Injectable()
export class JumioService {

  headers: any;
  jumioApi: string = "https://lon.netverify.com/api/netverify/v2/scans";

  constructor(
    private logger: LogService,
    private brmService: BRMService,
    private datepipe: DatePipe,
    public platform: Platform) {
    this.logger.debug("### INIT  JumioService ###");
  }  

  isUserDetailsValid(): boolean {
    // let firstMiddleName = this.brmService.currentUser.Firstname.toLowerCase() + " " + this.brmService.currentUser.Middlename.toLowerCase();
    // if (this.brmService.currentUser.Firstname.toLowerCase() !== firstMiddleName.trim() ||
    //     this.brmService.currentUser.KYCStatus.JumioLastName.toLowerCase() !== this.brmService.currentUser.Lastname.toLowerCase().trim() ||
    //     this.brmService.currentUser.KYCStatus.JumioDOB !== this.datepipe.transform(this.brmService.currentUser.DateOfBirth, 'yyyy-MM-dd')) {
    // let jumioFullName = this.brmService.currentUser.KYCStatus.JumioFirstName + " " + this.brmService.currentUser.KYCStatus.JumioLastName;
    if (this.platform.is('mobileweb')) return true;
      
    let jumioFullName = "";
    if (this.brmService.currentUser.KYCStatus.JumioFirstName != "N/A")
      jumioFullName = this.brmService.currentUser.KYCStatus.JumioFirstName + " ";
    if (this.brmService.currentUser.KYCStatus.JumioLastName != "N/A")
      jumioFullName = jumioFullName + this.brmService.currentUser.KYCStatus.JumioLastName;
    
    if (jumioFullName.toLowerCase().trim() !== this.brmService.currentUser.Fullname.toLowerCase().trim() ||
        this.brmService.currentUser.KYCStatus.JumioDOB !== this.datepipe.transform(this.brmService.currentUser.DateOfBirth, 'yyyy-MM-dd')) {
      return false;
    } else {
      return true;
    }
  }

  // kycVerify(jumioToken: string, jumioSecret: string, scanReference: string, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
  //   this.logger.debug("### Jumio API Calling with Scan Reference: " + scanReference);
  //   if(this.platform.is('mobileweb')) {
  //     this.jumioApi = "/jumioApi";
  //     this.logger.debug("### JumioService - added proxy - API: " + this.jumioApi);
  //   }
  //   let base64encodedData = new Buffer(jumioToken + ':' + jumioSecret).toString('base64');
  //   this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  //   this.headers = this.headers.set('Authorization', 'Basic ' + base64encodedData);

  //   return this.httpClient.get<any>(`${this.jumioApi}/${encodeURIComponent(String(scanReference))}/data`,
  //     {
  //       withCredentials: true,
  //       headers: this.headers,
  //       observe: observe,
  //       reportProgress: reportProgress
  //     }
  //   );
  // }

}