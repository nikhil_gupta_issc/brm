import { ErrorHandler, Injectable } from '@angular/core';
import { BRMService, ErrorMessage } from '../providers/brm.service';
import { Device } from '@ionic-native/device';

@Injectable()
export class CSCErrorHandler implements ErrorHandler {

    constructor( private brmService: BRMService,
                 private device: Device){
    }   

    handleError(err: any): void {
        let error: ErrorMessage = {
            device: this.device.manufacturer,
            model: this.device.model,
            timestamp: new Date().toISOString(),
            error: err.message ? err.message : err.toString()
        }
        this.brmService.sendErrorMessage(error);
    }
}