import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { CSCErrorHandler } from './csc-errors.module';

import { BrmApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { OperatorsPage } from '../pages/operators/operators';
import { SendPage } from '../pages/send/send';
import { ReceivePage } from '../pages/receive/receive';
import { ContactsPage } from '../pages/contacts/contacts';
import { TransactionsPage } from '../pages/transactions/transactions';
import { OperatorDetailsPage } from '../pages/operator-details/operator-details';
import { GetCscPage } from '../pages/get-csc/get-csc';
import { NetworkStatusPage } from '../pages/network-status/network-status' ;
import { SettingsPage } from '../pages/settings/settings';
import { ResponsibleGamingPage } from '../pages/responsible-gaming/responsible-gaming';
import { TermsOfServicePage } from '../pages/terms-of-service/terms-of-service';
import { AboutPage } from '../pages/about/about';
import { TransferFundsPage } from '../pages/transfer-funds/transfer-funds';
import { DepositPage } from '../pages/deposit/deposit';
import { KycPage } from '../pages/kyc/kyc';
import { LinkAccountPage } from '../pages/link-account/link-account';
import { RestorePage } from '../pages/restore/restore';
import { MainPage } from '../pages/main/main';
import { ChangePinPage } from '../pages/change-pin/change-pin';
import { RecoverPinPage } from '../pages/recover-pin/recover-pin';
import { TransactionDetailsPage } from '../pages/transaction-details/transaction-details';

import { ComponentsModule } from '../components/components.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import { File } from '@ionic-native/file';
import { AppVersion } from '@ionic-native/app-version';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Deeplinks } from '@ionic-native/deeplinks';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Network } from '@ionic-native/network';
import { PincodeController, PincodeCmp } from  'ionic2-pincode-input';
import { PincodeInputModule } from 'ionic2-pincode-input';

import { LogService } from '../providers/log.service';
import { WebsocketService } from '../providers/websocket.service';
import { CasinocoinService } from '../providers/casinocoin.service';
import { WalletService } from '../providers/wallet.service';
import { NotificationService } from '../providers/notification.service';
import { ValidatorService } from '../providers/validator.service';
import { MarketService } from '../providers/market.service';
import { BRMService } from '../providers/brm.service';

import { LocalStorageService, SessionStorageService } from "ngx-store";

import { DatePipe, DecimalPipe, CurrencyPipe } from '@angular/common';
import { CSCDatePipe, CSCAmountPipe, ToNumberPipe } from './app-pipes.module';
import { CountryPickerModule, CountryPickerService } from 'ngx-country-picker';

import { QRCodeModule } from 'angular2-qrcode';
import { Device } from '@ionic-native/device';

import { ApiModule as BRMApiModule, Configuration, ConfigurationParameters } from '../domain/brm-api';
import { BASE_PATH } from '../domain/brm-api/variables';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

/** Multi Language Support */
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Clipboard } from '@ionic-native/clipboard';
import { JumioService } from '../providers/jumio.service';
import { DatePicker } from '@ionic-native/date-picker';
import { AppCenterCrashes } from '@ionic-native/app-center-crashes';
import { SetupPage } from '../pages/setup/setup';
import { RecoveryPhrasePage } from '../pages/recovery-phrase/recovery-phrase';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

export function apiConfigFactory (): Configuration {
    const params: ConfigurationParameters = {
        apiKeys: {}
    }
    return new Configuration(params);
}

@NgModule({
  declarations: [
    BrmApp,
    CSCDatePipe, CSCAmountPipe,ToNumberPipe,
    SignupPage, LoginPage,
    HomePage, OperatorsPage,
    SendPage, ReceivePage,
    ContactsPage, TransactionsPage,
    OperatorDetailsPage,
    GetCscPage, NetworkStatusPage,
    SettingsPage, ResponsibleGamingPage,
    TermsOfServicePage, AboutPage,
    TransferFundsPage, DepositPage, KycPage,
    LinkAccountPage, RestorePage, MainPage,
    ChangePinPage, RecoverPinPage, SetupPage,
    RecoveryPhrasePage, TransactionDetailsPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(BrmApp, {
      platforms:{
        android: {
          scrollAssist: true,
          autoFocusAssist: true,
          tabsHideOnSubPages: true
        }
      }
    }),
    CountryPickerModule.forRoot(),
    ComponentsModule,
    QRCodeModule, PincodeInputModule,
    BRMApiModule.forRoot(apiConfigFactory),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    BrmApp, SignupPage,
    LoginPage, HomePage,
    OperatorsPage, SendPage,
    ReceivePage, ContactsPage,
    TransactionsPage, OperatorDetailsPage,
    GetCscPage, NetworkStatusPage,
    SettingsPage, ResponsibleGamingPage,
    TermsOfServicePage, AboutPage,
    PincodeCmp, TransferFundsPage,
    DepositPage, KycPage, LinkAccountPage,
    RestorePage, MainPage, ChangePinPage,
    RecoverPinPage, SetupPage, RecoveryPhrasePage,
    TransactionDetailsPage
  ],
  providers: [
    DatePicker,
    StatusBar,
    SplashScreen,
    Keyboard,
    Device,
    AndroidPermissions,
    File,
    Network,
    AppVersion,
    {provide: ErrorHandler, useClass: CSCErrorHandler},
    DatePipe, DecimalPipe, CurrencyPipe,
    CSCDatePipe, CSCAmountPipe, ToNumberPipe,
    { provide: BASE_PATH, useValue: 'https://brmdev.casinocoin.org/brm' },   // Development: https://brmdev.casinocoin.org/brm / Production: https://api.casinocoin.org/1.0.0/brm
    LogService,
    WebsocketService,
    CasinocoinService,
    WalletService,
    BRMService,
    MarketService,
    NotificationService,
    LocalStorageService,
    SessionStorageService,
    ValidatorService,
    CountryPickerService,
    InAppBrowser,
    Deeplinks,
    BarcodeScanner,
    SocialSharing,
    PincodeController,
    FingerprintAIO,
    Clipboard,
    JumioService,
    AppCenterCrashes
  ]
})
export class AppModule {}
