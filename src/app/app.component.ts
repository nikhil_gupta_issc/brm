import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Deeplinks } from '@ionic-native/deeplinks';
import { Network } from '@ionic-native/network';

import { WalletService } from '../providers/wallet.service';
import { WebsocketService } from '../providers/websocket.service';
import { CasinocoinService } from '../providers/casinocoin.service';
import { LogService } from '../providers/log.service';
import { BRMService } from '../providers/brm.service';
import { BehaviorSubject, Observable } from 'rxjs';

import { MainPage } from '../pages/main/main';

import { AppConstants } from '../domain/app-constants';
import { LocalStorageService } from 'ngx-store';
import { LoginPage } from '../pages/login/login';
import { GetCscPage } from '../pages/get-csc/get-csc';

//import { NetworkStatusPage } from '../pages/network-status/network-status';
import { SettingsPage } from '../pages/settings/settings';
import { ResponsibleGamingPage } from '../pages/responsible-gaming/responsible-gaming';
//import { TermsOfServicePage } from '../pages/terms-of-service/terms-of-service';
import { AboutPage } from '../pages/about/about';
//import { HomePage } from '../pages/home/home';


import { User } from '../domain/brm-api';

import { TranslateService } from '@ngx-translate/core';
import { AppCenterCrashes } from '@ionic-native/app-center-crashes';

@Component({
  templateUrl: 'app.html'
})
export class BrmApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  connectedText: string = "Disconnected";
  connectedColor: string = "disconnected";

  pages: Array<{title: string, component: any, icon: string, image: string}>;
  inActive: any;

  constructor( public platform: Platform, 
               public statusBar: StatusBar, 
               public splashScreen: SplashScreen,
               private deeplinks: Deeplinks,
               private localStorageService: LocalStorageService,
               private logger: LogService,
               public androidPermissions: AndroidPermissions,
               private walletService: WalletService,
               private casinocoinService: CasinocoinService,
               private websocketService: WebsocketService,
               private brmService: BRMService,
               private translateService: TranslateService,
               private network: Network,
               private alertCtrl: AlertController,
               private appCenterCrashes: AppCenterCrashes ) {
    this.initializeApp();

    // All the possible pages the app has from the Menu
    this.pages = [
      { title: 'Exchanges', component: GetCscPage, icon: "information-circle", image: "icon_exchanges.svg" },
      { title: 'Set Limits', component: ResponsibleGamingPage, icon: "hand", image: "icon_limits.svg" },
      { title: 'About', component: AboutPage, icon: "icon-csc", image: "logo_icon.svg" },
      { title: 'Settings', component: SettingsPage, icon: "settings", image: "icon_settings_side_menu.svg" }
      //{ title: 'Home', component: HomePage, icon: "home" },
      // { title: 'Player Profile', component: PlayerProfilePage },
     // { title: 'Network Status', component: NetworkStatusPage, icon: "icon-network" },
     // { title: 'Terms of Service', component: TermsOfServicePage, icon: "paper" }
    ];

    this.deeplinks.route({
      '/confirm-registration/:operatorAccountID/:userAccountID/:destinationTag': "finish-registration",
    }).subscribe((match) => {
      // match.$route - the route we matched, which is the matched entry from the arguments to route()
      // match.$args - the args passed in the link
      // match.$link - the full link data
      this.logger.debug("### APP - deeplink received: " + JSON.stringify(match));
      if(match.$route === "finish-registration"){
        this.brmService.finishRegistration(this.nav, match.$args.operatorAccountID, match.$args.userAccountID, match.$args.destinationTag);
      } else {
        this.logger.debug("### APP - "+ match.$route + " not implemented.")
      }
    }, (nomatch) => {
      // nomatch.$link - the full link data
      this.logger.debug("### APP - unmatched deeplink: " + JSON.stringify(nomatch));
    });

    // subscribe to CasinoCoin connections
    this.casinocoinService.casinocoinConnectedSubject.subscribe( connected => {
      if(connected){
        // set connected header
        this.connectedText = "Connected";
        this.connectedColor = "connected";
      } else {
        this.connectedText = "Disconnected";
        this.connectedColor = "disconnected";
      }
    });
  }

  initializeApp() {
    // check rights if on android
    if (this.platform.is('android')) {
        // android permissions
        this.androidPermissions.requestPermissions(
          [
            this.androidPermissions.PERMISSION.CAMERA,
            this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, 
            this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE,
            this.androidPermissions.PERMISSION.INTERNET
          ]
        );
    }
    // check if signup needs to run
    let userObj: User = this.localStorageService.get(AppConstants.KEY_BRM_USER);
    if (this.localStorageService.get(AppConstants.KEY_SETUP_COMPLETED) == null) {
      this.logger.debug("### APP - SHOW SIGNUP ###");
      this.rootPage = MainPage;
      // this.nav.setRoot(MainPage);
    }
    else{
      this.brmService.checkUserAccount(userObj.AccountID).subscribe( result =>{
        //console.log("result", result);
        if(!result){
          this.rootPage = MainPage;
          this.localStorageService.remove(AppConstants.KEY_BRM_USER);
          this.localStorageService.remove(AppConstants.KEY_SETUP_COMPLETED);
        }
      });
    }
    this.platform.ready().then(() => {
      this.logger.debug('### BRM App Platforms: ' + this.platform._platforms);
      // register to pause and resume events
      this.casinocoinService.appPaused = false;
      this.platform.pause.subscribe(() => {        
        this.logger.debug('### BRM App PAUSED ###');
        this.casinocoinService.appPaused = true;
        // logout when inactive for 5 mins
        this.inActive = Observable.interval(300000).subscribe((val) => {
          if(this.localStorageService.get(AppConstants.LOGGED_IN)) {
            this.logOut();
            this.inActive.unsubscribe();
          }
        });
      });  
      this.platform.resume.subscribe(() => {      
        this.logger.debug('### BRM App RESUMED ###');
        this.casinocoinService.appPaused = false;
        let timeSinceLastLedger = (Date.now() - this.casinocoinService.lastLedgerTimestamp) / 1000;
        this.logger.debug('### BRM timeSinceLastLedger: ' + timeSinceLastLedger);
        // a new ledger arrives maximum every 5 minutes so we check for a bit more
        if(this.casinocoinService.networkConnected && (timeSinceLastLedger > 310)){
          // we have network but probably no CasinoCoin connection
          this.casinocoinService.reconnect();
        }
        if(this.inActive){
          this.inActive.unsubscribe();
        }
      });
      // register to network connection events
      this.logger.debug('### BRM Network Type: ' + this.network.type);
      if(this.network.type && this.network.type !== 'unknown' && this.network.type !== 'none'){
        this.casinocoinService.networkConnected = true;
      } else {
        this.casinocoinService.networkConnected = false;
      }
      this.network.onDisconnect().subscribe(() => {
        this.logger.debug('### BRM Network was disconnected ###');
        this.casinocoinService.networkConnected = false;
      });
      this.network.onConnect().subscribe(() => {
        this.logger.debug('### BRM Network connected ###');
        this.casinocoinService.networkConnected = true;
        this.websocketService.isConnected$.subscribe(connected => {
          if(!connected){
            this.casinocoinService.reconnect();
          }
        });
      });
      // connect to casinocoin network
      this.casinocoinService.connect().subscribe(connected => {
        if(connected == AppConstants.KEY_CONNECTED){
          this.logger.debug("### Casinocoin Connected ###");
        }
      });
      // hide the splash screen
      this.splashScreen.hide();

      /**Multi Language Support */
      let defaultLanguage = userObj && userObj.Language ? userObj.Language : 'en';
      this.translateService.setDefaultLang(defaultLanguage);
      this.translateService.use(defaultLanguage);

      // check platform
      if (this.platform.is('cordova')) {
        //this.statusBar.styleDefault();
        this.statusBar.hide();
        this.splashScreen.hide();
        window['plugins'].preventscreenshot.enable(result => {
          this.logger.debug("### Screen Shot Enabled: " + result);
        }, error => {
          this.logger.debug("### Screen Shot Error: " + error);
        });
      }      
    });

    // App Crash
    this.appCenterCrashes.setEnabled(true).then(() => {
      this.appCenterCrashes.lastSessionCrashReport().then(report => {
        if(report) {
          console.log('Crash report ', report);
          let alert = this.alertCtrl.create({
            title: 'IMPORTANT!',
            message: "App was crashed in last Session. Do you want to send details to Foundation?",
            buttons: [
              { text: 'No' },
              {
                text: 'Yes',
                handler: data => {
                  // mail code here
                }
              }
            ]
          });
          alert.present();
        }
      });
   });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.title === "Home"){
      this.nav.setRoot(page.component);
    } else {
      this.nav.push(page.component);
    }
  }

  logOut() {  
    // log out
    this.walletService.openWalletSubject.subscribe( result => {
      if(result == AppConstants.KEY_LOADED){
        this.logger.debug("### Log Out");
        this.localStorageService.set(AppConstants.LOGGED_IN, false);
        this.walletService.closeWallet();
        this.casinocoinService.disconnect();
        this.walletService.openWalletSubject = new BehaviorSubject<string>(AppConstants.KEY_INIT);
        this.brmService.loginSubject = new BehaviorSubject<string>(AppConstants.KEY_INIT);
        // navigate to Login page
        this.nav.setRoot(LoginPage);
      }
    });
  }
}
