/**
 * CasinoCoin BRM API
 * CasinoCoin BRM API for communication with the CasinoCoin Foundation Bank Roll Manager database.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: andre@casinocoin.org
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface GamingLimit {
    OperatorID?: string;
    OperatorName?: string;
    Value24H?: number;
    Enabled24H?: boolean;
    TimestampSet24H?: number;
    Value7D?: number;
    Enabled7D?: boolean;
    TimestampSet7D?: number;
    Value30D?: number;
    Enabled30D?: boolean;
    TimestampSet30D?: number;
}
