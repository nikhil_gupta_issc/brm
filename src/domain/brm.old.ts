export enum VerificationStatus {
    NOT_VERIFIED = "Verification not Started",
    APPROVED_VERIFIED = "Approved and Verified",
    DENIED_FRAUD = "Denied due to Fraud",
    DENIED_UNSUPPORTED_ID_TYPE = "Unsupported ID Type",
    DENIED_UNSUPPORTED_ID_COUNTRY = "Unsupported ID Country",
    ERROR_NOT_READABLE_ID = "ID not Readable",
    NO_ID_UPLOADED = "ID not Uploaded"
}

export interface User {
    AccountID: string;
    Firstname: string;
    Lastname: string;
    Prefix?: string;
    DateOfBirth: number;
    Gender: string;
    NationalityISOCode: string;
    Nationality: string;
    Nickname?: string;
    Street: string;
    Housenumber: string;
    PostalCode: string;
    City: string;
    CountryISOCode: string;
    Country: string;
    Emailaddress: string;
    Telephonenumber: string;
    Verification: {
        Status: VerificationStatus;
        ScanReference?: string;
        ScanSubmittedDate?: string;
        VerificationReceivedDate?: string;
    };
    Registrations: Array<Registration>;
    GamingLimits?: {
        AllEnabled: boolean;
        All24H: number;
        All24HEnabled: boolean;
        All24Timestamp: number;
        All7D: number;
        All7DEnabled: boolean;
        All7DTimestamp: number;
        All30D: number;
        All30DEnabled: boolean;
        All30Timestamp: number;
        OperatorLimits: Array<GamingLimit>;
        SelfExclusion: boolean;
        SelfExclusionTimestamp: number;
    };
    CreationDate: string;
    LastUpdateDate: string;
}

export interface Operator {
    _id: string;
    AccountID: string;
    Name: string;
    DisplayName: string;
    Logo: string;
    Deeplink?: string;
    Weblink?: string;
    CreationDate: string;
    LastUpdateDate: string;
}

export interface Registration { 
    OperatorID: string;
    RegistrationInitiatedDate: string;
    RegistrationCompletedDate?: string;
    RegistrationCompleted: boolean;
    OperatorAccountID: string;
    DestinationTag?: number;
    UserAccountIDForOperator: string;
}

export interface GamingLimit {
    OperatorID: string;
    OperatorName: string;
    value24H: number;
    enabled24H: boolean;
    timestamp24H: number;
    value7D: number;
    enabled7D: boolean;
    timestamp7D: number;
    value30D: number;
    enabled30D: boolean;
    timestamp30D: number;
}

export interface ErrorMessage {
    device: string;
    model: string;
    timestamp: string;
    error: any;
}

export interface ShowLimits {
    value24H: number;
    enabled24H: boolean;
    timestamp24H: number;
    value7D: number;
    enabled7D: boolean;
    timestamp7D:number;
    value30D: number;
    enabled30D: boolean;
    timestamp30D: number;
};