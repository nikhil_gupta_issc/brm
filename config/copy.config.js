// New copy task for csc font files
module.exports = {
    copyFontCasinocoin: {
      src: ['{{ROOT}}/csc-font/fonts/**/*'],
      dest: '{{WWW}}/assets/fonts'
    }
  };